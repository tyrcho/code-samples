
# https://stackoverflow.com/questions/1527049/how-can-i-join-elements-of-an-array-in-bash
join_by() {
  local d=${1-} f=${2-}
  if shift 2; then
    printf %s "$f" "${@/#/$d}"
  fi
}


mkstring(){
	local start=$1
	local inter=$2
	local end=$3
	shift 3

	printf $start
	join_by $inter $*
	printf $end
}


mkstring2() {
	local start=$1
	local inter=$2
	local end=$3
	shift 3

  printf $start

  printf "$1"
  for arg in "${@:2}"; do
    printf "$inter"
    printf "$arg"
  done

  printf "$end"
}
