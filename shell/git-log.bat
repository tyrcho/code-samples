log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %Cblue<%an>%Creset' --abbrev-commit --date=relative --all

 git log --pretty=format:"[%h] %an %ad %s" --numstat --date=short
 
 git log  --pretty=format:"[%h] %an %ad %s" --numstat --date=short --follow FILENAME
 
 rem get all commits ids from auser, from all branches
 git log --pretty=format:"%H"  --author=auser --all
 
 rem diff at character level instead of line
 git diff HEAD~5 --word-diff-regex=.
 
 