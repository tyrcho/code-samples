#inspired by http://unix.stackexchange.com/questions/4367/extracting-nested-zip-files

while [ "`find . -type f -name '*.?ar' | wc -l`" -gt 0 ]; do 
  find -type f -name "*.?ar"  \
    -exec mkdir -p '{}.dir' \;  \
    -exec unzip -o -d '{}.dir'  -- '../{}' \;   \
    -exec rm -- '{}' \;; 
done
