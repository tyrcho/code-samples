#!/usr/bin/env bash
# list authors
# git shortlog -sne

# git-diffs-author someone@email.com 2020-06-25 develop >> log.txt
git-diffs-author() {
  local author=$1
  local date="$2"
  local reference=${3-master}

  git checkout "$reference@{$date}"
  git checkout -b temp

  git log --reverse --pretty=%H --author=$author temp..$reference |
  while read commit; do
      git show --format=fuller $commit
  done

  git checkout $reference
  git branch -D temp
}
