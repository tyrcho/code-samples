#!/bin/bash
read width
read height
read TEXT

UPPER_TEXT=${TEXT^^}

ALPHABET=`printf "%s" {A..Z}`

IFS=''
declare -a LETTERS
for ((i=0; i<height; i++)); do
    read -r LETTERS[i]
done

for (( row_index = 0 ; row_index < $height; row_index++ )); do
    row=${LETTERS[$row_index]}
    for (( i=0; i<${#UPPER_TEXT}; i++ )); do
        letter="${UPPER_TEXT:$i:1}"
        if [[ ${letter} == [A-Z] ]]; then
            letter_index=$(expr index $ALPHABET $letter - 1)
        else
            letter_index="26"
        fi
        start_index=$(($letter_index * $width))
        slice=("${row[@]:$start_index:$width}")
        echo -n "$slice"
    done
    echo
done

