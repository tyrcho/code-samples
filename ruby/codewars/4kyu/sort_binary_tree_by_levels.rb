def tree_by_levels(node)
  stack = [node]
  acc = []
  stack_index = 0
  until stack[stack_index] == nil
    head = stack[stack_index]
    stack << head.left unless head.left == nil
    stack << head.right unless head.right == nil
    acc = acc << head.value
    stack_index+=1
  end
  acc
end
