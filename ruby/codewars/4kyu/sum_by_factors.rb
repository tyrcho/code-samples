require 'prime'

def sumOfDivided(numbers)
  Prime.each(numbers.map(&:abs).max).flat_map { |prime|
    multiples = numbers.select { |x| x % prime == 0 }
    multiples.empty? ? [] : [[prime, multiples.sum]]
  }
end
