def who_is_winner(moves)
  winner_rec(moves, Array.new(7, Array.new()))
end

def winner_rec(moves, board)
  if(moves.empty?)
    "Draw"
  else
    move = moves.first.match(/(.*)_(.*)/).captures
    next_board = play(board, move)
    ["Yellow", "Red"]
      .find { |p| wins(next_board, p) 
            } || winner_rec(moves.drop(1), next_board)
  end 
end


def wins(board, player)
  diag1 = diag(board, 1)
  diag2 = diag(board, -1)
  trans = board.map { |row|  row.dup.fill(nil, row.length..6) }.transpose
  
  board.any? { |row| winning(row, player) } ||
  diag1.any? { |row| winning(row, player) } ||
  diag2.any? { |row| winning(row, player) } ||
  trans.any? { |row| winning(row, player) }   
end

def diag(board, direction)
  all_cells = (0..6).to_a.product((0..5).to_a)

  (-6..6)
    .map { |dist| all_cells.select { |x, y| x + direction * y == dist }}
    .flat_map { |cells| 
      cells
        .map { |c| board[c[0]][c[1]] }
        .each_cons(4)
        .to_a
    }
end

def winning(row, player)
  row.each_cons(4).any? { |cons| 
    cons.count(player) == 4 
  }
end

def play(board, move)
  index = ("A".."G").to_a.index(move[0])
  board.take(index) + [board[index] + [move[1]]] + board.drop(index + 1)
end
