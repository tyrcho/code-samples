def get_pins(observed)
 near = ["08", "124", "2135", "326", "4157", "52468", "6359", "748", "85790", "968"]
   .map{ |s| s.split('').map { |c| [c] } }
   .method(:at)

 observed
   .chars
   .map(&:to_i)
   .map(&near)
   .reduce(&:product)
   .map(&:join)
end

def get_pins(observed)
  neighbors = -> s { ["08", "124", "2135", "326", "4157", "52468", "6359", "748", "85790", "968"][s.to_i].split('') }
  observed
    .split('')
    .map(&neighbors)
    .reduce {|m, x| m.product(x).map(&:join) }
end
