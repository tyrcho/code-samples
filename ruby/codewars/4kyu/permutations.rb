def permutations(string)
  string
    .chars
    .permutation(string.length)
    .uniq
    .map(&:join)
end
