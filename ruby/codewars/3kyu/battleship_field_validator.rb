def validate_battlefield(battle_field)
  ships = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1]
  battle_field.size == 10 && battle_field.all? { |row| row.size == 10 } &&
  battle_field.flatten.sum == ships.sum &&
    validate_ships(battle_field, ships)    
end

def validate_ships(battle_field, ships)
  ships.empty? ||
    (possible_coordinates(battle_field, ships.first) + possible_coordinates_transpose(battle_field, ships.first))
      .any? { |coord| 
        rows, cols = coord[0], coord[1]
        next_field = remove(battle_field, rows, cols) 
        !has_adjacent(next_field, rows, cols) && validate_ships(next_field, ships.drop(1))
      }  
end

def has_adjacent(next_field, rows, cols)
  min_row = [rows.begin-1, 0].max
  max_row = [rows.end+1, 9].min
  min_col = [cols.begin-1, 0].max
  max_col = [cols.end+1, 9].min
  (min_row..max_row).any? { |r| 
    (min_col..max_col).any? { |c| 
      next_field[r][c] == 1 
    }
  }
end

def remove(battle_field, rows, cols)
  (0..9).map { |r|    
    (0..9).map { |c|
      battle_field[r][c] == 1 && !(rows.cover?(r) && cols.cover?(c)) ? 1 : 0
    }
  }
end

def possible_coordinates(battle_field, size)  
  possible_cols = (0..10-size).map { |start| start..start+size-1 }
  (0..9).flat_map { |r|
    possible_cols.flat_map { |col_range| 
      battle_field[r][col_range].count(1) == size ?
        [[r..r, col_range]] : []
    }
  }
end

def possible_coordinates_transpose(battle_field, size)  
  possible_coordinates(battle_field.transpose, size).map(&:reverse)
end
