def validate_battlefield(battle_field)
  ships = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1]
  battle_field.flatten.sum == ships.sum &&
    validate_ships(battle_field, ships)    
end

def validate_ships(battle_field, ships)
  ships.empty? ||
    (possible_coordinates(battle_field, ships.first) + possible_coordinates_transpose(battle_field, ships.first))
      .any? { |coord| 
        next_field = remove(battle_field, coord[0], coord[1]) 
        validate_ships(next_field, ships.drop(1))
      }  
end

def remove(battle_field, rows, cols)
  (0..9).map { |r|    
    (0..9).map { |c|
      battle_field[r][c] == 1 && !(rows.cover?(r) && cols.cover?(c)) ? 1 : 0
    }
  }
end

def possible_coordinates(battle_field, size)  
  possible_cols = (0..10-size).map { |start| start..start+size-1 }
  (0..9).flat_map { |r|
    possible_cols.flat_map { |col_range| 
      battle_field[r][col_range].count(1) == size ?
        [[r..r, col_range]] : []
    }
  }
end

def possible_coordinates_transpose(battle_field, size)  
  possible_coordinates(battle_field.transpose, size).map(&:reverse)
end
