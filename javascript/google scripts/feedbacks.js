function onFormSubmit(e) {
  Logger.log("form submit");
  var values=e.namedValues;
  var time=values['Horodateur'][0];
  Logger.log("form submit at time "+time);
  var points=values['Points'][0];
  var message=values['Message'][0];
  var from=values['Adresse e-mail'][0];
  var to=values['Destinataire'][0];  
  notifyRecipient(from, to, message);
  notifySender(from, to, message, points);
}


function notifySender(from, to, message, points) {
  var body = Utilities.formatString("Vous (%s) avez envoyé à %s: <br/> %s points <br/>avec le message <p>%s</p>", from, to, points, message) ;
  MailApp.sendEmail({
    to: from,
    subject: "feedback envoyé à " + to,
    htmlBody: body
  });
}

function notifyRecipient(from, to, message) {
  var body = Utilities.formatString("Vous (%s) avez reçu de %s un feedback avec le message <p>%s</p>", to, from, message) ;
  MailApp.sendEmail({
    to: to,
    subject: "feedback reçu de " + from,
    htmlBody: body
  });
}

function doGet() {
  Logger.log(groupBy(allFeedbacks(), month));
  return HtmlService
      .createTemplateFromFile('index.html')
      .evaluate();
}


function allFeedbacks() {
  var thisMonth = Utilities.formatDate(new Date(), "Europe/Paris", "yyyy-MM");
  function isThisMonth(row) { return month(row) === thisMonth; }
  
  return SpreadsheetApp
      .getActiveSpreadsheet()
      .getSheets()[0]
      .getDataRange()
      .getValues()
      .map(function(row) { 
        return { 
          "date": Utilities.formatDate(new Date(row[0]), "Europe/Paris", "yyyy-MM-dd HH:mm")+"",
          "from": row[1],
          "to": row[2],
          "message": row[4],
          "points": row[3],
        };
      });
      //.filter(isThisMonth);
}

function givenFeedbacks() {
  var user=Session.getActiveUser();
  return allFeedbacks()
    .filter(function(row) { return row.from == user.getEmail(); });
}

function receivedFeedbacks() {
  var user=Session.getActiveUser();
  return allFeedbacks()
      .filter(function(row) { return row.to == user.getEmail(); });
}

function receivedByMonth() {
  return groupBy(receivedFeedbacks(), month);
}

function givenByMonth() {
  return groupBy(givenFeedbacks(), month);
}

function month(row) {
  return row.date.substring(0, 7);
}

function groupBy(xs, key) {
  return xs.reduce(function(rv, x) {
    var v = key instanceof Function ? key(x) : x[key];
    (rv[v] = rv[v] || []).push(x); 
    return rv; }, 
  {});
};
