function doGet() {
 return HtmlService
     .createTemplateFromFile('index.html')
     .evaluate();
}

function getInfo(code_postal) {
  return SpreadsheetApp
     .getActiveSpreadsheet()
     .getSheets()[0]
     .getDataRange()
     .getValues()
     .filter(function(row) {
       return row[0]==code_postal;
     })
     .map(function(row) {
       return row;
     });
}
