// https://colisweb.fibery.io/fibery/space/Produit/database/Project/automations/rule/6400a51843bfdd3475c91d4a/actions

// Developer reference is at api.fibery.io/#action-buttons

// Fibery API is used to retrieve and update entities
const fibery = context.getService('fibery');
const utils = context.getService('utils');

// affected entities are stored in args.currentEntities;
// to support batch actions they always come in an array
for (const entity of args.currentEntities) {

    // to get collection fields query the API and provide the list of fields
    const entityWithExtraFields = await fibery.getEntityById(entity.type, entity.id, [ 'Repo Experts']);
    const experts = entityWithExtraFields['Repo Experts'];

    const taskId = utils.uuid();

    const task = await fibery.createEntity("Produit/Tâche", {
        "fibery/id" : taskId,
        "Name": "Résoudre pour " + entity.Name+ " les "+entity['Steward MR count']+" MR de scala-steward non mergées",
        "Repo": entity.Repo.id
    });

    for (const expert of experts) {
        await fibery.addCollectionItem("Produit/Tâche", taskId, 'Assign', expert['id']);
    }
}


