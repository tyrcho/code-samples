

// Developer reference is at api.fibery.io/#action-buttons
// https://the.fibery.io/@public/User_Guide/Guide/Formulas-39#Guide/How-to-check-whether-the-Rich-Text-is-empty-script-workaround-119

const fibery = context.getService('fibery');

// affected entities are stored in args.currentEntities;
// to support batch actions they always come in an array

for (const entity of args.currentEntities) {
	// the rich text field is a document, which requires a special id to get access to
	// In this case, the rich text field is "Description", so we request that field be included
	const entityWithExtraFields = await fibery.getEntityById(entity.type, entity.id, ['MR']);
	// as long as we have the rich text field looking valid, let's do something with it
	if (entityWithExtraFields['MR']) {
		// just saving a shorter reference to it
		const dsc = entityWithExtraFields['MR'];
		// get the rich text field contents as markdown (supports md, html, and json)
		const doc = await fibery.getDocumentContent(dsc.secret, 'md');
		await fibery.updateEntity(entity.type, entity.id, {
			'MR2': doc.replace(/>$/, '').replace(/^</, '')
		});
	}
}