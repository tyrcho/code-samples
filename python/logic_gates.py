# https://www.codingame.com/training/easy/logic-gates

in_count = int(input())
out_count = int(input())

inputs = dict(input().split() for _ in range(in_count))

operations = {
    "AND": lambda a, b: a and b,
    "OR": lambda a, b: a or b,
    "XOR": lambda a, b: a ^ b
}

def is_up(i): return i == "-"

def out(a, b, op, negate):
    return "_" if negate == op(is_up(a), is_up(b)) else "-"

for _ in range(out_count):
    (out_name, op_name, i1, i2) = input().split()
    negate = op_name[0] == 'N'
    operation = operations[op_name[1:]] if negate else operations[op_name]
    outputs = [out(a, b, operation, negate) for (a, b) in zip(inputs[i1], inputs[i2])]
    print("%s %s" % (out_name, "".join(outputs)))
