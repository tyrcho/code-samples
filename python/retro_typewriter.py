# https://www.codingame.com/training/easy/retro-typewriter-art

import functools

aliases = { "sp": " ",
            "bS": "\\",
            "sQ": "'",
            "nl": "1\n" }

for chunk in input().split():
    normalized = functools.reduce(lambda s, t: s.replace(*t), aliases.items(), chunk)
    print(normalized[-1] * int(normalized[:-1]), end="")