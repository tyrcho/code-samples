# https://www.codingame.com/training/easy/abcdefghijklmnopqrstuvwxyz

n = int(input())
rows = [input() for _ in range(n)]

starts = [(i, j) for i in range(n) for j in range(n) if rows[i][j] == "a"]

def complete(sentence):
    (i, j) = sentence[0]
    tip = rows[i][j]
    if tip == "z": return sentence
    expected = chr(ord(tip) + 1)
    for a, b in (i, j + 1), (i, j - 1), (i - 1, j), (i + 1, j):
        if a >=0 and b >=0 and a < n and b < n and rows[a][b] == expected:
            next = sentence.copy()
            next.insert(0, (a, b))
            res = complete(next)
            if(res): return res
    return False

for s in starts:
    positions = complete([s])
    if positions:
        for (i, row) in enumerate(rows):
            for j in range(n):
                if (i, j) in positions: print(row[j], end = "")
                else: print("-", end = "")
            print("")