object Sudoku {

  def isValid(board: Array[Array[Int]]): Boolean = 
    Seq(board, board.transpose, blocks(board))
      .flatten
      .forall(_.toSet == (1 to 9).toSet)

  def blocks(board: Array[Array[Int]]): Array[Array[Int]] = 
    for {
      i <- Array(0, 3, 6)
      j <- Array(0, 3, 6)
    } yield board.slice(i, i + 3).flatMap(_.slice(j, j + 3))    
}

