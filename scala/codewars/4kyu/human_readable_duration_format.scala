object HumanTime {

  def formatDuration(seconds: Int): String = 
    if(seconds <= 0) "now"
    else {
      val (_, large :+ smallest) = List(("second", 60), ("minute", 60), ("hour", 24), ("day", 365), ("year", Int.MaxValue))
        .foldLeft((seconds, List.empty[String])) {
          case ((count, labels), (name, size)) => 
            val (div, mod) = (count / size, count % size)
            (div,
              if(mod == 0) labels
              else s"$mod $name${"s".drop(2 - mod)}" :: labels)              
        }
      large.mkString(", ") + (if(large.nonEmpty) " and " else "") + smallest
  }
}
