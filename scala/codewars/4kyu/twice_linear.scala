object DblLinear {
  val values = sequence(19).distinct.sorted
  
  def dblLinear(n: Int): Int = {
    values(n)
  }
  
  def sequence(level:Int, next: Vector[Int] = Vector(1), acc: Vector[Int] = Vector.empty): Vector[Int] = 
    if(level == 0) acc
    else sequence(level - 1, next.flatMap(generate), acc ++ next)
    
  def generate(e: Int): Vector[Int] =
    Vector(2 * e + 1, 3 * e + 1)
}
