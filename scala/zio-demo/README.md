# Graceful shutdown with ZIO

    sbt assembly
    java -jar  target/scala-2.13/zio-demo-assembly-0.1.0-SNAPSHOT.jar  

When you hit Ctrl-C, the signal is handled before the program dies.

Note : we use `sbt assembly` rather than sbt run to be sure that the signal is handled by our app, not by sbt.
