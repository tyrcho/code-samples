ThisBuild / organization := "com.example"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion := "2.13.4"

lazy val root = (project in file("."))
  .settings(
    name := "zio-demo",
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio" % "1.0.13",
      "dev.zio" %% "zio-interop-cats" % "3.2.9.1",
      "org.http4s" %% "http4s-jetty" % "1.0-234-d1a2b53",
      "org.http4s" %% "http4s-dropwizard-metrics" % "1.0-234-d1a2b53"
    )
  )
