import zio._
import zio.duration._

import java.time.LocalTime
//import cats.effect._
//import com.codahale.metrics.MetricRegistry
//import org.http4s.jetty.server.JettyBuilder
//import org.http4s.metrics.dropwizard._
//import org.http4s.server.HttpMiddleware
//import org.http4s.server.Server
//import org.http4s.server.jetty.JettyBuilder
//import org.http4s.server.middleware.Metrics

object Demo extends zio.App {


  override def run(args: List[String]) = {
    ((printWithTime("long running job...") *> clock.sleep(100.seconds)).forkDaemon *>
      printWithTime("do stuff").repeat(Schedule.spaced(1.seconds)))
      .ensuring(handleShutdown.orDie)
      .exitCode
  }

  val handleShutdown =
    printWithTime("trying to finalize")
      .repeat(Schedule.spaced(1.seconds) && Schedule.recurs(2))


  private def printWithTime(msg: String) =
    console.putStrLn(s"${LocalTime.now().toString} $msg")
//
//
//  JettyBuilder[F]
//    .bindHttp(8080)
//    .mountService(metrics(ExampleService[F].routes), "/http4s")
//    .mountService(metricsService(metricsRegistry), "/metrics")
//    .mountFilter(NoneShallPass, "/black-knight/*")
//}
//
//def resource[F[_]: Async]: Resource[F, Server] =
//  builder[F].resource
}
