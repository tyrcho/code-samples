import math._
import scala.util._
import scala.annotation.tailrec

object Player extends App {
  val width = readInt
  val height = readInt
  val count = readInt

  Console.err.println((width, height, count))

  var round = 1
  var oldPlayer: Option[Point] = None
  var board = Board(width, height, Map.empty)
  var oldEnemies = Vector.empty[Point]
  // game loop
  while (true) {
    val neighbours = Vector.fill(4)(readNeighbour())
    val enemies = Vector.fill(count - 1)(readPoint())
    val player = readPoint()
    Console.err.println(s"old : $oldPlayer, new : $player")
    Console.err.println(neighbours)
    Console.err.println(player)
    Console.err.println(enemies)

    val m = oldPlayer.flatMap { o =>
      if (player == o.N) Some(N)
      else if (player == o.E) Some(E)
      else if (player == o.S) Some(S)
      else if (player == o.W) Some(W)
      else None
    }

    board = board.update(oldEnemies, enemies, oldPlayer.getOrElse(player), player, neighbours)
    board.debug()

    println(board.nextMove)
    round += 1
    oldPlayer = Some(player)
    oldEnemies = enemies

  }

  case class Point(x: Int, y: Int) {
    def N = check(Point(x, y - 1))
    def S = check(Point(x, y + 1))
    def E = check(Point(x + 1, y))
    def W = check(Point(x - 1, y))

    def check(p: Point): Point = {
      if (p.x == -1) Point(height - 1, p.y)
      else if (p.x == height) Point(0, p.y)
      else if (p.y == -1) Point(p.x, width)
      else if (p.y == width) Point(p.x, 0)
      else p
    }

    def neighbours = List(N, S, E, W)

    def dist(o: Point) =
      (o.x - x) * (o.x - x) + (o.y - y) * (o.y - y)

    def closer(ref: Point)(o: Point) = {
      val res = dist(ref) < o.dist(ref)
      //      Console.err.println(s"$this closer than $o to $ref ? $res")
      res
    }
  }

  def readPoint() = {
    val Array(x, y) = readLine split " "
    Point(x.toInt, y.toInt)
  }

  def readNeighbour() = readLine() match {
    case "_" => true
    case "#" => false
  }

  trait Direction {
    def apply(o: Point): Point
  }
  object N extends Direction {
    def apply(p: Point) = p.N
  }
  object S extends Direction {
    def apply(p: Point) = p.S
  }
  object E extends Direction {
    def apply(p: Point) = p.E
  }
  object W extends Direction {
    def apply(p: Point) = p.W
  }

  case class Board(
      width: Int,
      height: Int,
      data: Map[Point, Cell],
      playerTrail: List[Point] = Nil,
      enemies: Vector[Point] = Vector(),
      oldEnemies: Vector[Point] = Vector()) {

    def update(
      oldEnemies: Vector[Point],
      newEnemies: Vector[Point],
      oldPlayer: Point,
      newPlayer: Point,
      neighbours: Vector[Boolean]) = {

      val newData = data ++
        neighbours.zip(Vector(N, E, S, W)).map {
          case (b, d) =>
            val p = d.apply(newPlayer)
            p -> (if (b) data.getOrElse(p, Pill)
            else Wall)
        } ++
        oldEnemies.map(p => p -> (if (data(p).asInstanceOf[Enemy].overPill) Pill else Free)) ++
        Vector(oldPlayer -> Free, newPlayer -> Player) ++
        newEnemies.zipWithIndex.map { case (p, i) => p -> Enemy(i + 1, !data.get(p).exists(c => c == Free || c == Player || c.isInstanceOf[Enemy])) }

      copy(data = newData, playerTrail = oldPlayer :: playerTrail, enemies = newEnemies, oldEnemies = oldEnemies)
    }

    def nextMove = {
      val scoredMoves = validMoves(player).map(m => m -> score(m))
      moveChar(scoredMoves.sortBy(_._2).headOption.map(_._1))
    }

    val fibo: Stream[Int] = 0 #:: fibo.scanLeft(1)(_ + _)

    def score(move: Point) = {
      val threats = for {
        (e, i) <- enemies.zipWithIndex
        eId = i + 1
        if oldEnemies.nonEmpty
        oldE = oldEnemies(i)
        if e.closer(move)(oldE) && oldE != e
        dist <- path(move, { _ match { case Enemy(`eId`, _) => true; case _ => false } }).filterNot(_.contains(player)).map(_.length)
        if dist < 12
      } yield (dist - 2, eId)
      val isShortestWayToPill = directionToClosestPill.contains(move)
      val gain = if (isShortestWayToPill) -3 else 0
      val recentTrail = playerTrail.take(10)
      val trailPenalty = if (recentTrail.contains(move)) 20 - recentTrail.indexOf(move) else 0
      val score = gain + trailPenalty + (threats.toList.sorted.headOption match {
        case Some(close) => 1 << (12 - close._1)
        case None        => 0
      })
      Console.err.println(f"score for $move is $score : threats $threats way to pill : $isShortestWayToPill")
      score
    }

    def oldscore(move: Point) = {
      val enemyDistance = pathToEnemy(move).map(p => p.size - 2)
      val risk = enemyDistance.map(d => fibo(10 - d)).getOrElse(0)
      val isShortestWayToPill = directionToClosestPill.contains(move)
      val gain = if (isShortestWayToPill) 3 else 0
      val recentTrail = playerTrail.take(10)
      val trailPenalty = if (recentTrail.contains(move)) 20 - recentTrail.indexOf(move) else 0
      val score = risk + trailPenalty - gain
      Console.err.println(f"score for $move is $score : risk $risk + trail $trailPenalty - gain $gain (enemy at $enemyDistance and pill : $isShortestWayToPill)")
      score
    }

    def nextMoveCareful = {
      val cell = enemiesDirections.size match {
        case 0 =>
          val pill = directionToClosestPill
          Console.err.println(s"no close enemies, going to pill $pill")
          pill
        case _ =>
          val freeMove = (validMoves(player).toSet -- enemiesDirections.keys).headOption
          val fartherEnemy = enemiesDirections.keys.maxBy(enemiesDirections)
          Console.err.println(s"possible freeMove :$freeMove")
          freeMove.orElse {
            Console.err.println(s"no free move, going toward farther enemy $fartherEnemy")
            Some(fartherEnemy)
          }
      }
      moveChar(cell)
    }

    def moveChar(to: Option[Point]): Char = {
      val cells = for {
        (p, m) <- List(player.S, player.W, player.N, player.E) zip List('D', 'E', 'C', 'A')
        if Some(p) == to
      } yield m
      cells.headOption.getOrElse('B')
    }

    lazy val player = data.flatMap {
      case (p, Player) => Some(p)
      case _           => None
    }.head

    type Path = List[Point]

    def validMoves(p: Point) = for {
      m <- List(N, S, E, W)
      t = m(p)
      cell = data.get(t)
      if cell != Some(Wall)
    } yield t

    @tailrec
    private def pathImpl(
      toExplore: List[(Point, Path)],
      explored: Set[Point],
      condition: Point => Boolean): Option[Path] =

      toExplore match {
        case Nil                                => None
        case (to, path) :: _ if condition(to)   => Some(path)
        case (_, path) :: _ if path.length > 10 => None
        case (p, path) :: t =>
          def alreadyQueued(n: Point) = toExplore.exists {
            case (`n`, _) => true
            case _        => false
          }

          val newPoints = for {
            m <- validMoves(p)
            if !alreadyQueued(m)
            if !explored.contains(m)
          } yield (m, m :: path)
          pathImpl(t ::: newPoints, explored + p, condition)
      }

    lazy val enemiesDirections: Map[Point, Int] = {
      val e = for {
        (direction, paths) <- enemies.map(pathFromEnemy).flatten.groupBy { path => path(1) }
        turns = paths.map(_.length).min - 2
        if turns < 15
      } yield direction -> turns
      Console.err.println(s"enemies directions : $e")
      e
    }

    def pathFromEnemy(e: Point) = {
      val p = path(e, Player).map(_ ::: List(e))
      Console.err.println(s"path from enemy $e to player $player : \n$p")
      p
    }

    def pathToEnemy(e: Point) = {
      val p = pathImpl(List((e, Nil)), Set.empty, data.get(_) match { case Some(en: Enemy) => true; case _ => false })
      Console.err.println(s"path from point $e to enemy : \n$p")
      p
    }

    def path(from: Point, to: Cell) =
      pathImpl(List((from, Nil)), Set.empty, data.get(_) == Some(to))

    def path(from: Point, test: Cell => Boolean) =
      pathImpl(List((from, Nil)), Set.empty, p => data.get(p).map(test).getOrElse(false))

    lazy val pathToPill = path(player, Pill)

    lazy val directionToClosestPill =
      pathToPill.map { p =>
        val path = p.reverse
        Console.err.println(s"path to closest pill : $path")
        path.head
      }

    def cellAt(p: Point) = data(p)

    def debug() = {
      for {
        x <- 0 until height
      } {
        for {
          y <- 0 until width
          c = data.get(Point(x, y)).map(_.char).getOrElse('?')
        } Console.err.print(c)
        Console.err.println()
      }
    }
  }

  trait Cell {
    def char: Char
  }
  case class Enemy(id: Int, overPill: Boolean) extends Cell {
    val char = id.toString.head
  }
  case object Player extends Cell {
    val char = '0'
  }
  case object Free extends Cell {
    val char = ' '
  }
  case object Pill extends Cell {
    val char = '.'
  }
  case object Wall extends Cell {
    val char = '#'
  }

}
