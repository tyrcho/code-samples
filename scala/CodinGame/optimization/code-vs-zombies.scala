import math._
import scala.util._

object Player extends App {
  val ashSpeed = 1000
  val ashRange = 2000
  val zombieSpeed = 400

  // game loop
  while (true) {
    val Array(x, y) = for (i <- readLine split " ") yield i.toInt
    val humancount = readInt
    val humans = for (i <- 0 until humancount) yield {
      val Array(humanid, humanx, humany) = for (i <- readLine split " ") yield i.toInt
      Human(humanid, humanx, humany)
    }
    val ash = Point(x, y)
    val zombiecount = readInt
    val zombies = for (i <- 0 until zombiecount) yield {
      val Array(zombieid, zombiex, zombiey, zombiexnext, zombieynext) = for (i <- readLine split " ") yield i.toInt
      Zombie(zombieid, zombiex, zombiey, zombiexnext, zombieynext)
    }

    val state = State(zombies.toList, ash, humans.toList)
    import state._

    println(closer.str)
    def score(z: Zombie) = -notDoomed.map(1.0 / z.dist(_)).sum
  }

  def bary(points: Iterable[Point]): Point = {
    val sum = points.reduce(_ + _)
    sum / points.size
  }

  case class State(zombies: List[Zombie], ash: Point, humans: List[Human]) {

    val threats = realThreats()

    val (doomed, notDoomed) = humans.partition { h =>
      val zombieTurns = threats.find(_.human == h).get.turns
      val ashTurns = h.turnsForAsh(ash)
      Console.err.println(s"Ash reaches ${h.id} in $ashTurns turns")
      zombieTurns < ashTurns -1
    }
    Console.err.println(s"doomed:$doomed")

    def distToHuman(z: Zombie) = notDoomed.map(z.next.dist(_)).min

    val sorted = zombies.sortBy(distToHuman)
    val closer = sorted.map(_.next).inits.find { zombies => inRange(zombies, ash) } match {
      case Some(group) if group.size > 1 =>
        Console.err.println(group)
        bary(group)
      case _ =>
        val closerZombie = sorted.head.point
        notDoomed.minBy(_.dist(closerZombie)).point
        closerZombie
    }

    def realThreats(humansLeft: List[Human] = humans, zombiesMoved: List[Zombie] = zombies, turn: Int = 0, threats: List[Threat] = Nil): List[Threat] = humansLeft match {
      case Nil => threats
      case humans =>
        val newZombies =
          for {
            z <- zombiesMoved
            target = humansLeft.minBy(z.dist(_))
            point = z.move(target, zombieSpeed)
          } yield z.copy(x = point.x, y = point.y)
        val eaten = humans.filter(h => newZombies.exists(_.point == h.point))
        val live = humans.diff(eaten)
        val newThreats = eaten.map(h => Threat(h, newZombies.find(_.point == h.point).get, turn))
        realThreats(live, newZombies, turn + 1, threats ++ newThreats)
    }
  }

  case class Threat(human: Human, zombie: Zombie, turns: Int) {
    Console.err.println(s"zombie ${zombie.id} will get human ${human.id} in $turns turns")
  }

  def inRange(points: Iterable[Point], ash: Point) =
    points.nonEmpty && points.forall(_.dist(ash) < ashRange + ashSpeed)

  case class Zombie(id: Int, x: Int, y: Int, xn: Int, yn: Int) {
    def point = Point(x, y)
    def next = Point(xn, yn)
  }

  case class Human(id: Int, x: Int, y: Int) {
    def point = Point(x, y)

    def dist(p: Point) = sqrt(sqr(x - p.x) + sqr(y - p.y))

    def turnsLeft(zombies: Iterable[Zombie]): (Zombie, Int) = {
      val closer = zombies.minBy(z => point.dist(z.next))
      val dist = closer.dist(point)
      val turns = (dist / zombieSpeed).toInt
      (closer, turns)
    }

    def turnsForAsh(ash: Point): Int = {
      val da = dist(ash) - ashRange
      (da / ashSpeed).toInt +1
    }

    //    def isDoomed(ash: Point, zombies: Iterable[Zombie]): Boolean =
    //      turnsLeft(zombies)._2 <= turnsForAsh(ash)
  }

  case class Point(x: Int, y: Int) {
    def dist(p: Point) = sqrt(sqr(x - p.x) + sqr(y - p.y))

    def +(p: Point) = Point(x + p.x, y + p.y)

    def /(r: Double) = Point((x / r).toInt, (y / r).toInt)

    def str = s"$x $y"

    def move(to: Point, speed: Int) = {
      if (dist(to) <= speed) to
      else {
        val vect = Point(to.x - x, to.y - y)
        val ratio = vect.dist(Point(0, 0)) / speed
        val vs = vect / ratio
        this + vs
      }
    }
  }

  implicit def toPoint(h: Human): Point = h.point
  implicit def toPoint(z: Zombie): Point = z.point

  def sqr(x: Int) = x * x
}
