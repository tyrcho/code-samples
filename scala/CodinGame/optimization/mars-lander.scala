import math._
import scala.util._

object Player extends App {

  val marsSurfaceSize = readInt
  val ground = (0 until marsSurfaceSize).map { i =>
    val Array(land_x, land_y) = for (i <- readLine split " ") yield i.toInt
    Point(land_x, land_y)
  }

  val (targetLeft, targetRight) = ground.sliding(2)
    .filter { case Seq(a, b) => a.y == b.y }
    .map { case Seq(a, b) => (a.x, b.x) }
    .next

  var center = targetLeft + (targetRight - targetLeft) / 2

  while (true) {
    val Array(x, y, dx, dy, _, _, _) = for (i <- readLine split " ") yield i.toInt

    val angle =
      if (x < targetLeft && dx > 70) 30
      else if (x < targetLeft && dx > 20) -10
      else if (x < targetLeft) -38
      else if (x > targetRight && dx < -70) -30
      else if (x > targetRight && dx < -20) 10
      else if (x > targetRight) 38
      else if (dx > 3) 30
      else if (dx < -3) -30
      else 0

    val power =
      if (dy >= -30 && angle == 0) 0
      else 4

    println(s"$angle $power")
  }
}

case class Point(x: Int, y: Int)
