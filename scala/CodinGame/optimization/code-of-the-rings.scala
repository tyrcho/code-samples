import math._
import scala.util._
import scala.annotation.tailrec

object Player extends App {
  val magicphrase = readLine
  val chars = (' ' :: ('A' to 'Z').toList).toArray
  val count = chars.size
  def norm(i: Int) = (i + count) % count
  val wheelCount = 30
  def wnorm(i: Int) = (i + wheelCount) % wheelCount

  case class Wheel(index: Int = 0, id: Int) {
    def forward = copy(norm(index + 1))
    def backwards = copy(norm(index - 1))

    def char = chars(index)

    def moveTo(c: Char) = {
      val i = chars.indexOf(c)
      val move1 = norm(i - index)
      val move2 = -27 + move1
      //      Console.err.println(s"current : $index, to: $i")
      //      Console.err.println(s"m1 : $move1, m2 : $move2")
      if (abs(move1) < abs(move2))
        move1
      else
        move2
    }
  }

  case class State(
      phrase: List[Char] = magicphrase.toList,
      wheels: List[Wheel] = (0 until 30).toList.map(i => Wheel(id = i)),
      pos: Int = 0,
      out: String = "") {

    val wheel = wheels(pos)

    @tailrec final def encode: State = phrase match {
      case Nil => this
      case h :: t =>
        if (phrase.containsSlice(List.fill(26)(h))) {
          val head = phrase.takeWhile(_ == h)
          val s = min(head.size, 26)
          val tail = phrase.drop(s)
          Console.err.println(s"loop : $h * $s")
          loop26(h, tail).encode
        } else if (wheel.char == h) State(t, wheels, pos, out + ".").encode
        else wheels.find(_.char == h) match {
          case Some(w) if wnorm(w.id - pos) <= wheelCount / 2 => moveUp.encode
          case Some(w)                                       => moveDown.encode
          case None =>
            val doublons = wheels.filter(w => wheels.count(_.char == w.char) > 1 || !phrase.contains(w.char))
            val closestDoublon = doublons.minBy(w => abs(pos - w.id)).id
            closestDoublon match {
              case w if w == wheel.id => //already on the most frequent char
                val next = if (wheel.moveTo(h) > 0) forward else backwards
                next.encode
              case w if wnorm(w - pos) < wheelCount / 2 => moveUp.encode
              case w                                    => moveDown.encode
            }
        }
    }

    def charToInstructions(c: Char) = {
      val count = Wheel(id = -1).moveTo(c)
      (if (count > 0) "+"
      else "-") * abs(count)
    }

    def loop26(char: Char, tail: List[Char]): State = {
      val instructions =
        (if (wheel.char != ' ') "[>]" else "") + // move to a space
          charToInstructions(char) +
          ">-[<.>-]" // repeat 26
      copy(
        phrase = tail,
        out = out + instructions,
        wheels = updateWheel(_.copy(index = 0)))
    }

    def moveUp = copy(pos = wnorm(pos + 1), out = out + ">")
    def moveDown = copy(pos = wnorm(pos - 1), out = out + "<")

    def forward = copy(wheels = updateWheel(_.forward), out = out + "+")
    def backwards = copy(wheels = updateWheel(_.backwards), out = out + "-")

    def updateWheel(f: Wheel => Wheel) = wheels.updated(pos, f(wheel))

  }

  println(State().encode.out)
}
