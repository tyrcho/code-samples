import acraft._

import scala.annotation.tailrec

object Player extends App {

  val lines = Array.fill(10)(readLine.toUpperCase)
  val robots = List.fill(readInt) {
    val Array(_x, _y, direction) = readLine split " "
    Robot(Position(_x.toInt, _y.toInt), direction match {
      case "L" => Left
      case "R" => Right
      case "U" => Up
      case "D" => Down
    })
  }

  val board = GameBoard(lines, robots)
  val moves = board.improve(untilMs = System.currentTimeMillis() + 750)

  println(moves.mkString(" "))

}

package acraft {
  object Constants {
    val SEARCH_DEPTH = 200
    val rowCount     = 10
    val rows         = 0 until rowCount
    val colCount     = 19
    val columns      = 0 until colCount

    val emptyBoard = GameBoard(Array.fill(rowCount)("." * colCount), Nil)
  }

  import acraft.Constants._

  case class GameBoard(lines: Array[String], robots: List[Robot], score: Int = 0) {

    @tailrec
    final def improve(selectedMoves: List[Move] = Nil, maxScore: Int = 0, untilMs: Long = Long.MaxValue): List[Move] =
      if (System.currentTimeMillis() > untilMs) selectedMoves
      else {
        Console.err.println(s"${untilMs - System.currentTimeMillis()} ms left, improving")
        val best    = possibleMoves.maxBy(m => play(m).futureScore)
        val newBest = play(best).futureScore
        if (newBest > maxScore)
          play(best).improve(best :: selectedMoves, newBest, untilMs)
        else selectedMoves
      }

    def futureScore: Int = nextSteps(SEARCH_DEPTH).score

    def cellAt(p: Position): Char =
      lines(p.y)(p.x)

    def isVoid(p: Position): Boolean  = cellAt(p) == '#'
    def isEmpty(p: Position): Boolean = cellAt(p) == '.'

    def nextSteps: Stream[GameBoard] = Stream.iterate(this)(_.next)

    def play(m: Move): GameBoard = {
      val col  = m.position.x
      val row  = m.position.y
      val line = lines(row).updated(col, m.direction.toString.head)
      copy(lines = lines.updated(row, line))
    }

    def possibleMoves: Seq[Move] =
      for {
        x <- columns
        y <- rows
        p = Position(x, y)
        if isEmpty(p)
        d <- Seq(Left, Right, Up, Down)
      } yield Move(p, d)

    def nextPos(robot: Robot): Option[Robot] = {
      val next =
        if (score == 0) turn(robot).next.copy(past = List(robot))
        else robot.next
      val turned = turn(next)
      if (isVoid(turned.position) || turned.hasLooped) None
      else Some(turned)
    }

    private def turn(robot: Robot): Robot = {
      robot.copy(direction = direction(robot.position).getOrElse(robot.direction))
    }
    def direction(p: Position): Option[Direction] = cellAt(p) match {
      case 'U' => Some(Up)
      case 'D' => Some(Down)
      case 'L' => Some(Left)
      case 'R' => Some(Right)
      case _   => None
    }

    def next: GameBoard =
      if (robots.isEmpty) this
      else {
        val nextRobots = robots.flatMap(nextPos)
        copy(robots = nextRobots, score = score + nextRobots.size)
      }

    override def toString: String = s"$score $robots"
  }

  case class Position(x: Int, y: Int) {
    def next(direction: Direction): Position = direction match {
      case Up    => copy(y = (y - 1 + rowCount) % rowCount)
      case Left  => copy(x = (x - 1 + colCount) % colCount)
      case Down  => copy(y = (y + 1)            % rowCount)
      case Right => copy(x = (x + 1)            % colCount)
    }
  }

  case class Robot(position: Position, direction: Direction, past: List[Robot] = Nil) {
    def next: Robot = copy(position.next(direction), past = this :: past)

    override def toString: String = s"Robot($position, $direction)"

    def hasLooped: Boolean = past.exists(r => r.position == position && r.direction == direction)
  }

  case class Move(position: Position, direction: Direction) {
    override def toString: String = s"${position.x} ${position.y} $direction"
  }

  sealed trait Direction {
    val opposite: Direction
  }
  case object Up extends Direction {
    override val toString            = "U"
    override val opposite: Direction = Down
  }
  case object Down extends Direction {
    override val toString            = "D"
    override val opposite: Direction = Up
  }
  case object Left extends Direction {
    override val toString            = "L"
    override val opposite: Direction = Right

  }
  case object Right extends Direction {
    override val toString            = "R"
    override val opposite: Direction = Left

  }
}
