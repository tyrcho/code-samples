import math._
import scala.util._
import io.StdIn._

object Player extends App {
  val bustersperplayer = readInt // the amount of busters you control
  val ghostcount = readInt // the amount of ghosts on the map
  val myteamid = readInt // if this is 0, your base is on the top left of the map, if it is one, on the bottom right

  val config = ConfigGB(bustersperplayer, ghostcount, myteamid)
  val initialFog = FogGrid(width = config.width, height = config.height, scale = 100)
  var state = GameState(config, initialFog)
  var startTime = 0L

  // game loop
  while (true) {
    state = state.read()
    debug(state.myBusters)
    debug(state.enBusters)
    debug(state.ghosts)
    val moves = state.moves.toVector.sortBy(_._1.id).map(_._2)
    moves.foreach(println)
    val updates = State.sequence(moves.map(m => State.modify(m.apply)))
    state = updates(state)._1
  }

  case class GameState(
      config: ConfigGB,
      fog: FogGrid,
      entities: Set[Entity] = Set()) {

    import config._

    lazy val myBusters = entities.filter(_.kind == Friend)
    lazy val enBusters = entities.filter(_.kind == Enemy)
    lazy val ghosts = entities.filter(_.kind == Ghost).toVector

    def moves = orders.eval(myBusters).reduce(_ ++ _)

    def orders =
      State.sequence(Seq(releaseOrders, stunOrders, returnOrders, cornerOrders, ghostOrders, fogOrders))

    private def shouldStun(stunner: Entity, b: Entity) =
      b.timeInFog == 0 && stunner.timeInFog == 0 &&
        b.turnsStunnedLeft <= 1 &&
        !stunner.stunned &&
        !stunner.onCoolDown &&
        (b.pos dist2 stunner.pos) < sqr(stunRange)

    // me -> en
    def stunOrders: SelectOrders = State { busters: Set[Entity] =>
      // enemy => my stunners
      val possibleStunsByEn = enBusters.map(e => e -> myBusters.filter(m => shouldStun(m, e)))
      // my stunner => enemies
      val possibleStunsByMy = busters.map(m => m -> enBusters.filter(e => shouldStun(m, e))).toMap

      val myStuns = possibleStunsByEn.toVector.sortBy(_._2.size).foldLeft(Map.empty[Entity, Entity]) {
        case (decision, (en, myStunners)) =>
          myStunners.filterNot(decision.contains).toVector.sortBy(m => possibleStunsByMy(m).size).headOption match {
            case Some(stunner) => decision.updated(stunner, en)
            case None          => decision
          }
      }

      val (stunning, free) = busters.partition(myStuns.contains)
      (free, stunning.map(my => my -> Stun(myStuns(my).id)).toMap)
    }

    def cornerOrders: SelectOrders = State { busters: Set[Entity] =>
      val corners = Seq(closeCorner, farCorner, CENTER).take(bustersPerPl - 1)
      val cornersToExplore = corners.filter(c => fog.age(c) > 1000)
      cornersToExplore.foldLeft((busters, Map.empty[Entity, BusterOrder])) {
        case ((freeBusters, orders), corner) =>
          freeBusters.minOption(_.pos.dist2(corner)) match {
            case None          => (freeBusters, orders)
            case Some(closest) => (freeBusters - closest, orders + (closest -> Move(corner.moveTo(CENTER, sight), "corner")))
          }
      }
    }

    def ghostOrders: SelectOrders = State { busters: Set[Entity] =>
      def distToBusters(g: Entity): Double = busters.map(_.pos.dist2(g.pos)).minOption(identity).getOrElse(0)
      ghosts.sortBy(distToBusters).foldLeft((busters, Map.empty[Entity, BusterOrder])) {
        case ((freeBusters, orders), ghost) =>
          freeBusters.minOption(_.pos.dist2(ghost.pos)) match {
            case None                                     => (freeBusters, orders)
            case Some(closest) if canBust(closest, ghost) => (freeBusters - closest, orders + (closest -> Bust(ghost)))
            case Some(closest)                            => (freeBusters - closest, orders + (closest -> moveToCapture(ghost)))
          }
      }
    }

    def moveToCapture(g: Entity) =
      if (g.timeInFog > 0) Move(g.pos, s"capture ${g.id}")
      else Move(g.pos.moveTo(myBase, (maxBustRange + minBustRange) / 2), s"capture ${g.id}")

    def returnOrders: SelectOrders = State { busters: Set[Entity] =>
      val (full, empty) = busters.partition(_.hasGhost)
      val orders = full.map(b => b -> Move(myBase, "back")).toMap
      (empty, orders)
    }

    def releaseOrders: SelectOrders = State { busters: Set[Entity] =>
      val (ready, not) = busters.partition(b => b.hasGhost && b.pos.dist(myBase) <= releaseRange)
      val orders = ready.map(b => b -> Release).toMap
      (not, orders)
    }

    def fogOrders: SelectOrders = State { busters: Set[Entity] =>
      fog.olderThan(15).foldLeft((busters, Map.empty[Entity, BusterOrder])) {
        case ((freeBusters, orders), fogPos) =>
          freeBusters.minOption(_.pos.dist2(fogPos)) match {
            case None          => (freeBusters, orders)
            case Some(closest) => (freeBusters - closest, orders + (closest -> Move(fogPos, "clear fog")))
          }
      }
    }

    type SelectOrders = State[Set[Entity], Map[Entity, BusterOrder]]

    def read() = {
      val count = readInt // the number of busters and ghosts visible to you
      val ent = (for (i <- 0 until count) yield {
        // entityid: buster id or ghost id
        // y: position of this buster / ghost
        // entitytype: the team id if it is a buster, -1 if it is a ghost.
        // state: For busters: 0=idle, 1=carrying a ghost.
        // value: For busters: Ghost id being carried. For ghosts: number of busters attempting to trap this ghost.
        val Array(entityid, x, y, entitytype, state, value) = for (i <- readLine split " ") yield i.toInt
        val kind = config.kind(entitytype)
        val r = Entity(
          id = entityid,
          pos = Pos(x, y),
          kind = kind,
          carriedGhost = if (state == 1) Some(value) else None,
          stunned = state == 2,
          turnsStunnedLeft = value)
        entities.find(r.same) match {
          case None if r.kind == Ghost && r.id != 0 =>
            val guess = r.symmetric(CENTER)
            debug(s"Guessed a ghost $guess")
            Seq(r, guess)
          case None      => Seq(r)
          case Some(old) => Seq(old.update(r).copy(timeInFog = 0))
        }
      }).flatten.toSet
      val keepUnseen = entities.collect {
        case e if !ent.exists(e.same) => e.nextTurn
      }
      val nextEnt = (ent ++ keepUnseen).filterNot(_.ghostCaptured(ent))
      //TODO : symetric
      startTime = now
      copy(entities = nextEnt).updateFog
    }

    private def guessInitialGhosts(initGhosts: Set[Entity]) = {
      val guessed = for {
        g <- initGhosts
        id = g.id
        if id != 0
        guessedId = if (id % 2 == 0) id - 1 else id + 1
        if !initGhosts.exists(g => g.id == guessedId)
        guessedPos = g.pos.symmetric(CENTER)
      } yield Entity(guessedId, guessedPos, Ghost, stamina = g.stamina, timeInFog = Int.MaxValue / 2)
      guessed.foreach { g => debug(s"Guessed a ghost $g") }
      guessed
    }

    def updateFog = copy(fog = fog.updated(myBusters.map(_.pos), config.sight))
  }

  case class Entity(
      id: Int,
      pos: Pos,
      kind: Kind,
      stamina: Int = 0,
      carriedGhost: Option[Int] = None,
      stunned: Boolean = false,
      turnsStunnedLeft: Int = 0,
      stunCoolDown: Int = 0,
      timeInFog: Int = 0) {

    val hasGhost = carriedGhost.isDefined

    val onCoolDown = stunCoolDown > 0

    def symmetric(center: Pos) = copy(
      id = if (id % 2 == 0) id - 1 else id + 1,
      pos = pos.symmetric(center))

    def update(read: Entity) =
      read.copy(
        turnsStunnedLeft = turnsStunnedLeft,
        stunCoolDown = stunCoolDown).nextTurn

    def nextTurn = copy(
      turnsStunnedLeft = turnsStunnedLeft - 1,
      stunned = turnsStunnedLeft > 1,
      stunCoolDown = stunCoolDown - 1,
      timeInFog = timeInFog + 1)

    def same(e: Entity) = id == e.id && kind == e.kind

    def ghostCaptured(entities: Iterable[Entity]) =
      kind == Ghost && (
        ghostCapturedByAnyBuster(entities) ||
        ghostCapturedByOpp(entities.filter(_.kind == Friend).map(_.pos)))

    private def ghostCapturedByAnyBuster(entities: Iterable[Entity]) =
      entities.find(_.carriedGhost == Some(id)) match {
        case None => false
        case Some(e) =>
          debug(s"$id is captured by ${e.id}")
          true
      }

    private def ghostCapturedByOpp(myBusterPositions: Iterable[Pos]) =
      timeInFog > 0 &&
        myBusterPositions.exists(_.dist(pos) < 400)

  }

  sealed trait Kind
  case object Ghost extends Kind
  case object Friend extends Kind
  case object Enemy extends Kind

  case class Pos(x: Double, y: Double) {
    def +(p: Pos) = Pos(x + p.x, y + p.y)
    def -(p: Pos) = Pos(x - p.x, y - p.y)
    def *(r: Double) = Pos(x * r, y * r)
    def /(r: Double) = Pos(x / r, y / r)
    def scalar(p: Pos) = x * p.x + y * p.y

    def norm: Double = dist(Pos(0, 0))
    def dist(other: Pos): Double = sqrt(dist2(other))
    def dist2(other: Pos): Double = sqr(this.x - other.x) + sqr(this.y - other.y)

    // point of line a-b closest to this
    def closest(a: Pos, b: Pos) = {
      val da = b.y - a.y
      val db = a.x - b.x
      val c1 = db * a.x + db * a.y
      val c2 = -db * x + da * y
      val det = sqr(da) + sqr(db)
      if (det != 0)
        Pos((da * c1 - db * c2) / det, (da * c2 + db * c1) / det)
      else Pos(x, y)
    }

    def moveTo(tgt: Pos, distance: Int) = {
      val direction = tgt - this
      val distToTgt = sqrt(direction scalar direction)
      val delta = direction * (distance / distToTgt)
      this + delta
    }

    def symmetric(center: Pos) = center + center - this

    def round = Pos(x.toInt, y.toInt)
  }
  def sqr(x: Double) = x * x

  implicit class MinMaxOptionIterable[T](it: Iterable[T]) {

    def minOption[O: Ordering](by: T => O): Option[T] =
      if (it.isEmpty) None
      else Some(it.minBy(by))
  }

  case class FogGrid(
      width: Int = 1000,
      height: Int = 1000,
      scale: Int = 100,
      initialAge: Int = Int.MaxValue / 2,
      ages: Map[(Int, Int), Int] = Map.empty.withDefaultValue(Int.MaxValue / 2)) {

    def age(p: Pos) = ages(posToSpot(p))

    def updated(seen: Iterable[Pos], range: Double) = {
      def isSeen(p: Pos) = seen.exists(_.dist2(p) <= range * range)
      val newAges = for {
        (x, y) <- spots
        p = spotToPos(x, y)
        na = if (isSeen(p)) 0 else age(p) + 1
      } yield (x, y) -> na
      copy(ages = newAges.toMap)
    }

    private def posToSpot(p: Pos) = {
      val x = (p * scale / width).x.toInt
      val y = (p * scale / height).y.toInt
      (x, y)
    }

    private def spotToPos(x: Int, y: Int) =
      Pos(x * width / scale, y * height / scale)

    lazy val spots = for {
      i <- 0 until scale
      j <- 0 until scale
    } yield (i, j)

    def olderThan(a: Int): Seq[Pos] = for {
      (x, y) <- spots
      age = ages(x, y)
      if age > a
    } yield spotToPos(x, y)
  }

  case class ConfigGB(
      bustersPerPl: Int,
      ghosts: Int,
      myId: Int,
      sight: Int = 2200,
      stunRange: Int = 1760,
      maxBustRange: Int = 1760,
      minBustRange: Int = 900,
      releaseRange: Int = 1600,
      busterSpeed: Int = 800,
      ghostSpeed: Int = 400,
      width: Int = 16001,
      height: Int = 9001) {

    val MAX_X = width - 1
    val MAX_Y = height - 1
    val CENTER = Pos(width / 2, height / 2)
    val SIGHT_DIAG = (sight / sqrt(2)).toInt

    val (myBase, opBase) = if (myteamid == 0) (Pos(0, 0), Pos(MAX_X, MAX_Y)) else (Pos(MAX_X, MAX_Y), Pos(0, 0))
    val closeCorner = myBase.copy(y = opBase.y)
    val farCorner = myBase.copy(x = opBase.x)
    assert(myBase.dist(closeCorner) < myBase.dist(farCorner))
    val DIAG_LENGTH = myBase dist opBase

    def kind(i: Int): Kind = i match {
      case -1     => Ghost
      case `myId` => Friend
      case _      => Enemy
    }

    def canBust(buster: Entity, ghost: Entity) =
      ghost.timeInFog == 0 &&
        buster.pos.dist(ghost.pos) >= minBustRange &&
        buster.pos.dist(ghost.pos) <= maxBustRange
  }

  sealed trait BusterOrder {
    def apply(state: GameState) = state
  }

  case object Release extends BusterOrder {
    override val toString = "RELEASE"
  }

  case class Move(pos: Pos, msg: String = "") extends BusterOrder {
    override val toString = s"MOVE ${pos.x.toInt} ${pos.y.toInt} $msg"
  }

  case class Bust(ghost: Entity) extends BusterOrder {
    override val toString = s"BUST ${ghost.id}"
  }
  case class Stun(buster: Int) extends BusterOrder {
    override val toString = s"STUN $buster"

    override def apply(state: GameState) = {
      val tgt = state.entities.find(e => e.kind != Ghost && e.id == buster).get
      val stunned = tgt.copy(carriedGhost = None, turnsStunnedLeft = 20)
      state.copy(entities = state.entities - tgt + stunned)
    }
  }

  def now = System.nanoTime / 1000000

  def debug(m: => Any) = Console.err.println(s"${now - startTime} : $m")

  trait State[S, A] {
    val run: S => (S, A)

    def apply(s: S): (S, A) =
      run(s)

    def eval(s: S): A =
      apply(s)._2

    def map[B](f: A => B): State[S, B] = State { s: S =>
      val (s1, a) = run(s)
      (s1, f(a))
    }

    def flatMap[B](f: A => State[S, B]): State[S, B] = State { s: S =>
      val (s1, a) = run(s)
      f(a)(s1)
    }
  }

  object State {

    def apply[S, A](f: S => (S, A)): State[S, A] = new State[S, A] {
      final val run = f
    }

    def state[S, A](a: A): State[S, A] = State { s: S => (s, a) }
    def get[S]: State[S, S] = State { s: S => (s, s) }
    def gets[S, A](f: S => A): State[S, A] = State { s: S => (s, f(s)) }
    def modify[S](f: S => S): State[S, Unit] = State { s: S => (f(s), ()) }

    def reduce[S, A](states: Iterable[State[S, A]]): State[S, A] =
      sequence(states).map(_.last)

    def sequence[S, B](fs: Iterable[State[S, B]]): State[S, Vector[B]] = State(s => sequence_(fs, Vector(), s))
    private def sequence_[S, B](fs: Iterable[State[S, B]], out: Vector[B], nextS: S): (S, Vector[B]) =
      if (fs.isEmpty) (nextS, out)
      else {
        val (h, t) = (fs.head, fs.tail)
        val (s, a) = h(nextS)
        sequence_(t, out :+ a, s)
      }

  }
}