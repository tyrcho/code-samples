import scala.io.StdIn._

object Solution extends App {
  val size = readLine.toInt
  val movesCount = readLine.toInt

  val boardLines = Vector.fill(size) {
    readLine.toVector.map { 
      case 'B' => Some(true)
      case 'W' => Some(false)
      case _ => None
    }
  }

  boardLines.foreach(line => assert(line.size == size, line))

  val moves = Seq.fill(movesCount) {
    val Array(color, row, col) = readLine.split(" ")
    Move(color == "B", row.toInt, col.toInt)
  }

  val initial = Board(boardLines)

  val (_, last) = moves.foldLeft((Option.empty[Board], Option(initial))) {
    case ((old, board), move) => 
      val newBoard = board.flatMap(_.play(move))
      if (newBoard == old) (None, None) // KO
      else (board, newBoard)
  }

  println(last.fold("NOT_VALID")(_.toString))

    case class Move(black: Boolean, row: Int, col: Int)

    final case class Board(rows: Vector[Vector[Option[Boolean]]]) {
      def play(move: Move): Option[Board] = {
        System.err.println(s"recording $move")
        if(rows(move.row)(move.col).nonEmpty) None
        else {
          val row = rows(move.row).updated(move.col, Some(move.black))
          val b = Board(rows = rows.updated(move.row, row))
          b.removeKilled(move.black)
        }
      }

      def killedStones(blackPlayed: Boolean): Set[Pos] = {
        val groups = if(blackPlayed) whiteGroups else blackGroups
        val deadGroups = groups.filter(g => groupIsDead(g, !blackPlayed))
        deadGroups.flatMap(_.elements)
      }

      def removeKilled(blackPlayed: Boolean): Option[Board] = {            
        val dead = killedStones(blackPlayed)
        val suicides = killedStones(!blackPlayed)
        if(suicides.nonEmpty && dead.isEmpty) None
        else if(dead.isEmpty) Some(this)
        else Some {
          val count = dead.size
          System.err.println(s"$count stones captured")
          dead.foldLeft(this) {
            case (board, stone) => board.removeCaptured(stone)
          }
        }
      }

      def removeCaptured(stone: Pos): Board = {
        val row = rows(stone.row).updated(stone.col, None)
        Board(rows = rows.updated(stone.row, row))
      }


      def groupIsDead(group: Group, black: Boolean): Boolean = {
        val oppStones = if(black) whiteStones else blackStones
        val freedoms = group.neighbours
          .filter( p => p.row>=0 && p.col>=0 && p.row<size && p.col<size)
          .diff(oppStones)
          // System.err.println(s"checking $group $oppStones $freedoms")
          freedoms.isEmpty
      }

      def stonesOfColor(black: Boolean): Set[Pos] =
        (for {
          (row, r) <- rows.zipWithIndex
          (cell, c) <- row.zipWithIndex
          if cell == Some(black)
        } yield Pos(r, c)).toSet

      lazy val blackStones = stonesOfColor(true)
      lazy val whiteStones = stonesOfColor(false)
      lazy val blackGroups = groups(blackStones)
      lazy val whiteGroups = groups(whiteStones)

      @annotation.tailrec  
      def groups(stones: Set[Pos], acc: Set[Group] = Set()): Set[Group] = 
        if(stones.isEmpty) acc
        else {
          val stone = stones.head
          val (group, others) = fill(Group(Set(stone)), stones diff (Set(stone)))
          groups(others, acc + group)
        }

        @annotation.tailrec  
        def fill(group: Group, stones: Set[Pos]): (Group, Set[Pos]) = {
          val neighbours = group.neighbours.intersect(stones)
          if (neighbours.isEmpty) (group, stones)
          else fill(group ++ neighbours, stones diff neighbours)
        }

        override def toString: String =
          rows.map { row =>
            row.map(_.fold(".")(black => if(black) "B" else "W")).mkString
          }.mkString("\n")
    }

    case class Group(elements: Set[Pos]) {
      def neighbours: Set[Pos] =
        elements.flatMap(_.neighbours) diff elements

      def +(stone: Pos): Group = Group(elements + stone)

      def ++(stones: Iterable[Pos]): Group = Group(elements ++ stones)
    }

    case class Pos(row: Int, col: Int) {
      def neighbours: Set[Pos] =
        Set(
          Pos(row + 1, col),
          Pos(row - 1, col),
          Pos(row, col + 1),
          Pos(row, col - 1),
          )
    }
}
