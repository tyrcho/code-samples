import math._
import scala.util._

object Player extends App {

  // w: width of the building.
  // h: height of the building.
  val Array(w, h) = for (i <- readLine split " ") yield i.toInt
  var n = readInt // maximum number of turns before game over.
  val initialN = n
  def round = initialN - n
  val Array(x0, y0) = for (i <- readLine split " ") yield i.toInt

  val init = Point(x0, y0)

  var last = init
  var current = init

  var strategy: Strategy = Start()

  def isSafe(p: Point) = strategy.isSafe(p)

  def mark(old: Point, current: Point, dist: String): Unit = {
    strategy = strategy.mark(current)
    dist match {
      case "UNKNOWN" =>
      case "WARMER" =>
        strategy = strategy.mark(current, old)
      case "COLDER" =>
        strategy = strategy.mark(old, current)
      case "SAME" =>
        strategy = strategy.markSame(current, old)
    }
  }

  val center = Point(w / 2, h / 2)

  def ellipse(r: Int) = {
    val ratio = sqrt(5) * math.Pi * 2 * r
    center + Point((ratio * cos(ratio) / 10).toInt, (ratio * sin(ratio) / 10).toInt)
  }

  // game loop
  while (true) {
    val bomb_dist = readLine
    mark(last, current, bomb_dist)
    last = current
    current = strategy.next
    println(s"${current.x} ${current.y}")
    n -= 1
  }

  trait Strategy {
    def mark(closer: Point, far: Point): Strategy
    def markSame(a: Point, b: Point): Strategy
    def mark(p: Point): Strategy
    def isSafe(p: Point): Boolean
    def next: Point
    def size: Int
  }

  case class End(unsure: Set[Point]) extends Strategy {

    def mark(closer: Point, far: Point): End = mark(far.closer(closer))
    def markSame(a: Point, b: Point): End = mark(a.diff(b))
    def mark(p: Point): End = copy(unsure - p)
    def isSafe(p: Point): Boolean = !unsure.contains(p)

    def next = {
      val candidates = unsure.toList //if (n % 2 == 0) unsureWindows else windows
      val scored = candidates.sortBy(score)
      Console.err.println(scored)
      scored.last
    }

    def mark(p: Point => Boolean): End = {
      val points = for {
        point <- unsure
        if p(point)
      } yield (point)
      copy(unsure -- points)
    }

    def size = unsure.size

    def score(candidate: Point): Int = {
      val score1 = for {
        p <- unsure
        if p.dist(current) < p.dist(candidate)
      } yield 1
      val score2 = for {
        p <- unsure
        if p.dist(current) > p.dist(candidate)
      } yield 1

      score1.sum min score2.sum
    }
  }

  case class Start(
      bombsN: Int = 0,
      bombsW: Int = 0,
      bombsS: Int = h - 1,
      bombsE: Int = w - 1,
      history: List[Point] = Nil) extends Strategy {

    Console.err.println(s"bombs in : $bombsNW, $bombsSE")

    def bombsNW = Point(bombsW, bombsN)
    def bombsSE = Point(bombsE, bombsS)

    def size = width * height

    def width = bombsE - bombsW
    def height = bombsS - bombsN

    def buildEnd: End = {
      val unsure = for {
        x <- bombsW to bombsE
        y <- bombsN to bombsS
        p = Point(x, y)
        if !history.contains(p)
      } yield p
      End(unsure.toSet)
    }

    def next = {
      val n = inGame(standardStart)
      Console.err.println(s"init strategy => $n")
      n
    }

    def isSafe(p: Point) = !inBombsRect(p)

    def inBombsRect(p: Point) = p.between(bombsNW, bombsSE)

    def centerBombs = (bombsNW + bombsSE) / 2

    def mark(p: Point): Start = copy(history = p :: history)

    def markSame(a: Point, b: Point): Start = this //TODO

    def mark(close: Point, far: Point): Strategy = {
      val marked = if (close.y == far.y) {
        val mid = (close.x + far.x) / 2
        if (close.x > far.x) copy(bombsW = max(bombsW, mid + 1))
        else copy(bombsE = min(bombsE, mid - 1))
      } else if (close.x == far.x) {
        val mid = (close.y + far.y) / 2
        if (close.y > far.y) copy(bombsN = max(bombsN, mid + 1))
        else copy(bombsS = min(bombsS, mid - 1))
      } else this
      if (marked.size <= 40) marked.buildEnd
      else marked
    }

    def inGame(p: Point) = Point(
      min(w - 1, max(0, p.x)),
      min(h - 1, max(0, p.y)))

    def standardStart = round match {
      case 0                                    => center.symX(last)
      case 1                                    => center.symY(last)
      case r if inBombsRect(last) && r % 2 == 0 => centerBombs.symX(last)
      case r if inBombsRect(last) && r % 2 == 1 => centerBombs.symY(last)
      case _ =>
        val horiz = inGame(centerBombs.symX(last))
        val hMid = (horiz.x + last.x) / 2
        val hScore = height * min(hMid - bombsW, bombsE - hMid)

        val vert = inGame(centerBombs.symY(last))
        val vMid = (vert.y + last.y) / 2
        val vScore = width * min(vMid - bombsN, bombsS - vMid)

        Console.err.println(s"horiz : $horiz")
        Console.err.println(s"horiz score: $hScore")
        Console.err.println(s"vert : $vert")
        Console.err.println(s"vert score: $vScore")
        if (hScore < 0 && vScore < 0)
          if (round % 2 == 0) bombsNW
          else bombsSE
        else if (hScore > vScore) horiz else vert
    }
  }

  case class Point(x: Int, y: Int) {
    def dist(other: Point) = (other.x - x) * (other.x - x) + (other.y - y) * (other.y - y)

    def sqr = x * x + y * y

    //predicate : points closer to this than to far
    def closer(far: Point): Point => Boolean = {
      p: Point => sqr - far.sqr <= 2 * (p.x * (x - far.x) + p.y * (y - far.y))
    }

    //predicate : points not same distance to this than to that
    def diff(that: Point): Point => Boolean = {
      p: Point => sqr - that.sqr != 2 * (p.x * (x - that.x) + p.y * (y - that.y))
    }

    def symX(p: Point) =
      Point(x - (p.x - x), p.y)

    def symY(p: Point) =
      Point(p.x, y - (p.y - y))

    def +(that: Point) = Point(x + that.x, y + that.y)
    def /(r: Int) = Point(x / r, y / r)

    def between(a: Point, b: Point) =
      x >= min(a.x, b.x) && x <= max(a.x, b.x) &&
        y >= min(a.y, b.y) && y <= max(a.y, b.y)

  }
}
