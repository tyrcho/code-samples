import math._
import scala.util._

object Solution extends App {
  val Array(w, h) = for (i <- readLine split " ") yield i.toInt
  val line = readLine
  val pixels = for {
    Array(color, count) <- line.split(" ").grouped(2)
    val black = color == "B"
  } yield black -> count.toInt
  var data = Array.fill(w, h)(false)
  var i = 0
  for { (color, count) <- pixels } {
    for (j <- 1 to count) {
      val x = i / w
      val y = i % w
      data(y)(x.toInt) = color
      i += 1
    }
  }
  var last = Partition()

  for { line <- data } {
    val pix = pixels(line)
    Console.err.println(rangesToString(pix))
    Console.err.println(pix.length)
    last = last.update(pix)
    Console.err.println(last)
  }
  println(last.result.reverse.mkString(" "))

  def pixels(l: Line, from: Int = 0): Pixels = {
    val line = l.toList
    if (line.drop(from).contains(true)) {
      val start = line.indexWhere(identity, from)
      val end = line.indexWhere(!identity(_), start)
      (start to end) :: pixels(l, end)
    } else Nil
  }

  case class Partition(lines: Pixels = Nil, recording: List[Int] = Nil, pos: Option[Int] = None, result: List[String] = Nil) {
    def lineDistance = lines(1).start - lines(0).start

    def isQueue(pix: Pixels) = pix.exists(_.size > lineDistance * 1.5)

    def names = {
      val centers = lines.map(r => r.start + (r.end - r.start) / 2)
      val doLow = centers.max + lineDistance
      val allCenters = doLow :: centers
      val inter = allCenters.map(_ - lineDistance / 2)
      val positions = (allCenters ++ inter).sorted
      (positions zip "CDEFGABCDEFG".reverse).toMap
    }

    def sixthLine = {
      val low = lines.last
      val lowCLine = (low.start + lineDistance) to (low.end + lineDistance)
      lowCLine
    }

    def closer = names.keys.minBy(k => abs(pos.get - k))

    def name = names(closer).toString

    val BLACK = "Q"
    val WHITE = "H"

    def whiteOrBlack = {
      val keys = names.keys.toList.sorted
      val lowC = keys.reverse.head
      val lowD = keys.reverse.drop(1).head
      val highG = keys.head
      val inverse = Seq(lowD, highG) contains closer
      val normal = (recording.distinct diff List(8)).sorted match {
        case List(5, 6, 7) | List(5, 6) => WHITE
        case List(6, 7) if closer == lowC => WHITE
        case List(5) | List(6) | List(6, 7) | List(4, 5, 6) => BLACK
        case _ => throw new RuntimeException("recording " + recording)
      }
      if (inverse) if (normal == BLACK) WHITE else BLACK
      else normal
    }

    def clean(next: Pixels) =
      if (lines.isEmpty) next
      else {
        val cleaned = (for (l <- next) yield {
          if (l.end == sixthLine.end)
            if (l.diff(sixthLine).isEmpty) None
            else Some(l.start to sixthLine.start)
          else Some(l)
        }).flatten
        Console.err.println("cleaned : " + rangesToString(cleaned))
        cleaned
      }

    def update(next: Pixels) = clean(next) match {
      case Nil => this
      case init if lines == Nil =>
        val np = copy(lines = init)
        Console.err.println(np.lineDistance)
        Console.err.println(np.names)
        np

      case `lines` =>
        if (recording.nonEmpty) {
          val n = name + whiteOrBlack
          Console.err.println(s"note found : $pos => $n")
          Console.err.println(recording)
          copy(recording = Nil, result = n :: result)
        } else
          copy(recording = Nil)

      case queue if isQueue(queue) =>
        Console.err.println("ignoring queue " + queue)
        this

      case sth if sth != lines =>
        val s = sth.size
        recording match {
          case Nil =>
            val other = sth diff lines
            val pos = other match {
              case List(o)    => o.start + (o.end - o.start) / 2
              case List(a, b) => a.start + (b.end - a.start) / 2
            }
            copy(recording = List(s), pos = Some(pos))
          case h :: t if h == s => this
          case _                => copy(recording = s :: recording)
        }
    }
  }

  type Pixels = List[Range]

  def rangesToString(pixels: Pixels) = pixels.map(r => s"${r.start}-${r.end}").mkString(", ")

  def debug(line: Line) = {
    for { c <- line } {
      print(if (c) "X" else ".")
    }
    println
  }

  type Line = Array[Boolean]
}
