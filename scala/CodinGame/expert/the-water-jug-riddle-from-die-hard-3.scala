object Solution extends App {
  val target=readInt
  val numbers=Vector.fill(readInt)(readInt).toSet

  val solver = WaterPouringSolver(target, numbers)
  println(solver.solution.get.moves.size)

  case class WaterPouringSolver(target: Int, capacities: Set[Int]) {
    val initialState = State(capacities.map(Glass(_, 0)))
    val initialPath = Path(Nil, initialState)

    def from(paths: Set[Path], explored: Set[State]): Stream[Set[Path]] = {
      val newPaths = for {
        path <- paths
        move <- path.endState.moves
        newPath = path.add(move)
        if !explored.contains(newPath.endState)
      } yield newPath
      val distinctStates = newPaths.groupBy(_.endState)
      val pathsToDistinctStates=distinctStates.map { case (_, paths) => paths.head }.toSet
      paths #:: from(pathsToDistinctStates, distinctStates.keySet)
    }

    def solution = (for {
      steps <- from(Set(initialPath), Set(initialState))
      path <- steps
      if path.endState.glasses.exists(_.water == target)
    } yield path).headOption
  }

  case class Path(moves: List[Move], endState: State) {
    def add(move: Move) =
      Path(move :: moves, move(endState))

    override def toString = moves.reverse.mkString("\n")
  }

  trait Move {
    def apply(s: State): State
  }

  case class Empty(g: Glass) extends Move {
    def apply(s: State) = State(s.glasses - g + g.copy(water = 0))
  }

  case class Fill(g: Glass) extends Move {
    def apply(s: State) = State(s.glasses - g + g.copy(water = g.capacity))
  }

  case class Pour(from: Glass, to: Glass) extends Move {
    val moved = from.water min (to.capacity - to.water)
    def apply(s: State) = State(s.glasses - from - to +
      from.copy(water = from.water - moved) +
      to.copy(water = to.water + moved))
  }

  case class State(glasses: Set[Glass]) {
    def emptyMoves = glasses.map(Empty)
    def fillMoves = glasses.map(Fill)
    def pourMoves = for {
      g <- glasses
      h <- glasses
      if g != h
    } yield Pour(g, h)

    def moves: Set[Move] = emptyMoves ++ fillMoves ++ pourMoves
  }

  case class Glass(capacity: Int, water: Int)
}
