// https://www.codingame.com/ide/puzzle/the-resistance

import io.StdIn._

object Solution extends App {
  val m = Morse.read()
  println(m.decode(0))

  case class Morse(code: String, dict: Set[String]) {
    val cache = collection.mutable.Map.empty[Int, Long]

    def decode(pos: Int): Long =
      if (pos == code.length) 1
      else cache.get(pos) match {
        case Some(c) => c
        case None =>
          val c = (for {
            (word, p) <- words(pos)
          } yield decode(p)).sum
          cache(pos) = c
          c
      }

    def words(pos: Int, prefix: String = "", candidates: Set[String] = dict): Iterable[(String, Int)] = {
      val found =
        if (candidates.contains(prefix)) Some((prefix, pos))
        else None
      found ++ (for {
        (l, morseSize) <- letters(pos)
        word = prefix + l
        nextCand = candidates.filter(_.startsWith(word))
        if nextCand.nonEmpty
        nextPos = pos + morseSize
        rec <- words(nextPos, word, nextCand)
      } yield rec)
    }

    def letters(pos: Int): Iterable[(Char, Int)] =
      for {
        (coded, letter) <- Morse.morse
        if code.startsWith(coded, pos)
      } yield (letter, coded.length)

  }

  object Morse {
    def read() = {
      val code = readLine
      val dict = Seq.fill(readInt)(readLine)
      Morse(code, dict.toSet)
    }

    val codes = Seq(".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..",
      "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..")
    val morse = for {
      (c, i) <- codes.zipWithIndex
    } yield c -> ('A' + i).toChar
  }

}
