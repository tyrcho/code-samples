import io.StdIn._

object Solution extends App {
  val requests = Vector.fill(readInt) {
    val Array(start, duration) = readLine.split(" ").map(_.toInt)
    Request(start, start + duration - 1)
  }.sortBy(_.end)

  val selection = requests.foldLeft(Selection(end = -1, count = 0)) {
    case (best @ Selection(end, count), Request(s, e)) =>
      if (s > end) Selection(e, count + 1)
      else best
  }

  println(selection.count)
}

case class Selection(end: Int, count: Int)
case class Request(start: Int, end: Int)
