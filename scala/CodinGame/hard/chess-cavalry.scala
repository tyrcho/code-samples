import io.StdIn.readLine
import scala.annotation.tailrec

object Solution extends App {
  val Array(w, h) = readLine.split(" ").map(_.toInt)
  val grid = Vector.fill(h)(readLine)

  grid.foreach(Console.err.println)

  val to = find('E')
  val from = find('B')

  pathLength(List(from), Map(from -> 0)) match {
    case None    => println("Impossible")
    case Some(l) => println(l)
  }

  def find(c: Char): Pos = {
    val y = grid.indexWhere(_.contains(c))
    val x = grid(y).indexOf(c)
    Pos(x, y)
  }

  @tailrec
  def pathLength(edge: List[Pos], lengths: Map[Pos, Int] = Map.empty): Option[Int] = edge match {
    case Nil               => None
    case h :: t if h == to => lengths.get(h)
    case h :: t =>
      val next = h.reach.toSet -- lengths.keys
      val d = lengths(h)
      val newLengths = next.foldLeft(lengths) {
        case (map, pos) => map.updated(pos, d + 1)
      }
      pathLength(edge.tail ++ next, newLengths)
  }

  case class Pos(x: Int, y: Int) {
    def reach = for {
      (dx, dy) <- Seq((-2, -1), (-2, +1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1))
      nx = x + dx
      ny = y + dy
      if nx >= 0 && ny >= 0 && nx < w && ny < h
      if grid(ny)(nx) != '#'
    } yield Pos(nx, ny)
  }

}
