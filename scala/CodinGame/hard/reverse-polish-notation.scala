import io.StdIn._
import scala.annotation.tailrec

object Solution extends App {

  import Constants._

  readInt()

  println(result(readLine()))

  def result(line: String): String = process(line.split(" ").toList).reverse.mkString(" ")

  @tailrec
  def process(commands: List[String], stack: List[Int] = Nil): List[String] =
    commands match {
      case h :: t =>
        val (ints, ok) = handleCommand(stack, h)
        if (ok) process(t, ints)
        else "ERROR" :: ints.map(_.toString)
      case _ => stack.map(_.toString)
    }

  private def handleCommand(stack: List[Int], command: String): (List[Int], Boolean) = (stack, command) match {
    case (a :: tail, DUP) => (a :: a :: tail, true)
    case (a :: b :: tail, ADD) => (a + b :: tail, true)
    case (a :: b :: tail, SUB) => (b - a :: tail, true)
    case (a :: b :: tail, MUL) => (a * b :: tail, true)
    case (a :: _ :: tail, DIV | MOD) if a == 0 => (tail, false)
    case (a :: b :: tail, DIV) if a != 0 => (b / a :: tail, true)
    case (a :: b :: tail, MOD) if a != 0 => (b % a :: tail, true)
    case (a :: b :: tail, SWP) => (b :: a :: tail, true)
    case (_ :: tail, POP) => (tail, true)
    case (h :: tail, ROL) if tail.length >= 3 =>
      val (left, right) = tail.splitAt(2)
      (right.head :: left ::: right.tail, true)
    case (h :: tail, ROL) if tail.length < 3 => (tail, false)
    case (s, n) if n.matches(INT_RE) => (n.toInt :: s, true)
    case _ => (Nil, false)
  }
}

object Constants {
  val ADD = "ADD"
  val SUB = "SUB"
  val MUL = "MUL"
  val DIV = "DIV"
  val MOD = "MOD"
  val DUP = "DUP"
  val SWP = "SWP"
  val POP = "POP"
  val ROL = "ROL"
  val INT_RE = """-?\d+"""
}
