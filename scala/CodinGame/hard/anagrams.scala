import io.StdIn._

object Solution extends App {
  val letters = 'A' to 'Z'
  val special = (2 to 4).map(i => i -> letters.grouped(i).map(_.last).toList).toMap

  val phrase = readLine
  val step4 = {
    val words = phrase.split(" ")
    val sizes = words.map(_.size).reverse
    val flat = words.mkString
    val indices = sizes.scan(0)(_ + _)
    for ((i, j) <- indices zip indices.tail) yield flat.subSequence(i, j)
  }.mkString(" ")
  Console.err.println(step4)

  def step13(in: String, pos: Int, shift: IndexedSeq[Char] => IndexedSeq[Char]) = {
    val indices = (0 until in.length).filter(i => special(pos).contains(in(i)))
    if (indices.isEmpty) in else {
      val chars = indices.map(in)
      val shifted = shift(chars)
      (0 until indices.size).foldLeft(in)((w, i) => w.updated(indices(i), shifted(i)))
    }
  }

  val step3 = step13(step4, 4, chars => chars.last +: chars.init)
  Console.err.println(step3)
  val step2 = step13(step3, 3, chars => chars.tail :+ chars.head)
  Console.err.println(step2)
  val step1 = step13(step2, 2, chars => chars.reverse)
  println(step1)
}
