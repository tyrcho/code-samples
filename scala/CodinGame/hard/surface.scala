import scala.annotation.tailrec
import scala.io.StdIn.readInt
import scala.io.StdIn.readLine

object Solution extends App {
  val width = readInt
  val height = readInt
  val lines = Vector.fill(height)(readLine)

  val data = Data(lines.map(_.toVector.map('O'.==)),
    width, height)

  val n = readInt
  for (i <- 0 until n) {
    val Array(x, y) = for (i <- readLine split " ") yield i.toInt;
    println(data.lakeSize(List((x, y))))
  }
}

object Test extends App {
  val data = Data(Vector.fill(1000, 1000)(true), 1000, 1000)

  println(data.lakeSize(List((10, 10))))
}

case class Data(data: Vector[Vector[Boolean]], width: Int, height: Int) {
  def water(x: Int, y: Int) = data(y)(x)

  def clear(x: Int, y: Int) = copy(data = data.updated(y, data(y).updated(x, false)))

  def neighbours(x: Int, y: Int) = for {
    (dx, dy) <- List((-1, 0), (1, 0), (0, 1), (0, -1))
    nx = x + dx
    if nx < width && nx >= 0
    ny = y + dy
    if ny < height && ny >= 0
    if water(nx, ny)
  } yield (nx, ny)

  @tailrec
  final def lakeSize(toExplore: List[(Int, Int)], acc: Int = 0): Int =
    toExplore match {
      case Nil => acc
      case (x, y) :: tail =>
        if (water(x, y))
          clear(x, y).lakeSize(neighbours(x, y) ::: toExplore.tail, acc + 1)
        else
          lakeSize(toExplore.tail, acc)
    }
}
