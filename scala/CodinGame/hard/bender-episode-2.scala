import scala.annotation.tailrec
import io.StdIn._

object Solution extends App {
  val rooms = List.fill(readInt)(readRoom).toMap

  val origins = (for {
    Room(from, _, n1, n2) <- rooms.values
    to <- Seq(n1, n2)
  } yield to -> from).groupBy(_._1).mapValues(_.map(_._2).toSet).withDefaultValue(Set())

  println(collect())

  @tailrec
  def collect(totals: Map[String, Int] = Map.empty.withDefaultValue(0), tips: Vector[String] = Vector("E")): Int = {
    if (tips.isEmpty) totals("0")
    else {
      val tip = tips.head
      val totalForTip = totals(tip)
      val newTips = for {
        n <- origins(tip)
        if rooms(n).money + totalForTip > totals(n)
      } yield n
      val newValues = newTips.foldLeft(totals) {
        case (vals, roomId) => vals.updated(roomId, rooms(roomId).money + totalForTip)
      }
      collect(newValues, (tips.tail ++ newTips).distinct)
    }
  }

  def readRoom = {
    val Array(id, value, next1, next2) = readLine.split(" ")
    id -> Room(id, value.toInt, next1, next2)
  }
}

case class Room(id: String, money: Int, next1: String, next2: String)
