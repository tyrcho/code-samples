import scala.io.StdIn._

object Solution extends App {
  readInt() //levels
  val Array(x1, y1, x2, y2) = readLine().split(" ").map(BigInt(_))

  (y1 to y2)
    .map(line)
    .foreach(println)


  def line(y: BigInt): String =
    (x1 to x2)
      .map(x => charAt(x, y))
      .mkString

  def charAt(x: BigInt, y: BigInt): Char = {
    val hasCommonCenterBit = (centerBinaryPattern(x) & centerBinaryPattern(y)) != 0
    if (hasCommonCenterBit) '+' else '0'
  }

  def centerBinaryPattern(x: BigInt): BigInt = {
    val base3 = x.toString(radix = 3)
    val base2 = base3.replace('2', '0')
    BigInt(base2, radix = 2)
  }
}
