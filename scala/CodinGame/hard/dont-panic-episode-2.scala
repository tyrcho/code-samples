import math._
import scala.util._
import io.StdIn._
import scala.annotation.tailrec

object Player extends App {
  //  val s = ClonesState(109, 100, 4, 0, 6, false, 11, 47, Vector((11, 45), (1, 36), (2, 43), (3, 17), (11, 50), (3, 24), (10, 3), (10, 23), (6, 23), (8, 1), (2, 56), (2, 9), (1, 62), (4, 9), (2, 24), (3, 30), (6, 35), (1, 24), (11, 4), (5, 4), (8, 63), (1, 4), (8, 9), (6, 3), (1, 17), (9, 2), (10, 45), (6, 9), (9, 17), (1, 50), (8, 23), (2, 3), (3, 60), (4, 23), (7, 48), (2, 23)), 109, false)
  //
  //  s.buildPath.foreach(println)

  var statesVisited = 0

  val Array(nbfloors, width, nbrounds, exitfloor, exitpos, nbtotalclones, nbadditionalelevators, nbelevators) = for (i <- readLine split " ") yield i.toInt
  var elevators = (for (i <- 0 until nbelevators) yield {
    val Array(elevatorfloor, elevatorpos) = for (i <- readLine split " ") yield i.toInt
    (elevatorfloor, elevatorpos)
  }).toList

  var round = 0
  var moves: Vector[CloneMove] = Vector.empty
  while (true) {

    val Array(_clonefloor, _clonepos, direction) = readLine split " "
    val clonefloor = _clonefloor.toInt
    val clonepos = _clonepos.toInt

    if (round == 0) {
      val state = ClonesState(
        nbrounds,
        nbtotalclones,
        nbadditionalelevators,
        0,
        clonepos,
        direction == "LEFT",
        exitfloor,
        exitpos,
        elevators.toVector,
        nbrounds)
      Console.err.println(state)
      moves = state.buildPath
      moves.toList.foreach(Console.err.println)
    }

    println(moves.find(_.round == round).map(_.name).getOrElse("WAIT"))
    round += 1

  }

  case class CloneMove(name: String, state: ClonesState, round: Int)

  case class ClonesState(
      roundsLeft: Int,
      clonesLeft: Int,
      elevatorsLeft: Int,
      currentFloor: Int,
      currentX: Int,
      left: Boolean,
      exitfloor: Int,
      exitpos: Int,
      elevators: Vector[(Int, Int)],
      nbRounds: Int,
      dontTryBlock: Boolean = false) {

    def currentRound = nbRounds - roundsLeft

    override val toString = s"$currentX,$currentFloor ${if (left) "left" else "right"} $roundsLeft roundsleft"

    val valid =
      roundsLeft > 0 &&
        clonesLeft > 0 &&
        elevatorsLeft >= 0 &&
        exitfloor >= currentFloor

    val nextElevatorsOnRow = (for {
      (floor, pos) <- elevators
      if floor == currentFloor
      horizDelta = pos - currentX
      if horizDelta == 0 || left == horizDelta < 0
    } yield pos).sortBy(x => abs(x - currentX))

    val nextElevatorsOnUpperRow = (for {
      (floor, pos) <- elevators
      if floor == currentFloor + 1
      horizDelta = pos - currentX
      if (left && horizDelta <= 0) || (!left && horizDelta >= 0)
    } yield pos).sortBy(x => abs(x - currentX))

    val finished = {
      val horizDelta = exitpos - currentX
      exitfloor == currentFloor &&
        (horizDelta == 0 || (
          left == horizDelta < 0 &&
          roundsLeft >= horizDelta.abs &&
          nextElevatorsOnRow.forall(pos => abs(pos - currentX) > horizDelta.abs)))
    }

    def moves = waitMove ++ allElevatorMoves ++ blockMove

    def waitMove = nextElevatorsOnRow.headOption.map { elevatorX =>
      val distance = horizDist(elevatorX)
      CloneMove("WAIT", copy(
        roundsLeft = roundsLeft - 1 - distance,
        currentFloor = currentFloor + 1,
        currentX = elevatorX,
        dontTryBlock = false),
        currentRound)
    }

    val nextElevatorOnRow = nextElevatorsOnRow.headOption

    def horizDist(x: Int) = abs(x - currentX)

    def allElevatorMoves = {
      val maxDist = nextElevatorOnRow.map(horizDist).getOrElse(Int.MaxValue)
      val horizPositionsL = for {
        ex <- nextElevatorsOnUpperRow.toSet + currentX
        p <- Seq(ex + (if (left) -1 else 1), ex)
        dist = horizDist(p)
      } yield p
      val horizPositions =
        if (horizPositionsL.isEmpty) Set(currentX)
        else horizPositionsL + (if (left) (horizPositionsL.max + 1) else (horizPositionsL.min - 1))
      for {
        p <- horizPositions
        dist = horizDist(p)
        if dist < maxDist
        if (p >= currentX && !left) || (p <= currentX && left)
        move = CloneMove(
          "ELEVATOR",
          copy(
            roundsLeft = roundsLeft - 4 - dist,
            clonesLeft = clonesLeft - 1,
            elevatorsLeft = elevatorsLeft - 1,
            currentFloor = currentFloor + 1,
            currentX = p,
            dontTryBlock = false),
          currentRound + dist)
      } yield move
    }

    def blockMove: Iterable[CloneMove] =
      if (dontTryBlock) None
      else Some(CloneMove(
        "BLOCK",
        copy(
          roundsLeft = roundsLeft - 3,
          clonesLeft = clonesLeft - 1,
          left = !left,
          dontTryBlock = true),
        currentRound))

    type Path = Vector[CloneMove]

    @tailrec
    final def visit(queue: Vector[ClonesState], paths: Map[ClonesState, Path]): Path = if (queue.nonEmpty) {
      Player.statesVisited += 1
      val h = queue.head
      if (Player.statesVisited % 100 == 0) Console.err.println(Player.statesVisited, queue.size, h, queue(1))
      val t = queue.tail
      val path = paths(h)
      if (h.finished) path
      else {
        val newPaths = (for {
          m <- h.moves
          if m.state.valid
        } yield m.state -> (path :+ m)).toMap
        val keptStates = newPaths.keys.filter(ns => !paths.keys.exists(os => os isBetterThan ns)).toSet
        val keptPaths = newPaths.filter { case (s, p) => keptStates.contains(s) }
        val keptOldPaths = paths.filter { case (os, _) => !keptStates.exists(ns => ns isBetterThan os) }
        val keptStatesInQueue = t.filter(keptOldPaths.keySet.contains)
        val nextQueue = (keptStatesInQueue ++ keptPaths.keys)
        visit(nextQueue, keptOldPaths ++ keptPaths)
      }
    } else throw new Exception("no solution")

    val pk = (currentX, currentFloor, left)

    def isBetterThan(s: ClonesState) =
      this.pk == s.pk &&
        clonesLeft >= s.clonesLeft && elevatorsLeft >= s.elevatorsLeft && roundsLeft >= s.roundsLeft

    def buildPath: Vector[CloneMove] = visit(Vector(this), Map(this -> Vector()))
  }

}

