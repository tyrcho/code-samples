import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.StdIn.readInt
import scala.io.StdIn.readLine
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

object Player extends App {
  import Unfoldable._

  var state = State.read()
  state = state.update()
  Console.err.println(state)
  val ideal = state.copy(neededMotos = state.motosY.size)
  val preferred = Future.firstCompletedOf(List(solveAsync(ideal), Future { Thread.sleep(140); None }))
  val fallback = solveAsync(state)
  val futures = Future.sequence(List(preferred,fallback))
  val List(preferredRes, fallbackRes) = Await.result(futures, 145.millis)

  preferredRes.orElse(fallbackRes) match {
    case Some(lastState) =>
      val states = lastState.unfold { s =>
        s.previous.map(p => (s, p))
      }.reverse
      Console.err.println(states.map(_.moveFrom))
      for (s <- states) {
        Console.err.println(s)
        println(s.moveFrom.toUpperCase)
        state.update()
      }
  }

  def process(lastState: State) = {
    val states = lastState.unfold { s =>
      s.previous.map(p => (s, p))
    }.reverse
    Console.err.println(states.map(_.moveFrom))
    for (s <- states) {
      Console.err.println(s)
      println(s.moveFrom.toUpperCase)
      state.update()
    }
  }

  def solveAsync(s: State) = Future {
    Problem(s).solve()
  }

  case class State(
      motosY: Vector[Int],
      x: Int,
      speed: Int,
      neededMotos: Int,
      road: Road,
      turn: Int = 0,
      moveFrom: String = "",
      previous: Option[State] = None) {

    override def toString = s"$motosY live motos, speed=$speed, x=$x, turn=$turn, moveFrom=$moveFrom"

    def update(): State = {
      val s = readInt // the motorbikes' speed
      val motos = Vector.fill(motosY.size) {
        val Array(x, y, a) = for (i <- readLine split " ") yield i.toInt
        (x, y)
      }
      copy(motosY = motos.map(_._2), x = motos.head._1, speed = s, turn = turn + 1)
    }

    def liveMotos = motosY.length
    def death = liveMotos < neededMotos

    lazy val waitMove = {
      val stillAlive = motosY.filterNot { y =>
        (x + 1 to x + speed).exists(x => road.isUnsafe(x, y))
      }
      copy(turn = turn + 1, motosY = stillAlive, x = x + speed, moveFrom = "Wait", previous = Some(this))
    }

    lazy val upMove = {
      val stillAlive = for {
        y <- motosY
        if !(x + 1 to x + speed - 1).exists(x => road.isUnsafe(x, y))
        ny = if (motosY.contains(y - 1) || y <= 0) y else y - 1
        if !(x + 1 to x + speed).exists(x => road.isUnsafe(x, ny))
      } yield ny
      copy(turn = turn + 1, motosY = stillAlive, x = x + speed, moveFrom = "Up", previous = Some(this))
    }

    lazy val downMove = {
      val stillAlive = for {
        y <- motosY
        if !(x + 1 to x + speed - 1).exists(x => road.isUnsafe(x, y))
        ny = if (motosY.contains(y + 1) || y >= 3) y else y + 1
        if !(x + 1 to x + speed).exists(x => road.isUnsafe(x, ny))
      } yield ny
      copy(turn = turn + 1, motosY = stillAlive, x = x + speed, moveFrom = "Down", previous = Some(this))
    }

    lazy val speedMove = {
      val stillAlive = motosY.filterNot { y =>
        (x + 1 to x + speed + 1).exists(x => road.isUnsafe(x, y))
      }
      copy(turn = turn + 1, motosY = stillAlive, x = x + speed + 1, speed = speed + 1, moveFrom = "Speed", previous = Some(this))
    }

    lazy val slowMove = {
      val stillAlive = motosY.filterNot { y =>
        (x + 1 to x + speed - 1).exists(x => road.isUnsafe(x, y))
      }
      copy(turn = turn + 1, motosY = stillAlive, x = x + speed - 1, speed = speed - 1, moveFrom = "Slow", previous = Some(this))
    }

    lazy val jumpMove = {
      val stillAlive = motosY.filterNot { y =>
        road.isUnsafe(x + speed, y)
      }
      copy(turn = turn + 1, motosY = stillAlive, x = x + speed, moveFrom = "Jump", previous = Some(this))
    }

    def next = List(speedMove, jumpMove, waitMove, upMove, downMove) ++ (if (speed > 0) List(slowMove) else Nil)
  }

  object State {
    def read(): State = {
      val m = readInt // the amount of motorbikes to control
      val v = readInt // the minimum amount of motorbikes that must survive
      val road = Road.read()
      State(Vector.fill(m)(0), 0, 0, v, road)
    }
  }

  case class Road(kills: Vector[Vector[Boolean]]) {
    def isUnsafe(x: Int, y: Int) = {
      val r = kills(y)(x min kills.head.length - 1)
      r
    }
    def length = kills.head.length

    override def toString = "road"
  }

  object Road {
    def read(): Road = {
      val lanes = Vector.fill(4)(readLine().map('.'.!=).toVector)
      Console.err.println("road length : " + lanes(0).length)
      Road(lanes)
    }
  }

  case class Problem(root: State) {
    type Node = State

    trait Result
    case object Rejected extends Result
    case object Accepted extends Result
    case class Undecided(children: List[Node]) extends Result

    def analyse(state: State): Result = {
      import state._
      if (death || turn > 50) Rejected
      else if (x > road.length) Accepted
      else Undecided(state.next.sortBy(-_.motosY.size))
    }

    // gets the first solution only
    @tailrec
    final def solve(candidates: List[Node] = List(root)): Option[Node] = candidates match {
      case candidate :: tail =>
        analyse(candidate) match {
          case Accepted            => Some(candidate)
          case Rejected            => solve(tail)
          case Undecided(children) => solve(children ::: tail)
        }
      case Nil => None
    }
  }

  object Unfoldable {
    import scala.collection.generic.CanBuildFrom

    final implicit class UnfoldableOps[A](a: A) {
      def unfold[B, This, That](f: A => Option[(B, A)])(implicit cb: CanBuildFrom[This, B, That]): That = {
        val builder = cb()
        @tailrec def unfolding(a: A): That = {
          f(a) match {
            case Some((e, next)) =>
              builder += e
              unfolding(next)
            case None =>
              builder.result()
          }
        }
        unfolding(a)
      }
    }

  }

}
