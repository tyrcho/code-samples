import math._
import scala.util._

object Solution extends App {
  val n = readInt
  val data = for (i <- 0 until n) yield {
    val Array(num, t) = for (i <- readLine split " ") yield i.toInt
    num -> t
  }

  val best = allComplex.minBy(score)

  println(best.name)

  trait Complexity {
    def name: String
    def apply(N: Int): Double
  }

  object Const extends Complexity {
    val name = "O(1)"
    def apply(N: Int): Double = 1
  }

  object Log extends Complexity {
    val name = "O(log n)"
    def apply(N: Int): Double = log(N)
  }

  object Prop extends Complexity {
    val name = "O(n)"
    def apply(N: Int): Double = N
  }

  object Square extends Complexity {
    val name = "O(n^2)"
    def apply(N: Int): Double = N * N
  }

  object Cube extends Complexity {
    val name = "O(n^3)"
    def apply(N: Int): Double = pow(N, 2.8)
  }

  object Exp extends Complexity {
    val name = "O(2^n)"
    def apply(N: Int): Double = pow(2, N)
  }

  object Nlogn extends Complexity {
    val name = "O(n log n)"
    def apply(N: Int): Double = N * log(N)
  }

  object N2logn extends Complexity {
    val name = "O(n^2 log n)"
    def apply(N: Int): Double = N * N * log(N)
  }

  def allComplex = List(Const, Log, Prop, Nlogn, Square, N2logn, Cube, Exp)

  def sqr(x: Double) = x * x

  def delta(c: Complexity, N: Int, t: Int, i: Int): Double = {
    val (n0, t0) = data(i)
    val k = c(n0) / t0
    val kn = c(N) / t
    val d = 1 - kn / k
    d*d
  }

  def score(c: Complexity) = {
      val allScores=for { i <- 0 until data.length }
        yield (for ( (n,t) <- data) yield delta(c, n, t, i)).sum
    val res = allScores.min
    Console.err.println(c.name -> res)
    res
  }

}
