import math._
import scala.util._
import scala.annotation.tailrec

object Player extends App {

  // n: the total number of nodes in the level, including the gateways
  // l: the number of links
  // e: the number of exit gateways
  val Array(n, l, e) = for (i <- readLine split " ") yield i.toInt
  val edges =
    for {
      i <- 0 until l
      Array(n1, n2) = for (i <- readLine split " ") yield i.toInt
    } yield Edge(Node(n1), Node(n2))

  var graph = Graph(edges.toSet)

  val gates = (for (i <- 0 until e)
    yield Node(readInt)).toSet

  def cutLink(a: Node, b: Node): Unit = {
    graph = graph.cut(Edge(a, b))
    println(s"${a.value} ${b.value}")
  }

  // game loop
  while (true) {
    val si = Node(readInt) // The node on which the Skynet agent is positioned this turn
    gates.filter(g => graph.distance(g, si) == 2).headOption match {
      case Some(g) => cutLink(g, si)
      case None =>
        val scoredNodes = graph.gatewayNeighbours.toList.map {
          case (node, count) => node -> (count, -graph.distance(node, si, true))
        }.filter(_._2._1 >= 1).sortBy(_._2)
        scoredNodes.foreach(Console.err.println)
        val node = scoredNodes.last._1
        val gate = gates.find(graph.neighbours(node).contains).get
        cutLink(node, gate)
    }

  }

  case class Edge(from: Node, to: Node) {
    lazy val inv = Edge(to, from)
  }

  case class Node(value: Int)

  case class Path(path: List[Node]) {
    def end = path.last
    def append(n: Node) = Path(path ::: List(n))
    def length = path.size
    def lastSection: (Node, Node) = (path(path.length - 2), path(path.length - 1))
    def firstSection: (Node, Node) = (path(0), path(1))
  }

  case class Graph(edges: Set[Edge]) {
    lazy val nodes: Set[Node] =
      edges.map(_.from) ++ edges.map(_.to)

    lazy val neighbours: Map[Node, Set[Node]] =
      (for (n <- nodes) yield (n ->
        (for (r <- nodes if edges.contains(Edge(n, r)) || edges.contains(Edge(r, n))) yield r))).toMap

    lazy val gatewayNeighbours: Map[Node, Int] =
      (for (n <- nodes)
        yield n -> neighbours(n).filter(gates.contains(_)).size).toMap

    def cut(e: Edge): Graph =
      copy(edges = edges - e - e.inv)

    def reach1(n: Node): Set[Node] =
      neighbours.getOrElse(n, Set.empty)

    //nodes connected to a gateway can be traversed freely
    def smartReach(n: Node): Set[Node] = {
      val step1 = reach1(n)
      @tailrec
      def smartRec(nodes: Set[Node], acc: Set[Node] = Set.empty): Set[Node] = {
        if (nodes.isEmpty) acc
        else {
          val gateNeighbours = nodes.filter(n => gatewayNeighbours(n) > 0)
          val next = gateNeighbours.flatMap(reach1) -- acc
          smartRec(next, acc ++ nodes)
        }
      }
      smartRec(step1)
    }

    def without(nodes: Set[Node]) =
      Graph(edges = for (e <- edges if (!nodes.contains(e.from) && !nodes.contains(e.to))) yield e)

    //smart : behaves as if the nodes connected to a gate are ignored
    def distance(from: Node, to: Node, smart: Boolean = false): Int = {
      path(from, to, smart) match {
        case Some(p) => p.length
        case None    => Int.MaxValue
      }
    }

    def path(from: Node, to: Node, smart: Boolean = false): Option[Path] =
      path(Set(Path(List(from))), to, this, smart)

    @tailrec
    private def path(paths: Set[Path],
                     to: Node,
                     graph: Graph,
                     smart: Boolean): Option[Path] = {
      val newPaths = for {
        p <- paths
        n <- if (smart) graph.smartReach(p.end) else graph.reach1(p.end)
      } yield p.append(n)
      if (newPaths.isEmpty) None
      else {
        val uniqueEnds = for ((_, paths) <- newPaths.groupBy(_.end)) yield paths.head
        uniqueEnds.find(_.end == to) match {
          case Some(path) => Some(path)
          case None =>
            val trimmedGraph = graph.without(paths.map(_.end))
            path(uniqueEnds.toSet, to, trimmedGraph, smart)
        }
      }
    }
  }
  object Graph {
    def apply(edges: Iterable[(Int, Int)]): Graph =
      Graph((for ((a, b) <- edges) yield Edge(Node(a), Node(b))).toSet)
  }
}

object TestSkynet extends App {
  import Player._
  val wheel1 = (1 to 17).map(_ -> 0)
  val wheel2 = (29 to 36).map(_ -> 28)
  val wheel3 = (19 to 27).map(_ -> 18)
  val center37 = List(1, 2, 35, 23, 24).map(_ -> 37)
  val center35 = List(2, 23).map(_ -> 35)
  val arms = List(4 -> 33, 3 -> 34, 36 -> 22, 29 -> 21)
  val graph = Graph(wheel1 ++ wheel2 ++ wheel3 ++ center35 ++ center37 ++ arms)
  println(graph.path(Node(0), Node(37)))
  println(graph.path(Node(0), Node(-1)))
}
