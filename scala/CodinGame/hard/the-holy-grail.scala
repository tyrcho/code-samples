import scala.annotation.tailrec
import scala.io.StdIn._

object Solution extends App {
  val Array(width, height) = readLine.split(" ").map(_.toInt)
  val tiles = Seq.fill(256) {
    Option(readLine).map { line =>
      val Array(x, y) = line.split(" ").map(_.toInt)
      Coord(x, y)
    }
  }.flatten

  println(solve(width, height, tiles))

  def solve(xmax: Int, ymax: Int, tiles: Seq[Coord], covered: Set[Coord] = Set.empty): Int = {
    val target = Set(Coord(xmax - 1, ymax - 2), Coord(xmax - 2, ymax - 1))

    def solve(tiles: Seq[Coord], covered: Set[Coord]): Int = {
      if (Solution.reachable(covered).intersect(target).nonEmpty)
        0
      else {
        1 + solve(tiles.tail, covered + tiles.head)
      }
    }

    solve(tiles, covered)
  }


  def reachable(tiles: Set[Coord] = Set.empty): Set[Coord] = {
    @tailrec
    def reachable(tips: List[Coord] = List(Coord(0, 0)), reached: Set[Coord] = Set.empty): Set[Coord] = tips match {
      case Nil => reached
      case h :: t =>
        val newTips = h.neighbors.intersect(tiles) -- tips -- reached
        reachable(t ++ newTips, reached + h)
    }

    reachable()
  }
}

case class Coord(x: Int, y: Int) {
  def neighbors: Set[Coord] = Set(
    Coord(x, y + 1),
    Coord(x, y - 1),
    Coord(x + 1, y),
    Coord(x - 1, y)
  )
}
