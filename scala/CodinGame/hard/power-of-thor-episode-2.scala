import math._
import scala.util._

object Player extends App {

  val Array(tx, ty) = for (i <- readLine split " ") yield i.toInt
  var thor = Point(tx, ty)
  val maxX = 40
  val maxY = 18

  // game loop
  while (true) {
    // h: the remaining number of hammer strikes.
    // n: the number of giants which are still present on the map.
    val Array(h, n) = for (i <- readLine split " ") yield i.toInt
    val giants = for (i <- 0 until n) yield {
      val Array(x, y) = for (i <- readLine split " ") yield i.toInt
      Point(x, y)
    }

    Console.err.println("giants : " + giants)

    val free = thor.freeNeighbours(giants)
    Console.err.println("free : " + free)
    if (giants.forall(thor.inThorRange(_)) || free.isEmpty) println("STRIKE")
    else {
      val bar = bary(giants)
      Console.err.println("bary : " + bar)
      val target=thor.triangle(bar)
      val sorted = free.toList.sortBy(target.dist)
//      val to = sorted.find { f =>
//        val line = thor.line(f)
//        Console.err.println(line)
//        !thor.line(f).exists(giants.contains)
//      }.getOrElse(sorted.head)
      val to=sorted.head
      Console.err.println("to : " + to)

      val dir = thor.moveTo(to)
      thor = to
      println(dir)
    }
  }

  def bary(points: Iterable[Point]) = {
    val sum = points.reduce(_ + _)
    sum / points.size
  }

  case class Point(x: Int, y: Int) {
    def inThorRange(giant: Point) =
      abs(x - giant.x) < 5 &&
        abs(y - giant.y) < 5

    def +(p: Point) = Point(x + p.x, y + p.y)

    def /(i: Int) = Point(x / i, y / i)

    def line(direction: Point, depth: Int = 40): List[Point] =
      if (depth > 0) {
        val dx = direction.x - x
        val dy = direction.y - y
        val next = Point(x + dx, y + dy)
        val nn = Point(x + 2 * dx, y + 2 * dy)
        next :: next.line(nn, depth - 1)
      } else Nil

    def neighbours = for {
      i <- -1 to 1
      j <- -1 to 1
      nx = x + i
      ny = y + j
      if nx < maxX && nx >= 0
      if ny < maxY && ny >= 0
      p = Point(nx, ny)
      if p != this
    } yield p

    def sqr(x: Int) = x * x
    def dist(p: Point) = sqr(x - p.x) + sqr(y - p.y)

    def triangle(p: Point) = {
      val dx = p.x - x
      val dy = p.y - y
      if (abs(dx) > 1 && abs(dy) > 1) Point(p.x, y)
      else if (abs(dx) <= 1) Point(x + 5, p.y)
      else Point(p.x, y + 5)
    }

    def moveTo(neighbour: Point): String = {
      if (neighbour.x > x)
        if (neighbour.y > y) "SE"
        else if (neighbour.y < y) "NE"
        else "E"
      else if (neighbour.x < x)
        if (neighbour.y > y) "SW"
        else if (neighbour.y < y) "NW"
        else "W"
      else if (neighbour.y > y) "S"
      else "N"
    }

    def freeNeighbours(giants: Iterable[Point]): Iterable[Point] = for {
      n <- neighbours
      if !giants.exists(g => g.neighbours.contains(n) || g == n)
    } yield n
  }

}
