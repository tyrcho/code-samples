import math._
import scala.util._
import scala.util.parsing.combinator._

object Solution extends CgxParser with App {

    val n = readInt
    val lines = for (i <- 0 until n) yield readLine
  val parsed = process(lines)  match {
    case Success(res, _) =>
      res.format().foreach(println)
    case Failure(msg, _) =>
      println("FAILURE: " + msg)
    case Error(msg, _) =>
      println("ERROR: " + msg)
  }

}

class CgxParser extends RegexParsers {

  def process(lines: Iterable[String]): ParseResult[Element] = parse(root, lines.mkString)

  def string: Parser[Str] = """'[^']*'""".r ^^ { case s => Str(s.toString) }
  def number: Parser[Num] = """0|[1-9]\d*""".r ^^ { case i => Num(i.toInt) }
  def boolean: Parser[Boo] = "true|false".r ^^ { case b => Boo(b.toBoolean) }
  def none: Parser[Null.type] = "null" ^^^ Null 

  def keyValue : Parser[KeyValue] = string ~ "=" ~ element ^^ {
    case s ~ _ ~ p  => KeyValue(s, p)      
  }

  def primitive: Parser[Primitive[_]] = boolean | number | none | string

  def element: Parser[Element] = keyValue | primitive | bloc 

  def bloc: Parser[Bloc] = "(" ~> (element ~ (";" ~> element).*).? <~ ")" ^^ {
    case Some(e ~ elts) => Bloc(e :: elts)
    case None => Bloc(Nil)
  }

  def root: Parser[Element] = element

  trait Element {
    def format(indent: Int = 0): Iterable[String]
  }

  trait Primitive[T] extends Element {
    def format(indent: Int) =
      Some("    " * indent + value.toString)

    def value: T
  }

  case object Null extends Primitive[String] {
    val value = "null"
  }
  case class Str(value: String) extends Primitive[String]
  case class Boo(value: Boolean) extends Primitive[Boolean]
  case class Num(value: Int) extends Primitive[Int]
  case class KeyValue(key:Str, value:Element) extends Element {
      def format(indent:Int=0) = value match {
          case p:Primitive[_] =>
            Some(key.format(indent).head+"="+p.format(0).head)
          case b:Bloc =>  
            key.format(indent).head+"=" ::
            b.format(indent).toList
      }
  }

  case class Bloc(elements: List[Element]) extends Element {
    // mkstring("\n") seems not to be accepted by CG
    def format(indent: Int = 0) = {
      var it=elements.iterator
      val res=collection.mutable.ListBuffer.empty[String]
      res.append("    " * indent+"(")
      while(it.hasNext) {
          res.appendAll(it.next.format(indent+1))
          if(it.hasNext) {
              res.update(res.size - 1, res.last+";")
          }
      }
      res.append("    " * indent+")")
      res
    }
  }
}
