import math._
import scala.util._

//See http://en.wikipedia.org/wiki/Maze_solving_algorithm#Pledge_algorithm
// for next levels
object Player {

  def main(args: Array[String]) {
    // r: number of rows.
    // c: number of columns.
    // a: number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
    val Array(r, c, alarm) = for (i <- readLine split " ") yield i.toInt

    case class Visited(data:Map[Point, Map[Point, Int]]=Map.empty) {
        def counts(position:Point):Map[Point,Int] = {
            data.getOrElse(position, Map.empty).withDefaultValue(0)
        }
        
        def visit(position:Point, direction:Point):Visited={
            val atPos1=counts(position)
            val updated1=atPos1.updated(direction, atPos1(direction)+1)
            val opp=direction.opposite
            val pos2=position+opp
            val atPos2=counts(pos2)
            val updated2=atPos2.updated(opp, atPos2(opp)+1)
            copy(data=data.updated(position, updated1).updated(pos2, updated2))
        }
    }

    case class Situation(
        kirk: Point, 
        data: Seq[String],
        direction: Point = UP,
        back:Boolean=false, 
        pathFound:Boolean=false,
        visited:Visited=Visited()) {
            
        
        def cell(p: Point): Option[Area] = 
            cells.get(p)

        def convert(i:Int,j:Int): Area =  data(i)(j) match {
            case 'T' => Start
            case 'C' => CommandRoom
            case '.' => Free
            case '#' => Wall
            case '?' => Unknown
        }

      lazy val cells:Map[Point, Area] = (for {
        i <- 0 until r
        j <- 0 until c
        a = convert(i,j)
      } yield Point(j,i) -> a).toMap

      def commandRoomPos:Option[Point] = 
        (for ((p,a)<-cells if a==CommandRoom) yield p).headOption

      def startPos:Point = 
        (for ((p,a)<-cells if a==Start) yield p).head

      def isFree(p: Point) =
        cell(p) match {
            case Some(Wall)=>false
            case Some(CommandRoom) if !pathFound => false //would trigger the alarm too soon
            case Some(Unknown) => false
            case _ => true
        }

      override def toString = {
        val line: String = data(kirk.y).patch(kirk.x, Seq('K'), 1)
        data.patch(kirk.y, Seq(line), 1).mkString("\n")
      }

      def move(): Situation = if(back && pathFound) {
          Console.err.println("moving back")
          println(path(kirk,startPos).get.firstMove)
          this
      } else if(commandRoomPos==Some(kirk) && pathFound){
          copy(back=true).move()
      } else {
          commandRoomPos match {
            case Some(cr) =>
                Console.err.println("command room has been viewed")
                path(cr,startPos) match {
                    case Some(backPath) if backPath.length <= alarm+1 => 
                        Console.err.println("short path from command room to start found")
                        path(kirk, cr) match {
                            case Some(p) =>
                                Console.err.println("moving to CR")
                                println(p.firstMove)
                                copy(pathFound=true)
                            case None => 
                                Console.err.println("no path to CR yet")
                                copy(pathFound=true).exploreMove()
                        }
                    case _ =>
                        Console.err.println("no path from CR back to start yet")
                        exploreMove()

                }
            case None =>
                Console.err.println("command room not found yet")
                exploreMove()
        }
      }
      
    def exploreMove()={
        Console.err.println(s"exploring")
        //Console.err.println(visited)
        val candidates=for {
            d<-directions
            c=kirk.move(d)
            if isFree(c)
            score=unknownCount(c)
            count=visited.counts(c)(d)
        } yield (c,score,count,d)
        Console.err.println(candidates)
        val optimal=candidates.sortBy{case (pos,score,count,d)=> (-count,score)}.last
        val (newKirk,_,_,chosenDirection)=optimal
        println(chosenDirection.name)
        this.copy(direction=chosenDirection, visited=visited.visit(newKirk, chosenDirection))
    }
    
    def exploreMoveOld()={
        Console.err.println(s"exploring, following ${direction.name} wall")
        val directionsOrdered=directionsSeq.dropWhile(_ != direction.prev).take(4)
        val candidates=for {
            d<-directionsOrdered
            c=kirk.move(d)
            if isFree(c)
            score=unknownCount(c)
        } yield (c,score)
        Console.err.println(candidates)
        val bestScore=candidates.map(_._2).max
        val optimals=candidates.filter(_._2==bestScore).map(_._1)
        val chosenDirection=kirk.direction(optimals.head)
        println(chosenDirection.name)
        this.copy(direction=chosenDirection)
    }

    def unknownCount(p:Point):Int=
        (for {
            d <- directions
            i <- -2 to 2
            j <- -2 to 2
            t = p + Point(i,j)
            if cells.get(t)==Some(Unknown)
        } yield 1).sum

      def reach1(p: Point): Set[Point] =
        for (d <- directions.toSet if isFree(p + d)) yield p + d

      def path(from: Point, to: Point): Option[Path] =
        path(Set(Path(List(from))), to, Set(from))

      private def path(paths: Set[Path],
        to: Point,
        visited: Set[Point]): Option[Path] = {
       // Console.err.println(s"debug path(\n  paths=$paths, \n  to=$to, \n  visited=$visited)")
        val newPaths = for {
          p <- paths
          n <- reach1(p.end) diff visited
        } yield p.append(n)
        if (newPaths.isEmpty) None
        else {
          val uniqueEnds = for ((_, paths) <- newPaths.groupBy(_.end)) yield paths.head
          uniqueEnds.find(_.end == to) match {
            case Some(path) => Some(path)
            case None =>
              path(uniqueEnds.toSet, to, visited ++ paths.map(_.end))
          }
        }
      }
    }

    var situation: Situation = Situation(Point(0, 0), Nil)
    // game loop
    while (true) {
      // kr: row where Kirk is located.
      // kc: column where Kirk is located.
      val Array(kr, kc) = for (i <- readLine split " ") yield i.toInt
      val mapData = for (i <- 0 until r) yield readLine // C of the characters in '#.TC?' (i.e. one line of the ASCII maze).

      situation = situation.copy(Point(kc, kr), mapData)
      //Console.err.println(situation)

      situation = situation.copy(direction = situation.direction.prev).move()
    }
  }

  trait Area
  object Start extends Area
  object CommandRoom extends Area
  object Wall extends Area
  object Free extends Area
  object Unknown extends Area

  case class Point(c: Int, r: Int, name: String = "") {
    val x = c
    val y = r
    def +(p: Point) = Point(x + p.x, y + p.y)
    def -(p: Point) = Point(x - p.x, y - p.y)
    def *(i:Int)=Point(i*x,i*r)
    def move(p: Point) = this + p
  }

  val LEFT = Point(-1, 0, "LEFT")
  val RIGHT = Point(1, 0, "RIGHT")
  val UP = Point(0, -1, "UP")
  val DOWN = Point(0, 1, "DOWN")
  val directions = Seq(RIGHT, UP, LEFT, DOWN)
  val directionsSeq = directions ++ directions

  implicit class DirectionOp(direction: Point) {
    def next: Point =
      directionsSeq.dropWhile(_ != direction).drop(1).head

    def prev: Point =
      directionsSeq.reverse.dropWhile(_ != direction).drop(1).head
      
    def opposite:Point=
        directionsSeq.dropWhile(_ != direction).drop(2).head
      
    def direction(neighbour:Point):Point= 
        (for (d<-directions if (d+direction) == neighbour) yield d).head
  }

  case class Path(path: List[Point]) {
    def end = path.last
    def append(n: Point) = Path(path ::: List(n))
    def length = path.size
    def lastSection: (Point, Point) = (path(path.length - 2), path(path.length - 1))
    def firstSection: (Point, Point) = (path(0), path(1))
    def firstMove:String= {
        val (a,b)=firstSection
        a.direction(b).name
    }
  }
}
