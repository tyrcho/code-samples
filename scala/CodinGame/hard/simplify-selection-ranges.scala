import math._
import scala.util._

object Solution extends App {
  val l = readLine
  val numbers=l.substring(1,l.size - 1).split(",").map(_.toInt).toList.sorted

  def process(num:List[Int], start:Option[Int]=None, end:Int = -1):List[String]= (num,start) match {
    case (Nil,None) => Nil
    case (Nil,Some(i)) => List(transform(i,end))
    case (h::t, None) => process(t, Some(h), h)
    case (h::t, Some(i)) if h == end + 1 => process(t, Some(i), h)
    case (h::t, Some(i)) => transform(i, end) :: process(t, Some(h), h)
  }

  def transform(start:Int, end:Int):String= 
    if (end - start >= 2) 
      s"$start-$end"
        else  (start to end).mkString(",")

        Console.err.println(numbers)

        println(process(numbers).mkString(","))
}
