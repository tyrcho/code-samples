import math._
import scala.util._
import scala.annotation.tailrec

object Player extends App {
  val width = readInt // the number of cells on the X axis
  val height = readInt // the number of cells on the Y axis

  val cells = for {
    i <- 0 until height
    line = readLine // width characters, each either 0 or .
    (c, j) <- line.zipWithIndex
    if c != '.'
  } yield Cell(j, i, c.toString.toInt)

  Console.err.println(cells)

  val initialLinks = (for {
    c1 <- cells.toList
    c2 <- c1.neighboursBotRight
  } yield PossibleLink(c1, c2, 0, 2)).sortBy(l => -l.c2.neighbours.size - l.c1.neighbours.size)

  val sol = ProblemAPU.solve().get.simplify().filter(_.done)
  Console.err.println(sol.head.reachable())

  for (l <- sol.head.links if l.max > 0) println(s"${l.c1.x} ${l.c1.y} ${l.c2.x} ${l.c2.y} ${l.max}")

  case class NodeAPU(links: List[PossibleLink]) {
    def done = links.forall(_.done)

    @tailrec
    final def badCrosses(l: List[PossibleLink] = links): Boolean = l match {
      case h :: t => t.exists(_.crosses(h)) || badCrosses(t)
      case Nil    => false
    }

    def isLinked = reachable().size == cells.size

    def doubleCheckLinks: Boolean =
      cells.forall(c => doubleCheckLinks(c))

    def doubleCheckLinks(c: Cell): Boolean =
      links.filter(l => l.c1 == c || l.c2 == c).map(_.min).sum == c.links

    def isOk = !badCrosses() && isLinked

    @tailrec
    final def reachable(cells: List[Cell] = List(links.head.c1), seen: Set[Cell] = Set.empty): Set[Cell] = cells match {
      case Nil => seen
      case cell :: t =>
        val seen2 = seen + cell
        val newCells = for {
          l <- links
          if l.max > 0
          if l.c1 == cell || l.c2 == cell
          c <- List(l.c1, l.c2)
          if !seen2.contains(c)
        } yield c
        reachable(newCells ::: t, seen2 ++ newCells)
    }

    @tailrec
    final def simplify(todo: List[PossibleLink] = links, done: List[PossibleLink] = Nil, todoTried: Int = 0): List[NodeAPU] = { //set all links where only one possibility
      todo match {
        case Nil => List(NodeAPU(done))
        case h :: t =>
          val range = h.possibilities(todo ::: done)
          val updated = h.copy(min = range.start, max = range.end)
          if (range.size == 1) simplify(t, updated :: done, 0)
          else if (range.size < h.max - h.min + 1) simplify(t ::: List(updated), done, 0) // we learnt sth
          else if (todoTried <= todo.size) simplify(t ::: List(updated), done, todoTried + 1) // no progress, try next one
          else // we tried all with no progress
            for {
              i <- range.toList
              hLink = h.copy(min = i, max = i)
              newLinks = hLink :: t ::: done
              node = NodeAPU(newLinks)
            } yield node
      }
    }

    override def toString = {
      val undecided=links.count(! _.done)
      s"$hashCode - $undecided links"
    }
  }

  object ProblemAPU extends Backtracking.Problem {
    type Node = NodeAPU

    val root = NodeAPU(initialLinks)

    def analyse(node: NodeAPU) =
      if(!node.isOk) Rejected
      else if (node.done) Accepted
      else node.simplify() match {
        case Nil => Rejected
        case n::Nil =>
          if(n.isOk && n.done) Accepted
          else if(n==node) throw new Exception("simplification error")
          else Undecided(List(n))
        case l   => Undecided(l)
      }
  }

  case class PossibleLink(c1: Cell, c2: Cell, min: Int, max: Int) {
    val done = min == max

    lazy val vertical = c1.x == c2.x

    lazy val minY = math.min(c1.y, c2.y)
    lazy val maxY = math.max(c1.y, c2.y)
    lazy val minX = math.min(c1.x, c2.x)
    lazy val maxX = math.max(c1.x, c2.x)

    lazy val char: String =
      if (!done) "?"
      else if (max == 0) " "
      else if (vertical)
        if (min == 2) "H"
        else "|"
      else if (min == 2) "="
      else "-"

    lazy val innerCoords = for {
      i <- minY to maxY
      j <- minX to maxX
    } yield (j, i)

    def crosses(l: PossibleLink) =
      if (min > 0 && l.min > 0 &&
        (
          (vertical && !l.vertical &&
            minY < l.c1.y && maxY > l.c1.y &&
            l.minX < c1.x && l.maxX > c1.x) ||
            (!vertical && l.vertical &&
              l.minY < c1.y && l.maxY > c1.y &&
              minX < l.c1.x && maxX > l.c1.x))) {
        Console.err.println(s"$this crosses $l")
        true
      } else false

    def possibilities(allLinks: List[PossibleLink]): Range =
      if (done) (min to max) else {
        def compute(c: Cell) = {
          val linksToCell = allLinks.filter(l => (l.c1 == c || l.c2 == c) && l != this)
          val minToCell = linksToCell.map(_.min).sum
          val maxToCell = linksToCell.map(_.max).sum
          (c.links - maxToCell, c.links - minToCell)
        }
        val (min1, max1) = compute(c1)
        val (min2, max2) = compute(c2)
        val mini = math.max(0, math.max(min1, min2))
        val maxi = math.min(2, math.min(max1, max2))
        (mini to maxi)
      }
  }

  case class Cell(x: Int, y: Int, links: Int) {
    lazy val neighbours: List[Cell] = {
      val predicates: List[Cell => Boolean] = List(
        c => c.y == y && c.x > x,
        c => c.y == y && c.x < x,
        c => c.x == x && c.y > y,
        c => c.x == x && c.y < y)
      (for { p <- predicates }
        yield (for {
        c <- cells if p(c)
      } yield c).headOption).flatten
    }

    lazy val neighboursBotRight: List[Cell] = {
      val predicates: List[Cell => Boolean] = List(
        c => c.y == y && c.x > x,
        c => c.x == x && c.y > y)
      (for { p <- predicates }
        yield (for {
        c <- cells if p(c)
      } yield c).headOption).flatten
    }
  }

}

import scala.annotation.tailrec

object Backtracking {
  trait Problem {
    type Node

    trait Result
    case object Rejected extends Result
    case object Accepted extends Result
    case class Undecided(children: List[Node]) extends Result

    def root: Node

    def analyse(node: Node): Result

    // gets the first solution only
    @tailrec
    final def solve(candidates: List[Node] = List(root)): Option[Node] = candidates match {
      case candidate :: tail =>
        analyse(candidate) match {
          case Accepted => Some(candidate)
          case Rejected =>
            Console.err.println(s"Rejected : $candidate")
            solve(tail)
          case Undecided(children) =>
            Console.err.println(s"Undecided with ${children.size} children: $candidate")
            solve(children ::: tail)
        }
      case Nil => None
    }
  }
}

