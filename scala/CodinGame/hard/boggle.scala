import scala.io.StdIn._

object Solution extends App {


  val line1 = readLine()
  val line2 = readLine()
  val line3 = readLine()
  val line4 = readLine()
  val words = Seq.fill(readInt())(readLine())
  process(Seq(line1, line2, line3, line4), words).foreach(println)

  def process(grid: Seq[String], words: Seq[String]): Seq[Boolean] =
    words.map(w => solveWord(w, grid))

  def solveWord(word: String, grid: Seq[String], paths: Set[List[Pos]] = Set.empty[List[Pos]]): Boolean =
    word.toList match {
      case h :: t =>
        val next = nextPaths(grid, paths, h)
        if (next.isEmpty) false
        else solveWord(t.mkString, grid, next)
      case Nil => true
    }


  def findWord(grid: Seq[String], word: String): Boolean = {
    grid.exists(l => l.contains(word))
  }

  def findCoord(grid: Seq[String], c: Char): List[Pos] =
    if (grid.isEmpty)
      Nil
    else
    // We assume grid is always square
      findCoord(grid, c, grid.size)


  private def findCoord(grid: Seq[String], c: Char, size: Int) = {
    for {
      row <- 0 until size
      col <- 0 until size
      if grid(row)(col) == c
    } yield Pos(row, col)
  }.toList

  def neighbors(grid: Seq[String], pos: Pos): Set[Pos] = {
    for {
      row <- pos.row - 1 to pos.row + 1
      col <- pos.col - 1 to pos.col + 1
      if row >= 0 && row < grid.size
      if col >= 0 && col < grid.size
      if row != pos.row || col != pos.col
    } yield Pos(row, col)
  }.toSet

  def filterNeighbors(grid: Seq[String], pos: Pos, c: Char): Set[Pos] = {
    neighbors(grid, pos) filter { case Pos(row, col) => grid(row)(col) == c }
  }

  def filterNeighborsVisited(grid: Seq[String], pos: Pos, c: Char, visited: Set[Pos]): Set[Pos] =
    filterNeighbors(grid, pos, c) diff visited

  def nextPaths(grid: Seq[String], paths: Set[List[Pos]], letter: Char): Set[List[Pos]] =
    if (paths.isEmpty)
      findCoord(grid, letter).map(pos => List(pos)).toSet
    else
      addNeighborsToPaths(grid, paths, letter)


  private def addNeighborsToPaths(grid: Seq[String], paths: Set[List[Pos]], letter: Char)

  =
    for {
      path <- paths
      neighbors = filterNeighborsVisited(grid, path.head, letter, path.toSet)
      n <- neighbors
    } yield n :: path


}

case class Pos(row: Int, col: Int)
