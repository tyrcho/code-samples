import scala.io.StdIn._

object Solution extends App {
  val Array(n, k) = readLine.split(" ").map(_.toInt)

  Candies.solve(k, n).foreach(line => println(line.mkString(" ")))
}

object Candies {
  def solve(canEat: Int, totalCandies: Int, previous: Seq[Int] = Nil): Seq[Seq[Int]] =
    if (totalCandies == 0)
      Seq(previous)
    else
      (1 to (canEat min totalCandies)).flatMap(eaten => solve(canEat, totalCandies - eaten, previous :+ eaten))

}
