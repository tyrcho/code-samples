object Solution extends App {
  val n = readInt
  val sequences = for (i <- 0 until n) yield readLine

  def proximity(a: String, b: String): Int = {
    val m = a.length min b.length
    if (a.containsSlice(b) || b.containsSlice(a)) m
    else proximity1(a.takeRight(m), b.take(m), m)
  }

  def proximity1(a: String, b: String, common: Int): Int = {
    if (common == 0) 0
    else if (a.takeRight(common) == b.take(common)) common
    else proximity1(a, b, common - 1)
  }

  def length(permutation: List[String], acc: Int = 0): Int = {
    if (permutation.size == 1) permutation.head.size + acc
    else {
      val List(a, b) = permutation.take(2)
      length(permutation.tail, acc + a.size - proximity(a, b))
    }
  }

  for {
    a <- sequences
    b <- sequences
    if a != b
    p = proximity(a, b)
  } Console.err.println(s"$a $b $p")

  val res = (for {
    permutation <- sequences.permutations
    l = length(permutation.toList)
  } yield l).min

  println(res)
}

