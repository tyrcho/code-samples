import io.StdIn.readLine

object Solution extends App {
  val lines = Vector.fill(4)(readLine.map(_.toString.toInt).toVector)
  val sudoku = Sudoku(lines, initialCandidates(lines))
  Console.err.println(sudoku.debugString)
  println(sudoku.solve.get.debugString)

  case class Sudoku(lines: Vector[Vector[Int]], candidates: Vector[Vector[Vector[Int]]]) {
    def solve: Option[Sudoku] =
      if (undecided.isEmpty) Some(this)
      else {
        for {
          n <- next
          s <- n.solve
        } yield s
      }.headOption

    def next: Vector[Sudoku] = {
      for {
        (l, c) <- undecided.toVector
        cand <- candidates(l)(c)
        n <- set(l, c, cand)
      } yield n
    }

    def sameBlock(l: Int, line: Int, c: Int, col: Int) =
      line / 2 == l / 2 && c / 2 == col / 2

    def set(line: Int, col: Int, value: Int): Option[Sudoku] = {
      val filteredCandidates = for {
        l <- Vector(0, 1, 2, 3)
      } yield for {
        c <- Vector(0, 1, 2, 3)
        cands = candidates(l)(c)
        updated = if (l == line && c == col) Vector(value)
        else if (l == line || c == col || sameBlock(l, line, c, col)) cands.filterNot(value.==)
        else cands
      } yield updated

      if (filteredCandidates.flatten.exists(_.isEmpty)) None
      else Some(Sudoku(lines.updated(line, lines(line).updated(col, value)), filteredCandidates))
    }

    def undecided: Option[(Int, Int)] = (for {
      l <- 0 to 3
      c <- 0 to 3
      if lines(l)(c) == 0
    } yield (l, c)).headOption

    val debugString = (for {
      l <- 0 to 3
    } yield lines(l).mkString).mkString("\n")
  }

  def initialCandidates(lines: Vector[Vector[Int]]): Vector[Vector[Vector[Int]]] = {
    lines.map { line =>
      line.map {
        case 0 => Vector(1, 2, 3, 4)
        case n => Vector(n)
      }
    }
  }
}
