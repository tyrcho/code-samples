import java.io.FileInputStream
import scala.xml.XML
import scalaxb._
import dota._

object Demo extends App {
  val xmlData = XML.load(new FileInputStream("src/main/resources/dota.xml"))

  val items = fromXML[Items](xmlData)

  println(items.shop.head.item.head.name)

}
