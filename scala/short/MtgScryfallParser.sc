import upickle.default._
import scala.io.Codec
import java.nio.charset.CodingErrorAction
import scala.util.Try

implicit val codec = Codec("UTF-8")
codec.onMalformedInput(CodingErrorAction.REPLACE)
codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

val cards = scala.io.Source
  .fromFile("""C:\Users\Michel\Downloads\default-cards-20240109220508.json""")
  .getLines()

val ng = cards
  .flatMap(getCard("B"))
  .flatMap(c => nGrams(3)(c.flavor_text))
  .filter(!_.exists(mehWords))
ng.toList
  .groupBy(identity)
  .mapValues(_.size)
  .toList
  .sortBy(-_._2)
  .take(200)
  .foreach(println)

def getCard(i: String)(line: String): Seq[Card] = Try {
  val c = read[Card](line.dropRight(1))
  (c +: c.card_faces).filter(_.color_identity.contains(i))
}.getOrElse(Nil)

def nGrams(n: Int)(s: String): Seq[Seq[String]] = {
  s.toLowerCase
    .split("[.,;?!]")
    .flatMap(_.split("[^a-zA-Z']").toSeq.filter(_.nonEmpty).sliding(n).toSeq)
}

case class Card(
    name: String,
    color_identity: Seq[String] = Nil,
    oracle_text: String = "",
    card_faces: Seq[Card] = Nil,
    flavor_text: String = ""
)

object Card {
  implicit val rw: ReadWriter[Card] = macroRW
}

lazy val mehWords = Set(
  "a",
  "the",
  "of",
  "is",
  "for",
  "to",
  "but",
  "i",
  "am",
  "not",
  "do",
  "in",
  "you",
  "be",
  "can",
  "it",
  "no"
)
