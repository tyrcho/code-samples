import scala.collection.generic.CanBuildFrom
import scala.util.Random

object IterableUtils {

  implicit class IterableOps[C[T] <: Seq[T], T](iterable: C[T]) {

    def sample(count: Int)(implicit cbf: CanBuildFrom[C[T], T, C[T]]): C[T] = {
      val builder = cbf()
      Random.shuffle(iterable).take(count).foreach(builder.+=)
      builder.result()
    }

    def distinctBy[S](by: T => S)(implicit cbf: CanBuildFrom[C[T], T, C[T]]): C[T] = {
      val builder = cbf()
      val seen    = collection.mutable.HashSet[S]()

      for (t <- iterable) {
        val key = by(t)
        if (!seen(key)) {
          builder += t
          seen += key
        }
      }

      builder.result
    }

  }
}
