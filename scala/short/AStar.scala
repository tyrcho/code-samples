import scala.annotation.tailrec

object AStarDemo extends App {
  def isBlocked(p: Point): Boolean = Point(4, 5).dist(p) <= 3

  case class Point(x: Int, y: Int) {

    def dist(that: Point): Double =
      math.sqrt((this.x - that.x) * (this.x - that.x) + (this.y - that.y) * (this.y - that.y))

    def neighbors: Iterable[(Point, Double)] =
      for {
        dx <- -1 to 1
        dy <- -1 to 1
        if dx != 0 || dy != 0
        d = if (dx == 0 || dy == 0) 1 else 1.6
        p = Point(x + dx, y + dy)
        if !isBlocked(p)
      } yield p -> d
  }

  val origin = Point(0, 0)
  val target = Point(6, 8)

  val aStar = AStar[Point, Double](_.neighbors, _ dist _)
  val path  = aStar.shortestPath(origin, target).get
  println(path)
  for {
    x <- 0 to 10
  } {
    for { y <- 0 to 10 } {
      val p = Point(x, y)
      if (path.contains(p)) print("X")
      else if (isBlocked(p)) print("O")
      else print(".")
    }
    println()
  }
}

/**
  * @param reachable lists which nodes can be reached from a given one and at which cost
  * @param heuristicDistance estimates the distance. Must be >= actual cost
  */
case class AStar[Node, Cost: Numeric](
    reachable: Node => Iterable[(Node, Cost)],
    heuristicDistance: (Node, Node) => Cost
) {
  private val numeric = implicitly[Numeric[Cost]]
  import numeric.{mkNumericOps, mkOrderingOps, zero}

  def shortestPath(start: Node, end: Node): Option[List[Node]] =
    State(
      open = Set(start),
      fScore = Map(start -> heuristicDistance(start, end)),
      gScore = Map(start -> zero)
    ).pathTo(end)

  case class State(
      open: Set[Node],
      fScore: Map[Node, Cost], //the total cost of getting from the start node to the goal by passing by that node. That value is partly known, partly heuristic.
      gScore: Map[Node, Cost], // cost of getting from the start node to that node.
      closed: Set[Node] = Set.empty,
      parents: Map[Node, Node] = Map.empty
  ) {

    @tailrec final def pathTo(end: Node): Option[List[Node]] =
      if (open.isEmpty) None
      else {
        val u = open.minBy(fScore)
        if (u == end) Some(pathFromParents(u))
        else copy(closed = closed + u, open = open - u).step(u).pathTo(end)
      }

    def step(node: Node): State =
      reachable(node).foldLeft(this) {
        case (state, (next, cost)) => state.updateIfFaster(node, next, cost)
      }

    private def updateIfFaster(node: Node, next: Node, cost: Cost): State = {
      val tentativeGScore = gScore(node) + cost
      if (closed(next) || (open.exists(next.==) && gScore(next) <= tentativeGScore)) this
      else {
        copy(
          open = open + next,
          parents = parents.updated(next, node),
          gScore = gScore.updated(next, tentativeGScore),
          fScore = fScore.updated(next, gScore(node) + heuristicDistance(node, next))
        )
      }
    }

    @tailrec private def pathFromParents(dest: Node, acc: List[Node] = Nil): List[Node] =
      parents.get(dest) match {
        case None         => dest :: acc
        case Some(parent) => pathFromParents(parent, dest :: acc)
      }
  }
}
