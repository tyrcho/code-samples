import scala.io.Source
import scala.util.parsing.json.JSON

object Sort extends App {
  //enter your own values, these won't work !
  val listId = "87308c95352724c5f1"
  val key = "01e5ed84e035d27b365db068c"
  val token = "56027af793b49845fde60505aa429ad82c6ec332a1f98170246"
  val data = Source.fromURL(s"https://api.trello.com/1/lists/$listId/cards?key=$key&token=$token").getLines mkString

  println(data)
  val json = JSON.parseFull(data).get.asInstanceOf[List[Map[String, Any]]]
  println(json)

  def display(m: Map[String, _]) =
    s"${m("name")} (${m("dateLastActivity")})"

  json.sortBy(_("dateLastActivity").toString) map display foreach println
}