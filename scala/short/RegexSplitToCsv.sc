#!/usr/bin/env amm
!#

import $ivy.`com.lihaoyi::mainargs:0.2.3`

import mainargs.main

@main
def split(fileName: String) = {
  val data = scala.io.Source.fromFile(fileName).getLines().toList
  println(s"name,description,${Tech.fieldNames.sorted.mkString(",")}")
  groupLines(data).sortBy(_.name).map(_.csv).foreach(println)
}

def groupLines(lines: List[String], acc: List[Tech] = Nil): List[Tech] =
  lines match {
    case Nil => acc
    case line :: next => val (sameTech, rest) = next.span(l => !Tech.isTitle(l))
      groupLines(rest, Tech(line::sameTech) :: acc)
  }

case class Tech(name: String, description: String, data: Map[String, String] = Map.empty) {
  def csv: String = s""""$name","$description",${data.toList.sortBy(_._1).map(_._2).mkString("\"", "\",\"", "\"")}"""
}

object Tech {
  def apply(lines: List[String]): Tech = {
    val name=lines.head
    val (description, rest) = lines.tail.span(l => Tech.isField(l).isEmpty)
    val properties = groupLines(rest)
    fieldNames.filter(f => !properties.contains(f)).foreach(f => System.err.println(s"missing $f in $name"))
    Tech(name, description.mkString, properties)
  }

  def groupLines(lines: List[String], acc: Map[String, String] = Map.empty): Map[String, String] =
    lines match {
      case Nil => acc
      case line :: next => val (sameField, rest) = next.span(l => Tech.isField(l).isEmpty)
        val (fieldName, value) = splitField(line)
        groupLines(rest, acc.updated(fieldName, (value :: sameField).mkString))
    }

  def splitField(line: String) = {
    val fieldName = isField(line).get
    val re = s"$fieldName : (.*)".r
    val re(value) = line
    (fieldName, value)
  }

  def isTitle(line: String) = line.toUpperCase == line

  def isField(line: String) = fieldNames.find(f => line.startsWith(s"$f : "))

  val fieldNames = List(
    "Art Martial",
    "Prérequis",
    "Arme nécessaire",
    "Type",
    "Coût",
    "Précision",
    "Puissance",
    "Localisation",
    "Combo",
    "Règles Spéciales",
    "Attributs",
  )
}
