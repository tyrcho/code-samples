#!/usr/bin/env amm
!#

import $ivy.`com.lihaoyi::mainargs:0.2.3`

import scala.util.Random
import mainargs.main

@main
def stats(): Unit =
  for (advantage <- -4 to 4) {
    val size = 100_000
    val total = Seq.fill[BigDecimal](size)((FateRoll().roll4dF + advantage) max 0).sum
    val average = total / size
    println(s"$advantage\t$average")
  }

@main
def fights(repeats: Int = 100): Unit = {
  val firstTeam = Set.tabulate(2)(hero)
  val secondTeam = Set.tabulate(4)(monster)
  // Set.tabulate(7)(minion)

  val firstWins = Seq.fill(repeats)(Judge().firstGroupWins(firstTeam, secondTeam)).count(true.==)
  println(s"$firstTeam VS $secondTeam : $firstWins / $repeats")
}

def minion(i: Int): FateCharacter =
  FateCharacter(
    s"i$i",
    attack = 1,
    defense = 1,
    initiative = 1,
    consequencesAvailable = Set(),
    stressAvailable = Set(1, 2)
  )

def monster(i: Int): FateCharacter =
  FateCharacter(
    s"O$i",
    attack = 2,
    defense = 2,
    initiative = 3,
    consequencesAvailable = Set(),
    stressAvailable = Set(1, 2)
  )

def hero(i: Int): FateCharacter =
  FateCharacter(s"H$i")

case class Judge(roll: () => Int = () => FateRoll().roll4dF) {
  def runOneRound(
                   t1: Set[FateCharacter],
                   t2: Set[FateCharacter],
                   turnOrder: List[String]
                 ): (Set[FateCharacter], Set[FateCharacter]) =
    turnOrder match {
      case Nil => (t1, t2)
      case name :: tail =>
        t1.find(_.name == name) match {
          case Some(p1) =>
            t2.minByOption(_.maxDamageBeforeOut) match {
              case Some(p2) =>
                runOneRound(t1, t2 - p2 ++ attack(p1, p2), tail)
              case None => runOneRound(t1, t2, tail)
            }

          case None =>
            t2.find(_.name == name) match {
              case Some(p2) =>
                t1.minByOption(_.maxDamageBeforeOut) match {
                  case Some(p1) =>
                    runOneRound(t1 - p1 ++ attack(p2, p1), t2, tail)
                  case None => runOneRound(t1, t2, tail)
                }
              case None => runOneRound(t1, t2, tail)
            }
        }

    }

  def firstGroupWins(
                      g1: Set[FateCharacter],
                      g2: Set[FateCharacter]
                    ): Boolean = {
    val turnOrder =
      Random.shuffle(g1 ++ g2).toList.sortBy(_.initiative).reverse.map(_.name)
    var (t1, t2) = (g1, g2)
    var round = 1
    while (!t1.forall(_.out) && !t2.forall(_.out)) {
      println(s"round $round ${
        turnOrder
          .filter(name => (t1 ++ t2).exists(c => c.name == name && !c.out))
      }")
      val res = runOneRound(t1, t2, turnOrder)
      t1 = res._1
      t2 = res._2
      round += 1
    }
    println()
    t2.forall(_.out)
  }

  def attack(
              attacker: FateCharacter,
              defender: FateCharacter
            ): Option[FateCharacter] = {
    val attackRoll = attacker.attack + roll()
    // println(s"$attacker attacks with a total of $attackRoll")
    val defenseRoll = defender.defense + roll()
    val shifts = attackRoll - defenseRoll
    // println(s"$defender defended with a total of $defenseRoll")
    val hit =
      if (shifts > 0) defender.damaged(shifts + attacker.weapon)
      else defender
    if (hit.out) None else Some(hit)
  }
}

case class FateCharacter(
                          name: String,
                          attack: Int = 3,
                          defense: Int = 2,
                          weapon: Int = 1,
                          armor: Int = 0,
                          initiative: Int = 1,
                          stressAvailable: Set[Int] = Set(1, 2, 3),
                          consequencesAvailable: Set[Int] = Set(2, 4, 6),
                          out: Boolean = false
                        ) {
  val maxStress = stressAvailable.maxOption.getOrElse(0)
  val maxConsequence = consequencesAvailable.maxOption.getOrElse(0)
  val maxDamageBeforeOut =
    stressAvailable.sum + consequencesAvailable.sum + armor

  override def toString: String =
    s"$name ${stressAvailable.mkString("-")} ${consequencesAvailable.mkString("=")}"

  def receiveStress(stress: Int): FateCharacter =
    if (stress <= 0) this
    else {
      val neededConsequence = stress - maxStress

      if (neededConsequence > 0)
        (for {
          actualConsequence <- consequencesAvailable
            .filter(neededConsequence.<=)
            .minOption
          remainingStress = stress - actualConsequence
          actualStress <- stressAvailable.filter(remainingStress.<=).minOption
        } yield copy(
          stressAvailable = stressAvailable - actualStress,
          consequencesAvailable = consequencesAvailable - actualConsequence
        )).getOrElse(copy(out = true))
      else {
        val actualStress = stressAvailable.filter(stress.<=).min
        copy(stressAvailable = stressAvailable - actualStress)
      }
    }

  def damaged(damage: Int): FateCharacter =
    receiveStress(damage - armor max 0)

}

case class FateRoll(seed: Long = Random.nextLong()) {
  private val random = new Random(seed)

  def rollOneDice: Int = random.nextInt(3) - 1

  def roll4dF: Int = roll2dF + roll2dF

  def roll2dF: Int = rollOneDice + rollOneDice

}
