// brew install coursier
//
//  AWS env variables for authentication to S3
// $(aws configure export-credentials  --format env)
// need to specify scala version (spark is not yet available for 3.0) and jvm (some issues with JRE 17+)
// coursier launch ammonite --scala 2.13 --jvm adoptium-jre:1.11.0.17 -- spark.sc

import $ivy.`org.apache.spark::spark-sql:3.5.1`
import $ivy.`org.apache.spark::spark-hadoop-cloud:3.5.1`

import org.apache.spark.sql.SparkSession

val spark = SparkSession.builder()
    .appName("Simple Application")
    .master("local[*]")
    .config("spark.driver.bindAddress", "127.0.0.1")
    .getOrCreate()

//val df=Iterator.tabulate(100){(i:Int) => println(s"reading file $i");spark.read.parquet(s"/tmp/data/$i")}.reduce(_ union _)
val df=spark.read.parquet("s3a://michel-daviot-logs-test-2/michel-daviot-logs-test-2/inventory-michel-daviot/data/0ccacc26-b284-49a4-8ed3-8092b9df3306.parquet")

df.createOrReplaceTempView("data")
//spark.sql("SELECT * FROM data WHERE col_1 LIKE 'ab%' and col_2 < 5000").show()
spark.sql("SELECT object_owner FROM data WHERE key LIKE 'copiedlogs/%' AND size>100").show()


