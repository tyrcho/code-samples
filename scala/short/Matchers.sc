import $ivy.`com.github.julien-truffaut::monocle-macro:2.0.1`
import monocle.macros.GenLens
import monocle.Lens

object matchers {

  trait Matcher[E] extends (E => Boolean) {
    def lenses: Seq[Lens[E, _]]

    def sample(template: E): E

    def ==>[V](value: V): SimpleDecision[E, V] = SimpleDecision(this, value)

    def **[Q](that: SimpleMatcher[E, Q]): ConjonctionMatcher[E]

  }

  case class SimpleMatcher[E, P: Ordering](threshold: P, lens: Lens[E, P]) extends Matcher[E] {
    val lteq: (P, P) => Boolean = implicitly[Ordering[P]].lteq

    def apply(element: E): Boolean = lteq(lens.get(element), threshold)

    override def lenses: Seq[Lens[E, _]] = Seq(lens)

    override def sample(template: E): E = lens.set(threshold)(template)

    def **[Q](that: SimpleMatcher[E, Q]): ConjonctionMatcher[E] = ConjonctionMatcher(Seq(this, that))
  }

  case class ConjonctionMatcher[E](matchers: Seq[SimpleMatcher[E, _]]) extends Matcher[E] {
    def apply(element: E): Boolean = matchers.forall(_.apply(element))

    override def lenses: Seq[Lens[E, _]] = matchers.map(_.lens)

    def **[Q](that: SimpleMatcher[E, Q]): ConjonctionMatcher[E] = ConjonctionMatcher(that +: matchers)

    override def sample(template: E): E = matchers.foldLeft(template) {
      case (value, m: SimpleMatcher[E, _]) => m.sample(value)
    }

  }

  trait Decision[E, V] extends PartialFunction[E, V] {
    def lenses: Seq[Seq[Lens[E, _]]]

    def ||(that: SimpleDecision[E, V]): DisjonctionDecision[E, V]
  }

  case class SimpleDecision[E, V](matcher: Matcher[E], output: V) extends Decision[E, V] {
    def apply(element: E): V = output

    override def isDefinedAt(x: E): Boolean = matcher(x)

    override def lenses: Seq[Seq[Lens[E, _]]] = Seq(matcher.lenses)

    def ||(that: SimpleDecision[E, V]): DisjonctionDecision[E, V] = DisjonctionDecision(Seq(this, that))
  }

  case class DisjonctionDecision[E, V](decisions: Seq[SimpleDecision[E, V]]) extends Decision[E, V] {
    // get is safe here because we are implementing PartialFunction
    def apply(element: E): V = decisions.find(_.isDefinedAt(element)).get.apply(element)

    override def isDefinedAt(x: E): Boolean = decisions.exists(_.isDefinedAt(x))

    override def lenses: Seq[Seq[Lens[E, _]]] = decisions.map(_.matcher.lenses)

    def ||(that: SimpleDecision[E, V]): DisjonctionDecision[E, V] = DisjonctionDecision(decisions :+ that)

    def samples(template: E): Seq[E] = decisions.map(_.matcher.sample(template))
  }

}


import matchers._

case class Dimensions(l: Length = 50, L: Length = 100, h: Length = 150) extends Ordered[Dimensions] {
  def compare(that: Dimensions): Int =
    if (this.l <= that.l &&
      this.L <= that.L &&
      this.h <= that.h) -1 else 1
}

case class Delivery(
                     maxWeight: Mass = 30,
                     totalWeight: Mass = 30,
                     dimensions: Dimensions = Dimensions(),
                     chosenRoom: Boolean = false,
                     distance: Distance = 10
                   )

type Price = Float
type Distance = Float

type Mass = Int
type Length = Int


val ground = SimpleMatcher(false, GenLens[Delivery](_.chosenRoom))
def totalWeight(kg: Mass): SimpleMatcher[Delivery, Mass] = SimpleMatcher(kg, GenLens[Delivery](_.totalWeight))
def maxWeight(kg: Mass): SimpleMatcher[Delivery, Mass] = SimpleMatcher(kg, GenLens[Delivery](_.maxWeight))
def dimensions(l: Int, L: Int, h: Int): SimpleMatcher[Delivery, Dimensions] = SimpleMatcher(Dimensions(l, L, h), GenLens[Delivery](_.dimensions))
val m: ConjonctionMatcher[Delivery] = ground ** totalWeight(30)

println(s"true <= false : ${true <= false}")
println(s"false <= true : ${false <= true}")

println(Delivery())
if (m(Delivery(chosenRoom = true))) println("m") else println("no m")

val simple: SimpleDecision[Delivery, Double] = totalWeight(10) ** maxWeight(5) ** ground ==> 8.99

println(simple.lift(Delivery(totalWeight = 14, maxWeight = 4)))

val pricer: DisjonctionDecision[Delivery, Double] =
  dimensions(35, 35, 35) ** maxWeight(5) ** totalWeight(5) ==> 8.99 ||
    dimensions(80, 100, 150) ** maxWeight(30) ** totalWeight(100) ** ground ==> 20 ||
    dimensions(80, 100, 150) ** maxWeight(30) ** totalWeight(100) ==> 25

println(pricer.lift(Delivery(chosenRoom = true)))

println(pricer.samples(Delivery(chosenRoom = true)))


