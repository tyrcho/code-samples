val jsonStr=
  """{
    |    "login": "Ammonite-Bot",
    |    "id": 20607116,
    |    "node_id": "MDQ6VXNlcjIwNjA3MTE2",
    |    "gravatar_id": "",
    |    "type": "User",
    |    "site_admin": false,
    |    "tags" : ["a", "c"]
    |}
    |""".stripMargin

val data = ujson.read(jsonStr)

data("tags").arr += "d"

data.obj += "name" -> "toto"

println(data)
