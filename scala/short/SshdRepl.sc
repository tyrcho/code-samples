#!/usr/bin/env amm
!#

// https://ammonite.io/#RemoteREPL

// run from a real terminal (not from IJ) to avoid this error "sh: /dev/tty: Device not configured"
// then connect from another terminal with "ssh localhost -p 22222"

// TODO: handle proper authentication (either password or private key)

import $ivy.`com.lihaoyi:ammonite-sshd_2.13.10:2.5.5`

import org.apache.sshd.server.auth.password.AcceptAllPasswordAuthenticator
import ammonite.sshd._

val replServer = new SshdRepl(
  SshServerConfig(
    address = "localhost", // or "0.0.0.0" for public-facing shells
    port = 22222, // Any available port
    passwordAuthenticator = Some(AcceptAllPasswordAuthenticator.INSTANCE)
  )
)
replServer.start()
Thread.sleep(100000)