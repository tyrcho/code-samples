import $ivy.`org.scalatest::scalatest:3.2.12`

import org.scalatest._
import flatspec._
import matchers._

def solveAll(numbers: List[Int], target: Int): List[Computation] = {
  val combinaisons = allSums(numbers)
  val tries = combinaisons.flatMap(useAllOperations)
  tries.filter(target == _.eval.getOrElse(0))
}

private def useAllOperations(c: Computation): List[Computation] =
  c match {
    case Leaf(i) => List(Leaf(i))
    case Node(computation1, computation2, _) =>
      for {
        operation <- List(Add, Prod, Sub, Div)
        c1 <- useAllOperations(computation1)
        c2 <- useAllOperations(computation2)
        computation <- operation.possibleComputations(c1, c2)
        if computation.eval.isDefined
      } yield computation
  }

private def allSums(values: List[Int]): List[Computation] =
  values match {
    case Nil => Nil
    case head :: tail =>
      val next = allSums(tail)
      Leaf(head) :: next ::: next.flatMap(allSums(_, head))
  }

private def allSums(c: Computation, v: Int): List[Computation] =
  Node(Leaf(v), c) :: (c match {
    case l: Leaf => Nil
    case Node(l, r, _) => allSums(l, v).map(Node(_, r)) ::: allSums(r, v).map(Node(l, _))
  })

trait Computation {
  def eval: Option[Int]
}

case class Node(a: Computation, b: Computation, operation: Operation = Add) extends Computation {


  def eval = (a.eval, b.eval) match {
    case (Some(ar), Some(br)) => operation.eval(ar, br)
    case _ => None
  }

  override def toString = s"($a $operation $b) => ${eval.getOrElse('?')}"
}

case class Leaf(i: Int) extends Computation {
  def eval = Some(i)

  override val toString = i.toString
}

case class Operation(commutative: Boolean, repr: Char, eval: (Int, Int) => Option[Int]) {
  override val toString = repr.toString

  def possibleComputations(a: Computation, b: Computation): List[Node] =
    if (commutative) List(Node(a, b, this))
    else List(Node(a, b, this), Node(b, a, this))
}

val Add = Operation(true, '+', (a, b) => Some(a + b))
val Prod = Operation(true, '*', (a, b) => Some(a * b))
val Sub = Operation(false, '-', (a, b) => if (a > b) Some(a - b) else None)
val Div = Operation(false, '/', (a, b) => if (b != 0 && a % b == 0) Some(a / b) else None)


class LcebSpec extends AnyFlatSpec with should.Matchers {
  "lceb" should "solve 100-((2+4)x10)+7x50 = 390" in {
    val s = solveAll(List(100, 2, 4, 10, 7, 50), 390)
    println(s.head)
    s.foreach(_.eval.get shouldBe 390)
  }
}

(new LcebSpec).execute()
