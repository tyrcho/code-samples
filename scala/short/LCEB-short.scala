import Function.unlift

def desc(i: Int): (Int, String) = (i, i.toString)

def solve(numbers: Seq[(Int, String)], target: Int): Option[String] = numbers match {
  case (h, desc) :: _ if h == target => Some(desc)
  case (i, di) :: (j, dj) :: t =>
    List(
      Some(i + j, s"($di + $dj)"),
      Some(i * j, s"($di * $dj)"),
      Some(i - j, s"($di - $dj)"),
      if (j != 0 && i % j == 0) Some(i / j, s"($di / $dj)") else None
    ).flatten.flatMap(a => solvePerm(a :: t, target)).headOption
  case _ => None
}

def solvePerm(numbers: Seq[(Int, String)], target: Int): Option[String] =
  numbers.permutations.collectFirst(unlift(solve(_, target)))

@main
def run(args: Int*) =
  if (args.isEmpty)
    println(solvePerm(List(100, 2, 4, 10, 7, 50).map(desc), 390))
  else println(solvePerm(args.tail.map(desc), args.head))
