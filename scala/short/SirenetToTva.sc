import mainargs.main

import scala.io.Source
import scala.util.{Failure, Success, Try}

@main
def readAll(fileName: String): Unit =
  Source.fromFile(fileName).getLines().foreach(processLine)

def processLine(siren: String): Unit = {
  Try(requests.get(s"https://entreprise.data.gouv.fr/api/sirene/v1/siren/$siren")) match {
    case Success(resp) =>
      val j = ujson.read(resp.text())
      j.obj.get("numero_tva_intra") match {
        case Some(tva) => println(s"$siren,${tva.str}")
        case None => println(s"$siren,ko")
      }
    case Failure(e) => println(s"$siren $e")
  }


}
