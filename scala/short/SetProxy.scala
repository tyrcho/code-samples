object SetProxy {
  def apply(proxyConfig: (String, Int)) {
    val (host, port) = proxyConfig
    for (protocol <- Seq("http", "https")) {
      System.setProperty(s"$protocol.proxyPort", port.toString)
      System.setProperty(s"$protocol.proxyHost", host)
    }
  }
}