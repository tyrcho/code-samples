import $ivy.`com.github.julien-truffaut::monocle-macro:2.0.1`

import dsl._
import scala.language.postfixOps
import scala.language.implicitConversions
import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox
import monocle.Lens
import monocle.macros.GenLens

// sbt: libraryDependencies += "org.scala-lang" % "scala-compiler" % "2.12.8"


object dsl {
  def compile[A](string: String): A = {
    val toolbox = currentMirror.mkToolBox()
    val tree = toolbox.parse(string)
    toolbox.eval(tree).asInstanceOf[A]
  }

  case class Dimensions(l: Length = 50, L: Length = 100, h: Length = 150) {
    def <=(that: Dimensions): Boolean =
      this.l <= that.l &&
        this.L <= that.L &&
        this.h <= that.h
  }

  case class Delivery(
                       maxWeight: Mass = 30,
                       totalWeight: Mass = 30,
                       dimensions: Dimensions = Dimensions(),
                       ground: Boolean = true,
                       distance: Distance = 10
                     )


  object DeliveryMatcher {
    def apply[T](threshold: T)(lens: Lens[Delivery, T], comparison: (T, T) => Boolean): DeliveryMatcher[T] =
      DeliveryMatcher(matches = delivery => comparison(lens.get(delivery), threshold),
        update = d => lens.set(threshold)(d),
        threshold = threshold)
  }

  case class DeliveryMatcher[T](
                                 matches: Delivery => Boolean,
                                 update: Delivery => Delivery,
                                 threshold: T
                               ) extends (Delivery => Boolean) {

    def apply(d: Delivery): Boolean = matches(d)

    def x[U](that: DeliveryMatcher[U]): DeliveryMatcher[(T, U)] = {
      DeliveryMatcher[(T, U)](
        matches = (d: Delivery) => this.matches(d) && that.matches(d),
        update = (d: Delivery) => this.update(that.update(d)),
        threshold = (this.threshold, that.threshold)
      )
    }
  }


  type Price = Float
  type Distance = Float

  type Mass = Int
  type Length = Int


  sealed trait Pricer {
    self =>
    def price(d: Delivery): Option[Price]

    val threshold: Delivery => Delivery = identity

    def withExtra(f: Delivery => Price): Pricer = new Pricer {
      def price(d: Delivery): Option[Price] =
        self.price(d).map(p => f(d) + p)

      override val threshold: Delivery => Delivery = self.threshold
    }
  }

  case class FinalPricer(f: PartialFunction[Delivery, Price], override val threshold: Delivery => Delivery) extends Pricer {
    override def price(d: Delivery): Option[Price] = f.lift(d)
  }

  case class CombinedPricer(f: PartialFunction[Delivery, Pricer], override val threshold: Delivery => Delivery) extends Pricer {
    override def price(d: Delivery): Option[Price] =
      f.lift(d).flatMap(_.price(d))
  }


  def ground = DeliveryMatcher[Boolean](false)(GenLens[Delivery](_.ground), _ != _)

  def totalWeight(kg: Mass) = DeliveryMatcher[Mass](kg)(GenLens[Delivery](_.totalWeight), _ <= _)

  def maxWeight(kg: Mass) = DeliveryMatcher[Mass](kg)(GenLens[Delivery](_.maxWeight), _ <= _)

  def dimensions(l: Int, L: Int, h: Int) =
    DeliveryMatcher[Dimensions](Dimensions(l, L, h))(GenLens[Delivery](_.dimensions), _ <= _)


  val smallLight = totalWeight(5) x dimensions(30, 30, 30)

  implicit class MatcherPricerBuilder(matcher: DeliveryMatcher[_]) {
    def ==>(price: Price): FinalPricer = FinalPricer({
      case d if matcher(d) => price
    }, matcher.update)

    def ==>(pricer: Pricer): CombinedPricer = CombinedPricer({
      case d if matcher(d) => pricer
    }, d => matcher.update(pricer.threshold(d)))
  }

  case class SequencePricer(p1: Pricer, p2: Pricer) extends Pricer {
    override def price(d: Delivery): Option[Price] =
      p1.price(d).orElse(p2.price(d))
  }

  implicit class SequencePricerBuilder(pricer: Pricer) {
    def |(that: Pricer): SequencePricer = SequencePricer(pricer, that)

    def | : Pricer = pricer
  }

  implicit def fixPrice(p: Price): FinalPricer = FinalPricer({ case _ => p }, identity)

}


val simplePrice: Pricer = (totalWeight(5) x dimensions(30, 30, 30)) ==> 9

println(simplePrice.price(Delivery(3, 3, Dimensions(20, 20, 20))))

val simplePrice2: Pricer = totalWeight(5) ==> {
  dimensions(30, 30, 30) ==> 9
}

println(simplePrice2.price(Delivery(3, 3, Dimensions(20, 20, 20))))

val cplxPrice: Pricer = totalWeight(5) ==> {
  dimensions(30, 30, 30) ==> 8.99F |
    dimensions(130, 230, 330) ==> 99
}

println(cplxPrice.price(Delivery(3, 3, Dimensions(20, 20, 20))))
println(cplxPrice.price(Delivery(3, 3, Dimensions(20, 20, 220))))

println((dimensions(35, 35, 35) x totalWeight(5) x maxWeight(5) x ground).update(Delivery()))

val full: Pricer =
  ((dimensions(35, 35, 35) x totalWeight(5) x maxWeight(5)) ==> 8.99F |
    (totalWeight(100) x maxWeight(30)) ==> {
      dimensions(80, 100, 150) ==> {
        ground ==> 20 | 25
      } |
        dimensions(100, 150, 240) ==> {
          ground ==> 23 | 29
        }
    } | {
    maxWeight(70) ==> {
      (totalWeight(150) x dimensions(100, 150, 240)) ==> {
        ground ==> 45 | 75
      } |
        (totalWeight(350) x dimensions(100, 150, 320)) ==> {
          ground ==> 65 | 115
        } |
        (totalWeight(650) x dimensions(100, 150, 400)) ==> {
          ground ==> 90 | 145
        } |
        (totalWeight(650) x dimensions(50, 200, 240)) ==> {
          ground ==> 90 | 145
        } |
        (totalWeight(1200) x dimensions(100, 150, 320)) ==> {
          ground ==> 115 | 220
        }
    }
  } | dimensions(80, 100, 150) ==> {
    (maxWeight(100) x totalWeight(100)) ==> {
      ground ==> 65 | 119
    } |
      (maxWeight(150) x totalWeight(150)) ==> {
        ground ==> 135 | 179
      } |
      (maxWeight(250) x totalWeight(250)) ==> {
        ground ==> 195 | 239
      } |
  }).withExtra(d => if (d.distance < 30) 0 else (d.distance - 30) * 0.45F)
    .withExtra(d => if (d.maxWeight < 70) 0 else 40)

println(full.threshold(Delivery()))

println(
  full.price(
    Delivery(maxWeight = 4,
      totalWeight = 4,
      dimensions = Dimensions(l = 30, L = 30, h = 30),
      distance = 88)))
val std = Delivery(maxWeight = 30,
  totalWeight = 100,
  dimensions = Dimensions(l = 80, L = 100, h = 150))
println(full.price(std))
println(full.price(std.copy(ground = false)))
println(full.price(std.copy(totalWeight = 645)))
println(full.price(std.copy(dimensions = Dimensions(50, 100, 240))))
val pab = Delivery(maxWeight = 150,
  totalWeight = 150,
  dimensions = Dimensions(70, 90, 150),
  ground = false,
  distance = 45)
println(full.price(pab))
