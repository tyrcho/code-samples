import $ivy.`ru.tinkoff::phobos-core:0.13.0`
import ru.tinkoff.phobos.decoding._
import ru.tinkoff.phobos.encoding._
import ru.tinkoff.phobos.syntax._
import ru.tinkoff.phobos.derivation.semiauto._


case class TravelPoint(country: String, city: String)
object TravelPoint {
  implicit val encoder: ElementEncoder[TravelPoint] = deriveElementEncoder
  implicit val decoder: ElementDecoder[TravelPoint] = deriveElementDecoder
}

case class Price(currency: String, value: Double)
object Price {
  implicit val encoder: ElementEncoder[Price] = deriveElementEncoder
  implicit val decoder: ElementDecoder[Price] = deriveElementDecoder
}


case class Journey(price: Price, departure: TravelPoint, arrival: TravelPoint)
object Journey {
  implicit val encoder: XmlEncoder[Journey] = deriveXmlEncoder("journey")
  implicit val decoder: XmlDecoder[Journey] = deriveXmlDecoder("journey")
}

val journey =
  Journey(
    price = Price("EUR", 1000.0),
    departure = TravelPoint("France", "Marcelle"),
    arrival = TravelPoint("Germany", "Munich")
  )

val xml: String = XmlEncoder[Journey].encode(journey)
println(xml)

val decodedJourney = XmlDecoder[Journey].decode(xml)
println(decodedJourney)
