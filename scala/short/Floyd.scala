import mainargs.main

import scala.util.Random


//https://www.cs.rochester.edu/u/nelson/courses/csc_173/graphs/apsp.html
def floydAPSP(n: Int, costs: Array[Array[Int]], distances: Array[Array[Int]], paths: Array[Array[Int]]) = {
  for (i <- 0 until n) {
    for (j <- 0 until n) {
      distances(i)(j) = costs(i)(j)
      paths(i)(j) = -1
    }
    distances(i)(i) = 0
  }
  for (k <- 0 until n) {
    for (i <- 0 until n) {
      // for (j <- 0 until n) is nicer, but 10 times slower here
      var j = 0
      while (j < n) {
        if (distances(i)(k) + distances(k)(j) < distances(i)(j)) {
          distances(i)(j) = distances(i)(k) + distances(k)(j)
          paths(i)(j) = k
        }
        j += 1
      }
    }
  }
}

@main
def run(n: Int = 1000) {
  val costs = Array.fill(n, n)(Random.nextInt(1000))
  val distances = Array.fill(n, n)(-1)
  val paths = Array.fill(n, n)(-1)

  println("initialized")
  val start = System.nanoTime
  floydAPSP(n, costs, distances, paths)
  val end = System.nanoTime
  println(s"done in ${(end - start) / 1_000_000}ms")
}
