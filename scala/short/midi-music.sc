
import java.util.{Timer, TimerTask}

import Note._
import javax.sound.midi._


val DOM = DO.accordParfaitMajeur
val DOm = DO.accordParfaitMineur

val noires = Rythm(Seq(1))
val croches = Rythm(Seq(0.5F))
val douze = Rythm(Seq(2, 1, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F, 0.5F), Map(0 -> 1.5F, 4 -> 1.3F))
val huit = Rythm(Seq(1, 1, 1, 1, 0.5F, 0.5F, 0.5F, 0.5F), Map(0 -> 2F, 4 -> 1.5F))
val sept = Rythm(Seq(1, 1, 1, 1, 0.5F, 0.5F, 1))
val triolet = Rythm(Seq(1, 1, 1, 1F / 3, 1F / 3, 1F / 3, 1, 1))


//Instrument("piano", 500).play(douze(doubleQuintes(DO, 7, melodie246) ++ cadenceParfaite(DO)).flatten)

Instrument("piano", 1000).play(douze(simpleQuintes(DO, 13, melodie53) ++ cadenceParfaite(DO)).flatten)
Thread.sleep(2000000)


def cadenceParfaite(tonique: Note): Seq[Seq[Note]] =
  Seq(1, 7, 7, 1, 1, 7, 1, 5, 5, 5, 5, 1).map(tonique.accordMajeur)


def melodie53(tonique: Note): Seq[Seq[Note]] =
  Seq(1, 7, 7, 1, 1, 7, 1, 5, 5, 3, 5, 3).map(tonique.accordMajeur) //.map(_.arpege())

def melodie246(tonique: Note): Seq[Seq[Note]] =
  Seq(1, 7, 7, 1, 1, 2, 4, 6, 2, 4, 6, 6).map(tonique.accordMajeur).map(_.arpege())

def doubleQuintes(base: Note, repeat: Int, melodie: Note => Seq[Seq[Note]]): Seq[Seq[Note]] = {
  def generate(note: Note, r: Int): Seq[Seq[Note]] =
    if (r == repeat) Nil
    else melodie(note) ++ generate(
      if (r % 6 == 5) note - 10 else note + 2, r + 1)

  generate(base, 0)
}

def simpleQuintes(base: Note, repeat: Int, melodie: Note => Seq[Seq[Note]]): Seq[Seq[Note]] = {
  def generate(note: Note, r: Int): Seq[Seq[Note]] =
    if (r == repeat) Nil
    else melodie(note) ++ generate(
      if (r % 12 == 10) note - 5
      else if (r % 2 == 0) note + 7
      else note - 5,
      r + 1)

  generate(base, 0)
}


case class Rythm(delays: Seq[Float], tempsForts: Map[Int, Float] = Map.empty) {
  def delay(i: Int): Float = if (i == -1) 0 else delays(i % delays.length) + delay(i - 1)

  def apply(accords: Seq[Seq[Note]]): Seq[Seq[Note]] =
    for {
      (accord, i) <- accords.zipWithIndex
      temps = i % delays.length
    } yield accord.map(note => note
      .addDelay(delay(i - 1))
      .copy(duration = delays(temps))
      .copy(intensity = tempsForts.getOrElse(temps, 1F) * note.intensity))
}

case class Note(value: Int, delay: Float = 0, duration: Float = 2, intensity: Float = 50) {
  def +(i: Int): Note = copy(value = value + i)

  def -(i: Int): Note = copy(value = value - i)

  def addDelay(d: Float): Note = copy(delay = delay + d)

  lazy val accordParfaitMajeur: Seq[Note] = Seq(this, this + tierceMajeure, this + quinteMajeure)

  lazy val accordParfaitMineur: Seq[Note] = Seq(this, this + tierceMineure, this + quinteMajeure)

  lazy val gammeMajeure: Seq[Note] = Seq(0, 2, 4, 5, 7, 9, 11, 12).map(this.+)

  def accordMajeur(i: Int): Seq[Note] = Seq(i - 1, (i + 1) % 7, (i + 3) % 7).map(gammeMajeure).sortBy(_.value)

  def gammeMineure: Seq[Note] = Seq(0, 2, 3, 5, 7, 9, 11, 12).map(this.+)

  def quinte: Note = this + Note.quinteMajeure

}

object Note {
  val DO = Note(52)
  val tierceMajeure = 3
  val tierceMineure = 2
  val quinteMajeure = 7
  val quarte = 5
  val octave = 12
}


case class Instrument(name: String, speed: Int = 180) {
  val timer = new Timer

  def play(notes: Seq[Note]): Unit = {
    for {
      n <- notes
      v = n.value
    } {
      in(n.delay * speed, {
        if (n.intensity > 20) println(v)
        channel.noteOn(v, n.intensity.toInt)
      })
      in((n.duration + n.delay) * speed, channel.noteOff(v))
    }
  }


  def in(delay: Float, action: => Unit): Unit =
    timer.schedule(new TimerTask {
      override def run(): Unit = action
    }, delay.toLong)

  val channel: MidiChannel = getChannel

  private def getChannel: MidiChannel = {
    val synth = MidiSystem.getSynthesizer
    synth.open()
    val channel = synth.getChannels.head
    val instruments = synth.getDefaultSoundbank.getInstruments
    val instrument = instruments.find(_.getName.toLowerCase.contains(name.toLowerCase)).getOrElse(instruments.head)
    synth.loadInstrument(instrument)
    println(s"using $instrument")
    channel.programChange(instrument.getPatch.getProgram)
    Thread.sleep(500)
    channel
  }
}

implicit class AccordOps(accord: Seq[Note]) {
  def arpege(delay: Float = 0.05F): Seq[Note] =
    accord.zipWithIndex
      .map { case (note, i) => note.copy(delay = note.delay + i * delay) }
}
