#!/usr/bin/env amm
!#

import $ivy.`com.lihaoyi::mainargs:0.2.3`

import scala.annotation.tailrec
import scala.util.Random
import mainargs.main

val `1d6` = TiamatRoll(0, 1)
val `2d6` = TiamatRoll(0, 2)

val `1d10` = TiamatRoll(1)
val `2d10` = TiamatRoll(2)
val `3d10` = TiamatRoll(3)
val `4d10` = TiamatRoll(4)
val debug=false

@main
def stats(): Unit =
  for {
    d6s <- 0 to 2
    d10s <- 1 to 4
  } {
    val roller = TiamatRoll(d10s, d6s)
    val rolls = Seq.fill(10_000)(roller.roll())
    val average = rolls.sum / 10_000F
    val max = rolls.max
    println(s"${d10s}D10 ${d6s}D6 average=$average max=$max")
  }


@main
def fight(repeats:Int=100): Unit = {
  val pc = TiamatCharacter("PC", precision = `4d10`+4)
  val npc = TiamatCharacter("NPC", precision = `4d10`+4)
  val npcWins = Seq.fill(repeats)(Judge.runToKO(pc,npc)._1).count(_.ko)

  println(s"NPC wins $npcWins / $repeats")
}

case class TiamatCharacter(
                            name:String,
                            precision: TiamatRoll = `2d10`,
                            damage: TiamatRoll = `2d10`,
                            speed: TiamatRoll = `2d10`,
                            endurance: Int = 2,
                            hitPoints: Int = 25,
                            actions: Int = 2,
                            defense: TiamatRoll = TiamatRoll(1)
                          ) {
  def damaged(d: Int): TiamatCharacter = {
    val hpLoss = (d - endurance) max 0
    copy(hitPoints = hitPoints - hpLoss)
  }

  def ko: Boolean = hitPoints <= 0

}

object Judge {
  def runToKO(pc: TiamatCharacter, npc: TiamatCharacter): (TiamatCharacter, TiamatCharacter) = {
    if(pc.ko || npc.ko) (pc, npc)
    else {
      val (pcDamaged,npcDamaged)=runOneRound(pc,npc)
      runToKO(pcDamaged,npcDamaged)
    }
  }

  def runOneRound(pc: TiamatCharacter, npc: TiamatCharacter): (TiamatCharacter, TiamatCharacter) = {
    val (pcInit, npcInit) = (pc.speed.roll(), npc.speed.roll())
    if(debug) println(pc)
    if(debug) println(npc)
    if(debug) println(s"PC init $pcInit VS NPC init $npcInit")
    if (npcInit > pcInit) {
      val pcDamaged = attackAll(npc, pc)
      val npcDamaged = attackAll(pcDamaged, npc)
      (pcDamaged, npcDamaged)
    } else { // PC wins ties against NPC
      val npcDamaged = attackAll(pc, npc)
      val pcDamaged = attackAll(npcDamaged, pc)
      (pcDamaged, npcDamaged)
    }
  }


  @tailrec
  def attackAll(attacker: TiamatCharacter, defender: TiamatCharacter, attacksAlreadyDone: Int = 0): TiamatCharacter = {
    if (!attacker.ko && attacksAlreadyDone < attacker.actions) {
      val a = attacker.precision.roll()
      val d = defender.defense.roll()
      if(debug) println(s"action ${attacksAlreadyDone+1} ${attacker.name} attack $a to ${defender.name} defense $d")
      if (a >= d) {
        val dmg = attacker.damage.roll()
        if(debug) println(s"damage roll $dmg")
        attackAll(attacker, defender.damaged(dmg), attacksAlreadyDone + 1)
      }
      else  attackAll(attacker, defender, attacksAlreadyDone + 1)
    }
    else defender
  }
}

case class TiamatRoll(d10s: Int = 1, d6s: Int = 0, bonus: Int = 0) {
  def +(that:TiamatRoll): TiamatRoll =
    TiamatRoll(this.d10s+that.d10s, this.d6s+that.d6s, this.bonus+that.bonus)

  def +(b:Int): TiamatRoll =copy(bonus=bonus+b)

  def roll(): Int = {
    var tens = 0
    var rolledD10 = 0
    var bestD10 = 1
    while (rolledD10 < d10s) {
      val roll = rollD10
      if (roll > bestD10) bestD10 = roll
      if (roll == 10) tens += 1
      rolledD10 += 1
    }
    val d6Total = Seq.fill(tens + d6s)(rollD6).sum
    bestD10 + d6Total + bonus
  }
}


val seed: Long = Random.nextLong()
val random = new Random(seed)

def rollD10: Int = random.nextInt(10) + 1

def rollD6: Int = random.nextInt(6) + 1
