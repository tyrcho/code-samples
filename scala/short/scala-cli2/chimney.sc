#!/usr/bin/env -S scala-cli shebang
!#

//> using scala "2.13"
//> using dep "io.scalaland::chimney:0.7.0"

import io.scalaland.chimney.dsl._


case class Foo(x: String, y: Int)
case class Bar(x: String, y: Int, z: Boolean = true)

println(("abc", 10).transformInto[Tuple2[String, Int]])
println(Foo("abc", 10).into[Bar].enableDefaultValues.transform)
