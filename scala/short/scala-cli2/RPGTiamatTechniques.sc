#!/usr/bin/env -S scala-cli shebang
!#


//> using scala "2.13"
//> using dep "com.nrinaudo::kantan.csv:0.7.0"
//> using dep "com.nrinaudo::kantan.csv-generic:0.7.0"
//> using dep "net.sourceforge.plantuml:plantuml:1.2023.10"
//> using dep "com.lihaoyi::os-lib:0.9.1"

import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._


val csvFile = new java.io.File(args(0))

val reader = csvFile.asCsvReader[Technique](rfc.withHeader)
val techniques = reader.toList.flatMap {
  case Right(v) => Some(v.copy(name = v.name.trim))
  case Left(e) => println(e); None
}


val artNames = techniques.map(_.art).filterNot(_.contains(",")).distinct
val arts = for {
  name <- artNames
} yield Art(name, techniques.filter(_.art.contains(name)))


for (art <- arts) {
  val mdFile = os.root / "tmp" / s"${art.name}.md"
  println(mdFile)
  val mdContent =
    s"""
       |@startuml
       |hide empty members
       |skinparam class {
       |  FontColor automatic
       |  HeaderBackgroundColor<<Pieds>> Orange
       |  HeaderBackgroundColor<<Poings>> OrangeRed
       |  HeaderBackgroundColor<<Prise>> Magenta
       |  HeaderBackgroundColor<<Esquive>> LightBlue
       |  HeaderBackgroundColor<<Parade>> LightSkyBlue
       |  HeaderBackgroundColor<<Contre-attaque>> Blue
       |  HeaderBackgroundColor<<Enchaînement>> Gold
       |  HeaderBackgroundColor<<Attaque à distance>> Tomato
       |  HeaderBackgroundColor<<Arme de contact, Attaque à distance>> Tomato
       |  HeaderBackgroundColor<<Arme de contact>> Tomato
       |}
       |title ${art.name}
       |${art.techniques.map(_.uml).mkString("\n")}
       |${art.freeUml}
    @enduml
    """.stripMargin
  os.write.over(mdFile, mdContent)
}


net.sourceforge.plantuml.Run.main(Array("-headless", "/tmp/*.md"))

case class Art(name: String, techniques: Seq[Technique]) {
  val names = techniques.map(_.name)

  def free =
    for {
      t <- techniques
      f0 <- t.frees.split(",")
      f = f0.takeWhile('('.!=).trim
      if names.contains(f)
    } yield (t.name, f)

  def freeUml = free.map { case (from, to) =>
    s""""$from" --> "$to""""
  }.mkString("\n")

}

case class Technique(name: String,
                     description: String,
                     weapon: String,
                     art: String,
                     attributes: String,
                     combo: String,
                     cost: String,
                     kind: String,
                     localisation: String,
                     precision: String,
                     prerequis: String,
                     puissance: String,
                     special: String,
                     triggered: String,
                     frees: String,
                     requires: String) {


  // https://crashedmind.github.io/PlantUMLHitchhikersGuide/color/color.html
  def header = kind match {
    case "Pieds" => s"""class "$name" << (_,#FF7700) $kind>> {"""
    case "Poings" => s"""class "$name" << (°,#FF7700) $kind>> {"""
    case "Prise" => s"""class "$name" << (8,#FF7700) $kind>> {"""
    case "Esquive" => s"""class "$name" << (',#FF7700) $kind>> {"""
    case "Parade" => s"""class "$name" << (#,#FF7700) $kind>> {"""
    case "Contre-attaque" => s"""class "$name" << (X,#FF7700) $kind>> {"""
    case "Enchaînement" => s"""class "$name" << (O,#FF7700) $kind>> {"""
    case "Attaque à distance" | "Arme de contact, Attaque à distance" | "Arme de contact" => s"""class "$name" << (/,#FF7700) $kind>> {"""
    case _ => throw new Exception(s"$kind unsupported for $name")
  }

  def uml: String = {
    s"""
       |$header
       |${block(description)}
       |---
       |${ifDefined("Coût", cost)}
       |${ifDefined("Arme", weapon)}
       |${ifDefined("Précision", precision)}
       |${ifDefined("Puissance", puissance)}
       |${ifDefined("Localisation", localisation, breakBefore = true)}
       |${ifDefined("Attributs", attributes, breakBefore = true)}
       |${block(special, breakBefore = true)}
       |}
       |""".stripMargin.replaceAll("\n\n", "\n")
  }


  def block(text: String, breakBefore: Boolean = false) =
    if (text.isEmpty) ""
    else (if (breakBefore) "---\n" else "") + breakLines(text)


  def note(text: String) =
    if (text.isEmpty) ""
    else {
      val noteName = "note_" + name.replaceAll("[^a-zA-Z]", "_")
      s"""
         |note "${text.replace("\n", "\\n").split("\\.").mkString(".\\n")}" as $noteName
         |$noteName .. "$name"
         |""".stripMargin
    }


  def ifDefined(label: String, property: String, breakBefore: Boolean = false) =
    if (property.isEmpty) ""
    else
      (if (breakBefore) "---\n" else "") + s"$label : <b>$property</b>"

  def breakLines(text: String, maxCharacters: Int = 40): String = {
    val words = text.replaceAll("\\.", ". ").split("\\s+").toList

    def breakIt(wordsLeft: List[String], currentLine: List[String] = Nil): List[List[String]] =
      wordsLeft match {
        case word :: tail =>
          if (currentLine.nonEmpty && currentLine.map(_.length).sum + word.length > maxCharacters || word.endsWith("."))
            (word :: currentLine) :: breakIt(tail, Nil)
          else breakIt(tail, word :: currentLine)
        case Nil => List(currentLine)
      }

    breakIt(words).map(_.reverse.mkString(" ")).mkString("\n")
  }
}