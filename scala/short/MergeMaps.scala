object Maps {
  implicit class MergableMap[K](map: Map[K, _]) {

    /**
     * Override values from this with values from that.
     */
    def deepMerge(that: Map[K, _]): Map[K, _] =
      (for (k <- map.keys ++ that.keys) yield {
        val newValue =
          (map.get(k), that.get(k)) match {
            case (Some(v), None) => v
            case (None, Some(v)) => v
            case (Some(v1), Some(v2)) =>
              if (v1.isInstanceOf[Map[K, _]] && v2.isInstanceOf[Map[K, _]])
                v1.asInstanceOf[Map[K, _]] deepMerge v2.asInstanceOf[Map[K, _]]
              else v2
            case (_, _) => ???
          }
        k -> newValue
      }).toMap
  }
}