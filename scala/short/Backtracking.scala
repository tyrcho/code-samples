object Backtracking {
  def solve[Node: Analysis](root: Node): Option[Node] =
    solve(List(root))

  def solve[Node: Analysis](candidates: List[Node]): Option[Node] =
    solveStream(candidates).headOption

  def solveStream[Node: Analysis](candidates: List[Node]): Stream[Node] = {
    val analysis = implicitly[Analysis[Node]]
    import analysis._

    candidates match {
      case candidate :: tail =>
        analyse(candidate) match {
          case Accepted            => candidate #:: solveStream(tail)
          case Rejected            => solveStream(tail)
          case Undecided(children) => solveStream(children ::: tail)
        }
      case Nil => Stream.empty
    }
  }
}

trait Analysis[Node] {
  sealed trait Result
  case object Rejected                       extends Result
  case object Accepted                       extends Result
  case class Undecided(children: List[Node]) extends Result

  def analyse(node: Node): Result
}

class DemoNQueens extends Approbation {
  case class NodeNQueens(cols: List[Int] = Nil, size: Int = 8) {
    def clashes: Boolean =
      cols.toSet.size != cols.size ||
        cols.zipWithIndex.combinations(2).exists {
          case List((r1, c1), (r2, c2)) => math.abs(r1 - r2) == math.abs(c1 - c2)
        }

    def children: List[NodeNQueens] =
      (0 until size)
        .toList
        .diff(cols)
        .map(c => copy(cols = c :: cols))

    override def toString: String =
      cols
        .map(c => "." * c + "X" + "." * (size - 1 - c))
        .mkString("\n", "\n", "\n")
  }

  implicit val analysed: Analysis[NodeNQueens] = new Analysis[NodeNQueens] {
    def analyse(node: NodeNQueens): Result =
      if (node.clashes) Rejected
      else if (node.cols.size == node.size) Accepted
      else Undecided(node.children)
  }

  it should "solve n queens" in { approver => approver.verify(Backtracking.solve(NodeNQueens(size = 15)))
  }

  it should "find several solutions for n queens" in { approver =>
    approver.verify(Backtracking.solveStream(List(NodeNQueens(size = 15))).take(3).toList)
  }
}
