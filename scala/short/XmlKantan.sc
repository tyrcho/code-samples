import $ivy.`com.nrinaudo::kantan.xpath:0.5.3`
import kantan.xpath._
import kantan.xpath.implicits._


final case class Address(code: String, name: String)

implicit val decoder: NodeDecoder[Address] = NodeDecoder.decoder(xp"Code", xp"Name")(Address.apply)

val addressString =
  """<Address>
    |  <Code>DELIVERY</Code>
    |  <Name>Centiro Solutions AB</Name>
    |  <Attention /> <CareOfName />
    |  <Address1>Vevgatan 6</Address1>
    |  <Address2 /> <Address3 /> <Address4 /> <Address5 />
    |  <PostalCode>50664</PostalCode>
    |  <City>Borås</City>
    |  <State /> <Country>SE</Country>
    |  <PrimaryPhone>+46 33 290 390</PrimaryPhone>
    |  <SecondaryPhone /> <MobilePhone /> <Fax />
    |  <Email>firstname.surname@centiro.com</Email>
    |</Address>
    |""".stripMargin

val Right(a)=addressString.evalXPath[Address](xp"Address")

println(a)
