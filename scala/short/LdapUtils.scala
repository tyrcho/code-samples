import javax.naming._
import javax.naming.directory._
import scala.collection.JavaConversions._
import javax.net.ssl.X509TrustManager
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.SSLContext
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.Hashtable
import Context._

object LdapUtils extends App {
  val serviceUser = "XXXX"
  lazy val context = new InitialDirContext(new Hashtable[String, String](Map(
    SECURITY_AUTHENTICATION -> "simple",
    SECURITY_PRINCIPAL -> s"aoLdapKey=$serviceUser,ou=people,dc=XXXX,dc=com",
    SECURITY_CREDENTIALS -> "XXXXXXX",
    INITIAL_CONTEXT_FACTORY -> "com.sun.jndi.ldap.LdapCtxFactory",
    "java.naming.ldap.factory.socket" -> "MySSLSocketFactory",
    PROVIDER_URL -> "ldaps://host:666")))

  /**
   * Searches the user and returns a map of attribute key -> value.
   */
  def searchUser(login: String) =
    search("ou=people,dc=dcdcdc,dc=com", "(uid={0})", login)
  def searchGroup(unit: String) =
    search("ou=groups,dc=dcdcdc,dc=com", "(uid={0})", unit)
  def searchSAPGroup(unit: String) =
    search("ou=sapGroups,dc=dcdcdc,dc=com", "(uid={0})", unit)
  def searchSite(site: String) =
    search("ou=sites,dc=dcdcdc,dc=com", "(uid={0})", site)
  def searchUnit(site: String) =
    search("ou=units,dc=dcdcdc,dc=com", "(uid={0})", site)
  def search(name: String, filter: String, value: String) = {
    val ctls = new SearchControls
    ctls.setSearchScope(SearchControls.ONELEVEL_SCOPE)
    val answers = context.search(name, filter, Array[AnyRef](value), ctls).toSeq
    assert(answers.size <= 1, "at most one value expected for " + value)

    if (answers.size == 1) Some(
      answers.head.getAttributes.getAll map (attribute =>
        attribute.getID -> attribute.get(0).toString) toMap)
    else None
  }

  for {
    user <-searchUser("xxxxx")
    kv <- user
  } println(kv)
  
}

// Warning : this Trust Manager accetps all certificates. This allows to avoid an exception with self-signed certificates
// See http://stackoverflow.com/questions/4615163/how-to-accept-self-signed-certificates-for-jndi-ldap-connections
object DummyTrustmanager extends X509TrustManager {
  def checkClientTrusted(x: Array[X509Certificate], s: String) {}
  def checkServerTrusted(x: Array[X509Certificate], s: String) {}
  def getAcceptedIssuers = Array.empty
}

class MySSLSocketFactory extends SSLSocketFactory {
  val ctx = SSLContext.getInstance("TLS")
  ctx.init(null, Array(DummyTrustmanager), new SecureRandom)
  val socketFactory = ctx.getSocketFactory

  def getDefaultCipherSuites = socketFactory.getDefaultCipherSuites
  def createSocket(x1: java.net.Socket, x2: String, x3: Int, x4: Boolean) = socketFactory.createSocket(x1, x2, x3, x4)
  def getSupportedCipherSuites = socketFactory.getSupportedCipherSuites
  def createSocket(x1: java.net.InetAddress, x2: Int, x3: java.net.InetAddress, x4: Int) = socketFactory.createSocket(x1, x2, x3, x4)
  def createSocket(x1: java.net.InetAddress, x2: Int) = socketFactory.createSocket(x1, x2)
  def createSocket(x1: String, x2: Int, x3: java.net.InetAddress, x4: Int) = socketFactory.createSocket(x1, x2, x3, x4)
  def createSocket(x1: String, x2: Int) = socketFactory.createSocket(x1, x2)
}

object MySSLSocketFactory {
  def getDefault = new MySSLSocketFactory
}
