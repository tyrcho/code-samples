#!/usr/bin/env amm
!#

import $ivy.`com.lihaoyi::cask:0.8.0`
import $ivy.`com.lihaoyi::mainargs:0.2.3`
import cask._
import scala.sys.process._
import mainargs._
import scala.util.Try

@main
def main(initialWait: Option[Int] = None, timeToLive: Option[Int] = None) {
  if (Try("lsof -ti tcp:8080".!).getOrElse(1) == 0) {
    val pid = "lsof -ti tcp:8080".!!
    s"kill $pid".!
    println(s"killed process $pid which was using port 8080")
    Thread.sleep(1000)
  }
  initialWait.foreach { time => println(s"waiting $time ms before opening the server"); Thread.sleep(time) }
  routes.main(Array())
  val time = timeToLive.getOrElse(Int.MaxValue)
  println(s"waiting $time ms before closing the server")
  Thread.sleep(time)
}

lazy val routes = new MainRoutes {
  @get("/") def hello(): String = "Hello !"

  override def host: String = "0.0.0.0"

  initialize()
}

