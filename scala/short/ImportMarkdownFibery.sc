// for scala 3 and ammonite

import $ivy.`com.lihaoyi::requests:0.8.0`
import $ivy.`com.lihaoyi::ujson:2.0.0`
import mainargs.main
import os._
import scala.util._
import java.util.UUID

val domain = "colisweb"
val apiUrl = s"https://$domain.fibery.io/api"
val organizationId = "d431b650-8a83-11ed-9d63-cdc144f6134b"

// reads the schema, usefull to check your token.
// amm ImportMarkdownFibery.sc check $TOKEN
@main
def check(token: String): Unit = {
  val headers = Map("Content-Type" -> "application/json", "Authorization" -> s"Token $token")

  val response = requests.post(url = s"$apiUrl/commands",
    headers = headers,
    data = """[{ "command": "fibery.schema/query" }]""")
  println(response)
}


/**
 * Find all markdown files in a folder (and recursively in subfolders).
 * Create the folder and subfolders in fibery.
 * Create the documents and attachs the needed images found in associated subfolders.
 * The names are kept from the export.
 *
 * amm ImportMarkdownFibery.sc  createDocumentsFromMarkdownFiles \
 * --token $TOKEN \
 * --folder ~/Downloads/export
 */
@main
def createDocumentsFromMarkdownFiles(
                                      token: String,
                                      folder: Path,
                                      parentFolderId: String = "578af403-9e8f-4d66-87dd-de744ae5875f"
                                    ): Unit = {
  val markdownFiles = listFiles(folder.toString)
  val subFolders = os.list(folder).filter(_.toNIO.toFile.isDirectory)
  if (markdownFiles.nonEmpty || subFolders.nonEmpty) {
    val folderId = createFiberyFolder(folder.last.dropRight(33), parentFolderId, token)
    markdownFiles.foreach { f =>
      val docId = createDocumentInFolder(f, folderId, token)
      updateDocument(f, docId, token)
    }
    subFolders.foreach(createDocumentsFromMarkdownFiles(token, _, folderId))
  }
}


/**
 * For all files in a folder, update the matching entities in Fibery.
 * It expects the folder to have the same structure as a Notion export (markdown files + folders with the same name for images)
 * For each markdown file the Fibery field with name targetFieldName will be updated.
 * To retrieve the proper Fibery entity, it searches the notion identifier (at the end of the name of the md file) in keyFieldName
 *
 * amm ImportMarkdownFibery.sc updateMarkdownContent \
 * --token $TOKEN \
 * --folder ~/Solutions\ 55c1e5be46764e458e830bd292e8b652 \
 * --keyFieldName "Notion Link" \
 * --space Produit \
 * --table Solution \
 * --targetFieldName Description
 */
@main
def updateMarkdownContent(
                           token: String,
                           folder: String,
                           keyFieldName: String = "Notion Link",
                           space: String = "Produit",
                           table: String = "Solution",
                           targetFieldName: String = "Description") =
  for {
    f <- listFiles(folder)
    docId <- getDocumentId(token = token, keyValue = f.id, targetFieldName = targetFieldName, keyFieldName = keyFieldName, space = space, table = table)
  } updateDocument(f, docId, token)


private def updateDocument(f: Entity, docId: String, token: String) = {
  val file = f.mdFileName
  val secretsForAttachments = f.attachmentsFileNames.map(uploadFile(token, _)).toMap
  val markdownContent = replaceImageLinksInMarkdown(read(file), secretsForAttachments)
  val jsonContent = ujson.Obj("content" -> markdownContent)
  val headers = Map("Content-Type" -> "application/json", "Authorization" -> s"Token $token")
  val response = requests.put(url = s"$apiUrl/documents/$docId?format=md", // https://api.fibery.io/#update-rich-text-field
    headers = headers,
    data = ujson.write(jsonContent)
  )
  println(response.statusCode)
  Thread.sleep(1000) // rate limit
}

// The links to display images in md like this: ![Untitled](image.png)
// In the notion export they target a local image file
// We need to replace these links with Fibery addresses, based on the secrets we got when uploading the images
private def replaceImageLinksInMarkdown(markdownText: String, secretsForAttachments: Map[String, String]): String = {
  val imagesRegex = """!\[([^\]]+)\]\(([^\)]+)\)""".r
  imagesRegex.replaceAllIn(markdownText, { m =>
    val name = m.group(1)
    val fileRegex = """(.*/)?(.*)""".r
    val fileRegex(_, file) = m.group(2)
    val secret = secretsForAttachments.get(file) match {
      case Some(s) => s
      case None =>
        System.err.println(s"no attachment was uploaded for $file")
        "no image found"
    }
    s"![$name]($apiUrl/files/$secret)" //  https://api.fibery.io/#download-file
  })
}

private def listFiles(folder: String): Seq[Entity] = {
  val mdFiles = os.list(Path(folder)).filter(_.ext == "md")
  mdFiles.map { mdFile =>
    val attachmentsFolder = Path(mdFile.toString.dropRight(3))

    val attachments = if (attachmentsFolder.toNIO.toFile.exists)
      os.list(attachmentsFolder).filter(_.ext != "md").filter(_.toNIO.toFile.isFile)
    else Nil

    Entity(mdFile, attachments)
  }
}

final case class Entity(mdFileName: Path, attachmentsFileNames: Seq[Path]) {
  def id: String = mdFileName.baseName.takeRight(30) // Notion Id

  def documentName: String = mdFileName.baseName.dropRight(33)
}

// Send an image file to Fibery and returns a secret which can be used to download it or refer in markdown
private def uploadFile(token: String, file: Path) = {
  val headers = Map("Authorization" -> s"Token $token")

  val response = requests.post(url = s"$apiUrl/files", // https://api.fibery.io/#upload-file
    headers = headers,
    data = requests.MultiPart(requests.MultiItem("file", file.toNIO, file.toString))
  )
  val respJson = ujson.read(response.text)
  val secret = respJson("fibery/secret").str
  val id = respJson("fibery/id").str
  println(s"uploaded file ${file.last} => id=$id secret=$secret")
  Thread.sleep(1000)
  file.last.replaceAll(" ", "%20") -> secret
}

// Use Fibery API to retrieve the id of the document associated with a field in an entity
// Typically we will lookup based on the NotionId field in Fibery
// Returns the secret pointer to the document (the rich text field we want to update)
private def getDocumentId(
                           token: String,
                           keyValue: String,
                           targetFieldName: String,
                           keyFieldName: String = "Name",
                           space: String = "Produit",
                           table: String = "Solution"
                         ): Option[String] = {
  val headers = Map("Content-Type" -> "application/json", "Authorization" -> s"Token $token")

  val response = requests.post(url = s"$apiUrl/commands", // https://api.fibery.io/#filter-entities
    headers = headers,
    data =
      s"""[
         |         {
         |           "command": "fibery.entity/query",
         |           "args": {
         |             "query": {
         |               "q/from": "$space/$table",
         |               "q/select": [
         |                 "$space/Name",
         |                 { "$space/$targetFieldName": [ "Collaboration~Documents/secret" ] }
         |               ],
         |               "q/where": ["q/contains", ["$space/$keyFieldName"], "$$name" ],
         |               "q/limit": 1
         |             },
         |              "params": {
         |              "$$name": "$keyValue"
         |            }
         |           }
         |         }
         |      ]""".stripMargin)
  val data = ujson.read(response.text)

  val element = Try(data.arr.head("result").arr.head) match {
    case Failure(e) => System.err.println(s"Could not find document for $keyValue - $e")
      None
    case Success(e) =>
      println(e(s"$space/Name").strOpt.getOrElse("Untitled") + s" found for $keyValue")
      Some(e)
  }
  // see https://api.fibery.io/#update-rich-text-field
  element.map(_(s"$space/$targetFieldName")("Collaboration~Documents/secret").str)
}


private def createFiberyFolder(folderName: String, parentFolderId: String, token: String): String = {
  val jsonContent = ujson.Obj(
    "jsonrpc" -> "2.0",
    "method" -> "create-folders",
    "params" -> ujson.Obj(
      "values" -> ujson.Arr(
        ujson.Obj(
          "fibery/name" -> folderName,
          "fibery/Parent Folder" -> ujson.Obj("fibery/id" -> parentFolderId),
          "fibery/container-app" -> ujson.Obj("fibery/id" -> organizationId),
        )
      )
    )
  )
  val headers = Map("Content-Type" -> "application/json", "Authorization" -> s"Token $token")
  val response = requests.post(url = s"$apiUrl/views/json-rpc",
    headers = headers,
    data = ujson.write(jsonContent))
  val respJson = ujson.read(response.text)
  println(respJson)
  respJson("result").arr.head("fibery/id").str
}

// Create the document, choose a random UUID for the secret and returns this secretId
private def createDocumentInFolder(file: Entity, parentFolderId: String, token: String): String = {
  val documentName = file.documentName
  val createDocumentJson = ujson.Obj(
    "jsonrpc" -> "2.0",
    "method" -> "create-views",
    "params" -> ujson.Obj(
      "views" -> ujson.Arr(
        ujson.Obj(
          "fibery/type" -> "document",
          "fibery/name" -> documentName,
          "fibery/Folder" -> ujson.Obj("fibery/id" -> parentFolderId),
          "fibery/container-app" -> ujson.Obj("fibery/id" -> organizationId),
        )
      )
    )
  )
  val headers = Map("Content-Type" -> "application/json", "Authorization" -> s"Token $token")
  val response = requests.post(url = s"$apiUrl/views/json-rpc",
    headers = headers,
    data = ujson.write(createDocumentJson))
  val respJson = ujson.read(response.text)

  val documentId = respJson("result").arr.head("fibery/id").str
  val secretId = UUID.randomUUID().toString // strangely the secret is decided by the client, not by fibery backend
  val updateSecretJson = ujson.Obj(
    "jsonrpc" -> "2.0",
    "method" -> "update-views",
    "params" -> ujson.Obj(
      "updates" -> ujson.Arr(
        ujson.Obj(
          "id" -> documentId,
          "values" -> ujson.Obj(
            "fibery/meta" -> ujson.Obj(
              "documentSecret" -> secretId
            )
          )
        )
      )
    )
  )

  requests.post(url = s"$apiUrl/views/json-rpc", headers = headers, data = ujson.write(updateSecretJson))

  secretId
}