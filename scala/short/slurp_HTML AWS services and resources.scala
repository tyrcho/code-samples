import $ivy.`org.ccil.cowan.tagsoup:tagsoup:1.2`
// import $ivy.`org.scala-lang.modules::scala-parallel-collections:1.2.0`

import java.net.URL
import scala.xml.XML
import org.xml.sax.InputSource
import scala.xml.parsing.NoBindingFactoryAdapter
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl
import java.net.HttpURLConnection
import scala.xml.Node
// import scala.collection.parallel.CollectionConverters._

// amm slurp_HTML\ AWS\ services\ and\ resources.scala > /tmp/resources.json
// output as csv :
// cat /tmp/resources.json | jq -r '.[] | . as $service | .resources.[] | [$service.service, $service.docUrl, .resourceType, .arn] | @csv'

object HTML {
    lazy val adapter = new NoBindingFactoryAdapter
    lazy val parser = (new SAXFactoryImpl).newSAXParser

    def load(url: URL, headers: Map[String, String] = Map.empty): Node = {
        val conn = url.openConnection().asInstanceOf[HttpURLConnection]
        for ((k, v) <- headers)
            conn.setRequestProperty(k, v)
        val source = new InputSource(conn.getInputStream)
        adapter.loadXML(source, parser)
    }
}


val site = new URL("https://docs.aws.amazon.com/service-authorization/latest/reference/reference_policies_actions-resources-contextkeys.html")

val content = HTML.load(site)

val resourcePages = for {
    a <- content \\ "a"
    href <- a.attribute("href")
    target = href.toString
    if target.startsWith("./list")
} yield target

// seems that running this in concurrent threads gets denied by the server
val json = resourcePages.flatMap(loadList).mkString("[", ",\n", "]")
println(json)

def loadList(name: String): Option[String] = {
    System.err.println(name)
    val builder = new StringBuilder()
    val docPage = s"https://docs.aws.amazon.com/service-authorization/latest/reference/$name"
    val page = new URL(docPage)
    val content = HTML.load(page)
    (content \\ "code").headOption map { code =>
        val servicePrefix = code.text
        builder.append(s""" {"service":"$servicePrefix", "docUrl":"$docPage", "resources":[ """)

        val resourceTable = for {
            table <- content \\ "table"
            th <- table \\ "th"
            header = th.text
            if header == "Resource types"
        } yield table

        val trs = resourceTable \\ "tr"

        val types = for {
            tr <- trs
            tds = tr \\ "td"
        } yield tds match {
            case Seq(a, b, _) =>
                val r = (a \\ "a").text
                val arn = (b \\ "code").text
                Some(s"""  { "resourceType": "$r", "arn": "$arn" }""")
            case _ => None
        }
        builder.append(types.flatten.mkString(",\n"))
        builder.append("]}")
        builder.toString
    }
}

