import cask._
import ujson._

object Main extends MainRoutes {
  @get("/")
  def hello(): String =
    "Hello World!"

  @post("/reverse")
  def reverse(request: Request): String =
    new String(request.readAllBytes()).reverse + "aaa "


  // Simple Json (manual deserialization)
  @postJson("/json")
  def jsonEndpointObj(value1: Value, value2: Seq[Int]): Obj = {
    Obj(
      "v1" -> value1,
      "v2" -> value2.reverse
    )
  }

  // Map json to case class with upickle
  @postJson("/api/person/:name")
  def jsonEndpointObj(name: String, person: Person): Person =
    person.copy(age = person.age + 18, name = name)

  initialize()
}

final case class Person(name: String, age: Int)

object Person {

  import upickle.default._

  implicit val rw: ReadWriter[Person] = macroRW
}


