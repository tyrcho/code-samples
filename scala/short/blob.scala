import scala.io.StdIn._
import Direction._

object Player extends App {
    val Array(width, height) = (readLine split " ").filter(_ != "").map(_.toInt)
    while (true) {
        val entityCount = readLine.toInt
        val organs = for (i <- 0 until entityCount) yield {
            val Array(_x, _y, _type, _owner, _organId, organDir, _organParentId, _organRootId) = readLine split " "
            val x = _x.toInt
            val y = _y.toInt
            val owner = _owner.toInt
            val organId = _organId.toInt
            val organParentId = _organParentId.toInt
            val organRootId = _organRootId.toInt
            Organ(Pos(row = y, col = x), _type, organId, owner, organDir.head)
        }
        val Array(myA, myB, myC, myD) = (readLine split " ").filter(_ != "").map(_.toInt)
        val Array(opA, opB, opC, opD) = (readLine split " ").filter(_ != "").map(_.toInt)

        val game = Game(width, height, organs, Map('A' -> myA, 'B' -> myB, 'C' -> myC, 'D' -> myD), Map('A' -> opA, 'B' -> opB, 'C' -> opC, 'D' -> opD))
        val requiredActionsCount = readLine.toInt
        for (i <- 0 until requiredActionsCount) {
            if (i == 0) println(game.move.get)
            else println(Wait)
        }
    }
}

case class Game(width: Int,
                height: Int,
                organs: Seq[Organ],
                myStock: Map[Char, Int],
                opStock: Map[Char, Int]) {

    val myOrgans = organs.filter(_.owner == 1)
    val opOrgans = organs.filter(_.owner == 0)
    val protNames = "ABCD".toSet
    val proteins = organs.filter(o => o.kind.length == 1 && protNames(o.kind.head))
    val aProteins = proteins.filter(_.kind == "A")

    val mySporers = myOrgans.filter(_.kind == "SPORER")
    val myRoots = myOrgans.filter(_.kind == "ROOT")
    val myHarvesters = myOrgans.filter(_.kind == "HARVESTER")
    val walls = organs.filter(_.kind == "WALL")

    val myHarvestTargets = myHarvesters.map(o => o.pos + withName(o.direction).pos).toSet
    Console.err.println(s"myHarvestTargets $myHarvestTargets")

    val myIncome: Map[Char, Int] = (for {
        t <- myHarvestTargets
        p <- at(t)
        if proteins.contains(p)
    } yield p.kind.head -> 1).foldLeft(protNames.map(_ -> 0).toMap) {
        case (map, (k, v)) => map.updated(k, map(k) + v)
    }

    Console.err.println(s"myIncome $myIncome")

    def sumMaps[K](m1: Map[K, Int], m2: Map[K, Int]): Map[K, Int] =
        (m1.keySet ++ m2.keySet).map(k => k -> (m1.getOrElse(k, 0) + m2.getOrElse(k, 0))).toMap

    val neededProtNames = sumMaps(myIncome, sumMaps(myIncome, myStock)).toSeq.sortBy(_._2).map(_._1).take(1)
    val neededProteins = organs.filter(o => neededProtNames.contains(o.kind))

    Console.err.println(s"needed $neededProtNames $neededProteins")

    def isConstructible(pos: Pos) = at(pos).fold(true)(o => o.kind.length == 1)

    def store(prot: Char) = myStock.getOrElse(prot, 0)

    def has(prot: Char) = store(prot) > 0

    val myEnveloppe = enveloppe(myOrgans.map(_.pos)).toSet
    val opEnveloppe = enveloppe(opOrgans.map(_.pos)).toSet

    def at(p: Pos) = organs.find(_.pos == p)

    def myNear(p: Pos) = neighbors(p).flatMap(at).collectFirst {
        case o if o.owner == 1 => o
    }.get

    val goodSpawnSpots = myEnveloppe diff myHarvestTargets

    def harvestFrom(prots: Iterable[Organ]): Option[Order] =
        if (has('C') && has('D')) {
            for {
                t <- goodSpawnSpots
                a <- prots.map(_.pos).toSet diff myHarvestTargets
                d = t dist a
                if d == 1
            } yield Grow(myNear(t).id, t, Harvester, fromAtoB(t, a))
        }.headOption
        else None


    val tentacle: Option[Order] =
        if (allStoresBiggerThan(2)) {
            for {
                m <- myEnveloppe
                o <- opOrgans
                d = m dist o.pos
                if d == 1
                if freeAndValid(m)
            } yield Grow(myNear(m).id, m, Tentacle, fromAtoB(m, o.pos))
            )
        }.minByOption(_._1).map(_._2)
        else None

    val requiredStockToSpore = 6

    def allStoresBiggerThan(required: Int) =
        store('A') > required &&
            store('B') > required &&
            store('C') > required &&
            store('D') > required

    val sporeRoot: Option[Order] =
        if (allStoresBiggerThan(requiredStockToSpore - 1)) {
            for {
                s <- mySporers
                line = sameLine(s.pos, withName(s.direction)).filterNot(myHarvestTargets).filterNot(myEnveloppe)
                t <- line.lastOption
                if !enveloppe(myRoots.map(_.pos))(line.last)
                d = s.pos dist t
            } yield (d, Spore(s.id, t))
        }.maxByOption(_._1).map(_._2)
        else None

    val sporer: Option[Order] =
        if (allStoresBiggerThan(requiredStockToSpore)) {
            for {
                t <- goodSpawnSpots
                d <- allDirections
                line = sameLine(t, d)
                if line.size > 3 && !enveloppe(myRoots.map(_.pos))(line.last)
            } yield Grow(myNear(t).id, t, Sporer, d)
        }.headOption
        else None

    def basicToProts(prots: Iterable[Organ]): Option[Order] =
        if (has('A')) {
            for {
                a <- prots
                if !myHarvestTargets(a.pos)
                t <- goodSpawnSpots
                if !myHarvestTargets(t)
                o = myNear(t)
                d = o.pos dist a.pos
            } yield (d, Grow(o.id, t, Basic, E, 1, s"to $a"))
        }.minByOption(_._1).map(_._2)
        else None

    val basicAny: Option[Order] =
        if (myStock.getOrElse('A', 0) > 0) {
            for {
                t <- myEnveloppe
                if !myHarvestTargets(t)
            } yield Grow(myNear(t).id, t, Basic, E, 1, "any")
        }.headOption
        else None

    val move =
        sporeRoot orElse
            sporer orElse
            tentacle orElse
            harvestFrom(neededProteins) orElse
            basicToProts(neededProteins) orElse
            harvestFrom(proteins) orElse
            basicToProts(proteins) orElse
            basicAny orElse
            Some(Wait)

    def neighbors(p: Pos): Seq[Pos] =
        for {
            d <- allDirections
            t = d.pos + p
        } yield t

    def validNeighbors(p: Pos): Seq[Pos] =
        neighbors(p).filter(freeAndValid)

    def sameLine(from: Pos, direction: Direction): Seq[Pos] =
        Iterator.iterate(from)(direction.pos.+).drop(1).takeWhile(freeAndValid).toSeq

    def enveloppe(positions: Iterable[Pos]): Set[Pos] =
        for {
            p <- positions.toSet[Pos]
            n <- validNeighbors(p)
            if freeAndValid(n)
        } yield n

    def freeAndValid(p: Pos) =
        valid(p) && isConstructible(p)

    def valid(p: Pos) =
        p.row >= 0 && p.col >= 0 && p.row < height && p.col < width
}

case class Organ(pos: Pos, kind: String, id: Int, owner: Int, direction: Char) {
}

case class Pos(row: Int, col: Int) {
    def +(that: Pos) = Pos(this.row + that.row, this.col + that.col)

    def -(that: Pos) = Pos(this.row - that.row, this.col - that.col)

    def dist(that: Pos) =
        (this.row - that.row).abs + (this.col - that.col).abs

    def to(that: Pos) =
        if (this.row > that.row) Pos(row - 1, col)
        else if (this.row < that.row) Pos(row + 1, col)
        else if (this.col > that.col) Pos(row, col - 1)
        else Pos(row, col + 1)

}

sealed trait Order {
    def value: Int = 0
}

case object Wait extends Order {
    override val toString = "WAIT"
}

case class Grow(id: Int,
                pos: Pos,
                oType: OrganType,
                direction: Direction = N,
                override val value: Int = 0,
                comment: String = "") extends Order {
    override val toString = s"GROW $id ${pos.col} ${pos.row} $oType $direction $comment"
}

case class Spore(id: Int,
                 pos: Pos,
                 override val value: Int = 0,
                 comment: String = "") extends Order {
    override val toString = s"SPORE $id ${pos.col} ${pos.row} $comment"
}

case class Direction(name: Char, dRow: Int, dCol: Int) {
    override val toString = name.toString
    val pos = Pos(dRow, dCol)
}

object Direction {
    val N = Direction('N', -1, 0)
    val S = Direction('S', 1, 0)
    val E = Direction('E', 0, 1)
    val W = Direction('W', 0, -1)

    def withName(n: Char) = allDirections.find(_.name == n).get

    val allDirections = Seq(N, S, E, W)

    def fromAtoB(a: Pos, b: Pos): Direction = {
        val diff = b - a
        allDirections
            .find(d => d.pos.row.sign == diff.row.sign && d.pos.col.sign == diff.col.sign)
            .get
    }
}

sealed trait OrganType

case object Basic extends OrganType {
    override val toString = "BASIC"
}

case object Harvester extends OrganType {
    override val toString = "HARVESTER"
}

case object Tentacle extends OrganType {
    override val toString = "TENTACLE"
}

case object Sporer extends OrganType {
    override val toString = "SPORER"
}
