import scala.annotation.tailrec
import scala.util.Random

object VrpDemo extends App {
  val seeded      = new Random(123)
  def randomPoint = Point(seeded.nextInt(100), seeded.nextInt(100))
  val initial     = VrpSolution(Random.shuffle(Vector.fill(100)(randomPoint)))
  println(initial.quality)
  val sol = SimulatedAnnealing[VrpSolution](cool = 0.99995F.*, neighbor = _.tweak, score = _.quality)
    .improve(initial, 200000, 100)
  println(sol.path, sol.quality)
}

case class VrpSolution(path: Vector[Point]) {
  val quality: Float = {
    val segments = (path zip path.tail :+ path.head).map((Segment.apply _).tupled)
    -segments.map(_.length).sum
  }

  def tweak: VrpSolution = {
    val points   = path
    val i        = Random.nextInt(points.size)
    val j        = Random.nextInt(points.size)
    val minIndex = i min j
    val maxIndex = i max j
    if (maxIndex - minIndex <= 1) tweak
    else {
      val start = points.take(minIndex)
      val end   = points.drop(maxIndex)
      val mid   = points.slice(minIndex, maxIndex).reverse
      VrpSolution(start ++ mid ++ end)
    }
  }

}

case class Segment(from: Point, to: Point) {
  def length: Float =
    math.sqrt(sqr(from.x - to.x) + sqr(from.y - to.y)).toFloat

  def sqr(a: Int) = a * a
}

case class Point(x: Int, y: Int)

case class SimulatedAnnealing[S](cool: Float => Float, neighbor: S => S, score: S => Float) {

  def improve(candidate: S, iterations: Int, temperature: Float): S =
    improve(candidate, candidate, score(candidate), iterations, temperature)

  @tailrec
  final def improve(solution: S, best: S, bestScore: Float, iteration: Int, temperature: Float): S =
    if (iteration == 0) best
    else {
      val tweaked            = neighbor(solution)
      val tweakedScore       = score(tweaked)
      val currentScore       = score(solution)
      val qualityImprovement = tweakedScore - currentScore
      val willMove           = qualityImprovement > 0 || Random.nextFloat() < Math.exp(qualityImprovement / temperature)
      if (iteration % 1000 == 0)
        println(f"$iteration $currentScore%.0f $qualityImprovement%.0f $temperature%.3f $willMove")

      improve(
        if (willMove) tweaked else solution,
        if (tweakedScore > bestScore) tweaked else best,
        bestScore max tweakedScore,
        iteration - 1,
        cool(temperature)
      )
    }
}


// ScalaFiddle (scala.js) https://scalafiddle.io/sf/W0nUEWI/3
import scala.annotation.tailrec
import scala.util.Random


var intermediate = collection.mutable.ArrayStack.empty[Any]

val seeded      = new Random(123)
def randomPoint = Point(seeded.nextInt(700), seeded.nextInt(800))
val initial     = VrpSolution(Random.shuffle(Vector.fill(200)(randomPoint)))
println(initial.quality)
  val sol = SimulatedAnnealing[VrpSolution](cool = 0.99F.*, neighbor = _.tweak, score = _.quality)
    .improve(initial, 100000, 3500)
println(sol.path, sol.quality)

intermediate = intermediate.reverse

Fiddle.schedule(20) {
  val sol = intermediate.pop().asInstanceOf[VrpSolution]
  sol.draw()
}

case class VrpSolution(path: Vector[Point]) {
  val quality: Float = {
    val segments = (path zip path.tail :+ path.head).map((Segment.apply _).tupled)
    -segments.map(_.length).sum
  }

  def tweak: VrpSolution = {
    val points   = path
    val i        = Random.nextInt(points.size)
    val j        = Random.nextInt(points.size)
    val minIndex = i min j
    val maxIndex = i max j
    if (maxIndex - minIndex <= 1) tweak
    else {
      val start = points.take(minIndex)
      val end   = points.drop(maxIndex)
      val mid   = points.slice(minIndex, maxIndex).reverse
      VrpSolution(start ++ mid ++ end)
    }
  }
  
  def draw(): Unit = {
    import Fiddle.draw._
    clearRect(0, 0, 1000, 1000)
    beginPath()
    moveTo(path.head.x, path.head.y)
    path.tail.foreach(p => lineTo(p.x, p.y))
    closePath()
    stroke()
  }

}

case class Segment(from: Point, to: Point) {
  def length: Float =
    math.sqrt(sqr(from.x - to.x) + sqr(from.y - to.y)).toFloat

  def sqr(a: Int) = a * a
}

case class Point(x: Int, y: Int)

case class SimulatedAnnealing[S](cool: Float => Float, neighbor: S => S, score: S => Float) {

  def improve(candidate: S, iterations: Int, temperature: Float): S =
    improve(candidate, candidate, score(candidate), iterations, temperature)

  final def improve(solution: S, best: S, bestScore: Float, iteration: Int, temperature: Float): S =
    if (iteration == 0) best
    else {
      val tweaked            = neighbor(solution)
      val tweakedScore       = score(tweaked)
      val currentScore       = score(solution)
      val qualityImprovement = tweakedScore - currentScore
      val willMove           = qualityImprovement > 0 || Random.nextFloat() < Math.exp(qualityImprovement / temperature)
      if (iteration % 100 == 0) {
        println(f"$iteration $currentScore%.0f $qualityImprovement%.0f $temperature%.3f $willMove")
        intermediate += solution
      }
      improve(
        if (willMove) tweaked else solution,
        if (tweakedScore > bestScore) tweaked else best,
        bestScore max tweakedScore,
        iteration - 1,
        cool(temperature)
      )

    }

}

