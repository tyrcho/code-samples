// run from inside a k8s cluster, for instance
// amm_k8s testing demo scala/short/kubernetes.sc

import $ivy.`io.github.hagay3::skuber:2.7.3`
import skuber._, skuber.json.format._

import skuber.api.client
import akka.actor.ActorSystem
import akka.stream.scaladsl._
import skuber.json.format._
import scala.concurrent.Await
import scala.concurrent.duration._


implicit val system = ActorSystem()
implicit val dispatcher = system.dispatcher

val k8s = client.init(
  client.defaultK8sConfig.currentContext,
  client.LoggingConfig(logRequestBasic = false, logResponseBasic = false))



val fut = k8s.list[PodList]
Await.ready(fut, 30.seconds)
fut.foreach(println)
