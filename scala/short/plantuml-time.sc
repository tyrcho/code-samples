import $ivy.`com.lihaoyi::ujson:0.9.6`
import $ivy.`com.lihaoyi::os-lib:0.8.0`
import $ivy.`com.lihaoyi::requests:0.7.1`
import $ivy.`com.lihaoyi::mainargs:0.2.3`
import $ivy.`net.sourceforge.plantuml:plantuml:1.2021.9`

import ujson.Value
import mainargs.main
import java.time.{DayOfWeek, Duration, LocalTime}
import os._
// https://plantuml.com/fr/timing-diagram

val validTriggerKinds = List("final", "draft")
val defaultFileName = "Downloads/projets_tournees.json"

@main
def listFinal(fileName: String = defaultFileName) =
  list(fileName, List("final"))

@main
def listDraft(fileName: String = defaultFileName) =
  list(fileName, List("draft"))

@main
def listAll(fileName: String = defaultFileName) =
  list(fileName, validTriggerKinds)


def list(fileName: String, kinds: List[String]) = {
  val file = (os.home / RelPath(fileName)).toNIO.toString
  val filterKinds = kinds intersect validTriggerKinds
  if (filterKinds.isEmpty) {
    println(s"unknown kinds $kinds, please use one of $validTriggerKinds")
    System.exit(1)
  }
  val triggers: List[(DayOfWeek, List[Trigger])] = allTriggers(file, filterKinds)
  for {
    (day, ts) <- triggers
  } {
    val changes = aggregate(ts).sortBy(_.at)

    val mdFile = os.root / "tmp" / s"$day.md"
    println(mdFile)
    os.write.over(mdFile, formatTimingDiagram(ts, changes, details = false, scale100px = 3600))

    for {
      hour <- 0 to 23
      name = f"$day-$hour%02d"
    } {
      val mdFile = os.root / "tmp" / s"$name.md"
      println(mdFile)
      os.write.over(mdFile, formatTimingDiagram(ts.filter(_.about(hour)), changes.filter(_.at.getHour == hour))
      )
    }
  }
  net.sourceforge.plantuml.Run.main(Array("-headless", "/tmp/*.md"))
}

def formatTimingDiagram(triggers: List[Trigger], changes: List[Event], details: Boolean = true, scale100px: Int = 300): String = {
  val maxCount = changes.map(_.count).maxOption.getOrElse(0)
  println(s"max count is $maxCount")

  s"""
     |@startuml
     |
     |scale $scale100px as 100 pixels
     |
     |robust count
     |count has ${(0 to maxCount).reverse.mkString(",")}
     |
     |${
    if (details)
      triggers.map(t => s"""concise "${t.project}" as ${t.project.replaceAll("[^\\w]", "")}""").distinct.mkString("\n")
    else ""
  }
     |
     |${changes.map(_.uml(details)).mkString("\n")}
     |@enduml
     |""".stripMargin
}


def allTriggers(file: String, filterKinds: List[String]): List[(DayOfWeek, List[Trigger])] = {
  val json = scala.io.Source.fromFile(file).getLines.mkString
  val triggers = for {
    project <- ujson.read(json).arr.toList
    groups <- filterKinds.map(kind =>
      project.obj.get(s"${kind}RoutesTriggers").fold(List.empty[(Value, String)])(_.arr.toList zip Iterator.continually(kind))
    )
    (trigger, kind) <- groups
  } yield Trigger(project("name").str, trigger("dayOfWeek").str, trigger("at").str, trigger("during").str, kind)
  triggers.groupBy(_.day).toList.sortBy(_._1)
}

def aggregate(triggers: List[Trigger]): List[Event] =
  triggers.flatMap(t => List(
    Event(t.at, start = List(t), count = 1),
    Event(t.end, stop = List(t), count = -1)
  )).sortBy(_.at)
    .foldLeft(List(Event())) {
      case (h :: t, event) if h.at == event.at => Event(h.at, h.start ++ event.start, h.stop ++ event.stop, h.count + event.count) :: t
      case (h :: t, event) => event.copy(count = event.count + h.count) :: h :: t
    }.reverse


case class Trigger(project: String, day: DayOfWeek, at: LocalTime, during: Duration, kind: String) {
  def about(hour: Int): Boolean = at.getHour == hour || end.getHour == hour

  val end: LocalTime = at.plus(during)
}

object Trigger {
  val durationRe = """(\d+.?\d*)\s?(?:min)?""".r

  def apply(project: String, day: String, at: String, during: String, kind: String): Trigger = {
    val durationRe(durationMin) = during
    Trigger(project, DayOfWeek.valueOf(day), LocalTime.parse(at), Duration.ofMinutes(durationMin.toDouble.toLong), kind)
  }
}

case class Event(at: LocalTime = LocalTime.of(0, 0), start: List[Trigger] = Nil, stop: List[Trigger] = Nil, count: Int = 0) {
  def uml(details: Boolean): String =
    s"""@$at:00
       |${if (details) start.map(p => s"${p.project.replaceAll("[^\\w]", "")} is \"${p.kind} (${p.at}-${p.end})\"").mkString("\n") else ""}
       |${if (details) stop.map(p => s"${p.project.replaceAll("[^\\w]", "")} is {hidden}").mkString("\n") else ""}
       |count is $count
       |""".stripMargin
}
