#!/usr/bin/env amm
!#
import $ivy.`ai.kien::python-native-libs:0.2.1`
import $ivy.`me.shadaj::scalapy-core:0.5.2`

import me.shadaj.scalapy.py

import ai.kien.python.Python

Python().scalapyProperties.fold(
  ex => println(s"Error while getting ScalaPy properties: $ex"),
  props => props.foreach { case(k, v) => System.setProperty(k, v) }
)

val list = py.Dynamic.global.range(1, 3 + 1)
val listSum = py.Dynamic.global.sum(list)

println(listSum)

import me.shadaj.scalapy.py.PyQuote

val s=py"sum(range(1,5))".as[Int]
println(s)

