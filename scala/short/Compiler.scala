import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox

// need  "org.scala-lang"             % "scala-compiler"     % "2.12.10"

object Compiler {

  def compile[A](string: String): A = {
    val toolbox = currentMirror.mkToolBox()
    val tree    = toolbox.parse(string)
    toolbox.eval(tree).asInstanceOf[A]
  }
}
