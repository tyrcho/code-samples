import scala.io.StdIn._

object Solution extends App {
  val Array(width, height) = readLine.split(" ").map(_.toInt)
  val emptyLine = Vector.fill(width + 2)(false)
  val cells: Vector[Vector[Boolean]] = emptyLine +: Vector.fill(height)(false +: readLine.map(_ == '1').toVector :+ false) :+ emptyLine

  val nextCells = Vector.tabulate(height) { h =>
    val lines = cells.drop(h).take(3)
    Vector.tabulate(width) { w =>
      val wasLive = cells(h + 1)(w + 1)
      val liveNeighbours = lines.map(_.drop(w).take(3).count(true.==)).sum - (if (wasLive) 1 else 0)
      liveNeighbours == 3 || (liveNeighbours == 2 && wasLive)
    }
  }

  println(toString(nextCells))

  def toString(someCells: Vector[Vector[Boolean]]) =
    someCells.map(_.map(c => if (c) 1 else 0).mkString).mkString("\n")
}