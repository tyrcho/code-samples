#!/usr/bin/env amm
!#

import $ivy.`com.amazonaws:aws-java-sdk-core:1.12.329`
import $ivy.`com.amazonaws:aws-java-sdk-s3:1.12.329`
import $ivy.`com.lihaoyi::os-lib:0.8.0`

import com.amazonaws.services.s3._
import com.amazonaws.services.s3.model._
import mainargs.main


import scala.jdk.CollectionConverters._

val region = "eu-west-1"

val s3 = AmazonS3Client.builder().withRegion(region).build()
val defaultBucket = "production-file-archives"

@main def copyFiles(prefix: String, count: Int, targetFolder: String = "/tmp") = {
  listFiles(prefix, count).foreach { summary =>

    val content = s3.getObject(new GetObjectRequest(defaultBucket, summary.getKey)).getObjectContent
    val targetFile = os.Path(targetFolder) / summary.getKey.split("/").last
    val targetStream = os.write.outputStream(targetFile)
    val buffer = new Array[Byte](1024)
    Iterator
      .continually(content.read(buffer))
      .takeWhile(x => x != -1)
      .foreach(read => targetStream.write(buffer, 0, read))
    targetStream.close()
    println(targetFile)
  }
}

@main
def listFiles(prefix: String, count: Int): List[S3ObjectSummary] = {
  def listOneBatch(marker: Option[String] = None, max: Int) = {
    val request = (new ListObjectsRequest).withBucketName(defaultBucket).withPrefix(prefix).withMaxKeys(max)
    s3.listObjects(marker.fold(request)(request.withMarker))
  }

  def step(marker: Option[String] = None, read: Int = 0): List[S3ObjectSummary] =
    if (read >= count || (read > 0 && marker.isEmpty)) Nil
    else {
      val listing = listOneBatch(marker, count - read)
      val readInThisBatch = listing.getObjectSummaries.asScala
      val sizeOfThisBatch = readInThisBatch.size
      System.err.println(s"read $sizeOfThisBatch elements starting from $marker")
      step(Option(listing.getNextMarker), read + sizeOfThisBatch) ++ readInThisBatch
    }

  step()
}