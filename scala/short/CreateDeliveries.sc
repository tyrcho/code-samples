import $ivy.`com.github.tototoshi::scala-csv:1.3.10`
import com.github.tototoshi.csv._

import java.io.File
import scala.util.Try

@main
def process(fileName: String, apiKey: String): Unit = {
  val handler = OrderHandler(apiKey)
  CSVReader
    .open(new File(fileName))(new DefaultCSVFormat {
      override val delimiter = ';'
    })
    .allWithHeaders()
    .foreach(handler.processLine)
}

final case class OrderHandler(
                               apiKey: String,
                               url: String = "https://sandbox.api.colisweb.com/4/deliveries/",
                               storeCode: String = "id_st_priest") {
  val headers = Map("Content-Type" -> "application/json", "Api-Key" -> apiKey)

  def processLine(line: Map[String, String]): Unit = {
    val bookResp = book(line)
    val bookJson = ujson.read(bookResp)
    val uuid = bookJson("delivery").obj("uuid").str
    val reference = line("Référence primaire")
    requests.put(url = s"$url/$uuid/confirm",
      headers = headers,
      data = s"""{ "primary_order_reference": "$reference" }""")
    println(s"confirmed order with reference $reference : uuid=$uuid")
  }

  def book(order: Map[String, String]): String = {
    val body =
      s"""{
    "slot": {
      "starts_at": "${order("Date")}T${order("Début du créneau").take(2)}:00:00+02:00",
      "size": 240
    },
    "route": [
      {
        "type": "pickup",
        "location": {
          "type": "address",
          "first_name": "${order("Nom retrait")}",
          "last_name": "${order("Nom retrait")}",
          "email": "noreply@colisweb.com",
          "primary_phone": "0",
          "line1": "${order("Adresse 1 retrait")}",
          "postal_code": "${order("C.P retrait")}",
          "city": "${order("Ville retrait")}",
          "country": "France",
          "floor": "",
          "lift_information": "maybe_lift"
        }
      },
      {
        "type": "shipping",
        "location": {
          "type": "address",
          "first_name": "${order("Nom livraison")}",
          "last_name": "${order("Nom livraison")}",
          "email": "noreply@colisweb.com",
          "primary_phone": "0",
          "line1": "${order("Adresse 1 livraison")}",
          "postal_code": "${order("C.P livraison")}",
          "city": "${order("Ville livraison")}",
          "country": "France",
          "floor": "${Try(order("Étage").toInt.toString).getOrElse("")}",
          "lift_information": "${order("Avec ascenseur")}"
        }
      }
    ],
    "packaging": {
      "packets" : [ ${packaging(order("Nombre d'articles").toInt)} ]
    },
    "owner": { "store_code": "$storeCode" },
    "required_skills": ["Livraison dans la pièce du choix"]
  }
"""

    requests.post(url = s"$url/book", headers = headers, data = body).text()
  }

  def packaging(count: Int) =
    Seq
      .fill(count)(""" { "length": 60, "height": 40, "width": 32, "weight": 10 } """)
      .mkString(",")

}
