import $ivy.`org.scalanlp::breeze-natives:0.13.2`
import $ivy.`org.scalanlp::breeze-viz:0.13.2`
import $ivy.`org.scalanlp::breeze:0.13.2`
import breeze.linalg._
import breeze.stats.regression._


val inputs = new DenseMatrix[Double](1000, 5)
val outputs = new DenseVector(new Array[Double](1000))
for (i <- 0 until 1000) {
  inputs.update(i, 0, i - 1)
  inputs.update(i, 1, i + 1)
  inputs.update(i, 2, i + 10)
  inputs.update(i, 3, i + 100)
  inputs.update(i, 4, 1)
  outputs.update(i, i)
}


val coefs = leastSquares(inputs, outputs).coefficients

println(coefs)

