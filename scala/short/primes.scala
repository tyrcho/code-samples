val primes:Stream[Int] =
  2 #:: Stream.from(3,2).filter(
     n => !primes.takeWhile(_ <= Math.sqrt(n)).exists(n % _ ==0))
primes take 100 foreach println