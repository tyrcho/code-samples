//  amm JsonProcess.sc https://raw.githubusercontent.com/5etools-mirror-1/5etools-mirror-1.github.io/master/data/backgrounds.json
import upickle.default._

import scala.util.Try

implicit val rwe: ReadWriter[Entry] = macroRW[Entry]

@main
def main(url: String) = {
  val r = requests.get(url)
  val jsonString = r.text
  val data = ujson.read(jsonString)
  val backgrounds = data("background").arr.toList.collect {
    case b if b.obj.value.contains("entries") =>
      Background(
        name = b("name").str,
        entries = b("entries").arrOpt
          .fold(List.empty[ujson.Value])(_.toList)
          .collect {
            case be if Try(be("entries").arr).isSuccess =>
              BackgroundEntry(
                `type` = be("type").str,
                entries = be("entries").arr.toList
                  .flatMap(e => Try(read[Entry](e)).toOption)
              )
          }
      )
  }

  val details = for {
    b <- backgrounds
    be <- b.entries
    e <- be.entries
    d <- e.data
  } yield e.kind -> d

  val keepKinds = Set("Personality Trait", "Bond", "Ideal", "Flaw")
  for {
    (k, v) <- details
      .groupMap(_._1)(_._2)
      .view
      .mapValues(_.distinct)
      .filterKeys(keepKinds)
  } {
    println(k)
    v.foreach(println)
    println
  }
}

final case class Background(name: String, entries: Seq[BackgroundEntry] = Nil)

final case class BackgroundEntry(
    `type`: String,
    entries: Seq[Entry] = Nil,
    name: Option[String] = None
)

final case class Entry(
    `type`: String,
    colLabels: Seq[String],
    rows: Seq[Seq[String]]
) {
  def kind: String = colLabels(1)

  def data: Seq[String] = rows.map(_(1))
}
