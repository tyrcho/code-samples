import scala.util.Random

object Main extends App {
  val players = Vector.tabulate(4)(i => ConsolePlayer(i, 4))
  val judge = Judge(players).start
  judge.play
}

case class Judge(
                  players: Vector[Player],
                  live: Map[Int, Boolean] = Map.empty.withDefaultValue(true),
                  hand: Map[Int, Card] = Map.empty,
                  turn: Int = 0,
                  next: Int = 0,
                  discarded: Vector[List[Card]] = Vector.empty,
                  deck: Deck = Deck()) {

  def start =
    players.zipWithIndex.foldLeft(this) { case (judge, (player, i)) =>
      judge.draw(player, i)
    }


  def play: Judge =
    if (deck.cards.size <= 1 || players.indices.count(live) == 1) this
    else playOnce.play

  def playOnce: Judge = {
    val player = players(next)
    val j = draw(player, next)
    val move = player.play
    j.resolve(move, next)
  }

  def resolve(m: Move, player: Int): Judge = {
    players.foreach(_.notify(Event(Discard,player,Some( m.card))))

    m match {
      case Move(Guard, Some(pl), Some(card)) =>
        if (hand(pl) == card) {
          players.foreach(_.notify(Event(Eliminated, pl)))
          remove(pl)
        } else this
    }
    this
  }

  def remove(p: Int) = copy(live = live.updated(p, false))


  private def draw(player: Player, i: Int) = {
    val (card, d) = deck.draw
    val others = players.toSet - player
    player.notify(Event(Draw, i, Some(card)))
    others.foreach(_.notify(Event(Draw, i, None)))
    copy(deck = d, hand = hand.updated(i, card))
  }
}


case class Deck(cards: List[Card]) {
  def draw = (cards.head, Deck(cards.tail))
}

object Deck {
  def apply(): Deck = {
    val cards = for {
      kind <- Cards.kinds
      i <- 1 to kind.initialCount
    } yield kind
    Deck(Random.shuffle(cards))
  }
}

trait Player {
  def notify(event: Event)

  def play: Move
}

case class ConsolePlayer(id: Int, count: Int) extends Player {


  def notify(event: Event): Unit = println(s"$id :$event")

  def play: Move = Move(Guard, Some((id + 1) % count), Some(Princess))
}

case class Move(card: Card, target: Option[Int], choice: Option[Card])

sealed trait Action

case object Draw extends Action

case object Discard extends Action

case object Eliminated extends Action

case class Event(action: Action, player: Int, card: Option[Card] = None)

case class Card(name: String, rank: Int, initialCount: Int = 2, targetsPlayer: Boolean = false, cardChoice: Boolean = false)

object Guard extends Card("Guard", 1, 5, true, true)

object Princess extends Card("Princess", 8, 1)

object Cards {
  val kinds = List(Guard, Princess)
}
