val re = """(?x)
# Match a 20th or 21st century date in yyyy-mm-dd format

# ?x is free-spacing flag to allow #comments, must be right at start of String

(19|20)\d\d                # year (group 1)

[- /.]                     # separator

(0[1-9]|1[012])            # month (group 2)

[- /.]                     # separator

(0[1-9]|[12][0-9]|3[01])   # day (group 3)

""".r

"1990-02-20" match {
  case re(y, m, d) => println(y, m, d)
}


// see https://www.regular-expressions.info/freespacing.html
// try it online @ https://scastie.scala-lang.org/cG09nykWSPG8tRfGhf5CHA