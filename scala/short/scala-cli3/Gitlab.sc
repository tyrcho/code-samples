#!/usr/bin/env -S scala-cli shebang -S 3
!#

//> using scala "3"
//> using dep "com.amazonaws:aws-java-sdk-core:1.12.426"
//> using dep "com.amazonaws:aws-java-sdk-s3:1.12.426"
//> using dep "com.lihaoyi::mainargs:0.4.0"
//> using dep "com.lihaoyi::pprint:0.8.1"
//> using dep "com.lihaoyi::os-lib:0.9.1"
//> using dep "org.gitlab4j:gitlab4j-api:5.0.1"

import com.amazonaws.services.s3._
import com.amazonaws.waiters._
import com.amazonaws.services.s3.model._
import scala.jdk.CollectionConverters._
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Project
import os.{Path, root}

val region = "eu-west-3"
val s3 = AmazonS3Client.builder().withRegion(region).build()
val defaultBucket = "code-source"


val token = sys.env("GITLAB_PAT")
val user = "ci.common"

val gitlab = new GitLabApi("https://gitlab.com", token)

val archive: Path = os.temp.dir()
println(s"tmp dir $archive")

os.remove(Path("/tmp/archive.zip"))

for {
  group <- Seq("/back", "/front", "/infra", "")
} {
  gitlab
    .getGroupApi
    .getGroup(s"colisweb-idl/colisweb$group")
    .getProjects
    .asScala
    .filterNot(_.getArchived)
    .foreach(archiveZip(archive))
}

def archiveZip(targetFolder: Path)(project: Project): Unit = {
  println(project.getNameWithNamespace)
  val zippedRepo = s"${project.getName.replaceAll(" ", "_")}.zip"
  os.proc("git",
    "archive",
    s"--remote=${project.getSshUrlToRepo}",
    project.getDefaultBranch,
    "-o",
    zippedRepo,
    "--format=zip",
    "-9"
  ).call(targetFolder)
  uploadFile(s"$targetFolder/$zippedRepo")
  os.proc("zip", "/tmp/archive.zip", zippedRepo).call(targetFolder)
}

def handleProject(project: Project): Unit = {
  println(project)
  val path = os.Path(s"/tmp/${project.getName}")
  if (!os.isDir(path)) {
    os.proc(s"git clone --depth 1 ${project.getSshUrlToRepo}".split(" ")).call(root / "tmp")
  }
}


def uploadFile(fileName: String) = {
  val datePrefix = java.time.format.DateTimeFormatter.BASIC_ISO_DATE.format(java.time.LocalDate.now())
  val path = Path(fileName)
  val s3Key = s"$datePrefix/${path.last}"
  s3.putObject(new PutObjectRequest(defaultBucket, s3Key, path.toNIO.toFile))
  s3.waiters().objectExists().run(new WaiterParameters(new GetObjectMetadataRequest(defaultBucket, s3Key)))
  s3.listObjects((new ListObjectsRequest).withBucketName(defaultBucket).withPrefix(s3Key)).getObjectSummaries.asScala
}


