//  scala-cli .\JsonParser.sc --java-opt "-Dfile.encoding=UTF-16" > out.md


//> using scala "3.3.4"
//> using dep "com.amazonaws:aws-java-sdk-s3:1.12.779"
//> using dep "com.lihaoyi::upickle:4.0.2"
//> using dep "com.lihaoyi::os-lib:0.11.3"
//> using dep "com.lihaoyi::pprint:0.9.0"


val surnames = ujson.read(os.read(os.pwd / "common-surnames-by-country.json")).obj
val forenames = ujson.read(os.read(os.pwd / "common-forenames-by-country.json")).obj

for {
    code <- (surnames.keys ++ forenames.keys).toSeq.sorted
} {
    val surnamesStr = surnames.getOrElse(code, ujson.Arr()).arr.toSeq.map { name =>
        val local = name.obj("localized").arr.headOption.fold("")(_.str)
        val roman = name.obj("romanized").arr.headOption.fold("")(_.str)
        if local != roman then s"$roman ($local)" else roman
    }.sorted
    val forenamesStrM = forenames.getOrElse(code, ujson.Arr()).arr.toSeq.flatMap { entry =>
        entry("names").arr.toSeq.flatMap { name =>
            val local = name.obj("localized").arr.head.str
            val roman = name.obj("romanized").arr.head.str
            val gender = name.obj("gender").str
            if gender == "M" then
                Some(if local != roman then s"$roman ($local)" else roman)
            else None
        }
    }.sorted
    val forenamesStrF = forenames.getOrElse(code, ujson.Arr()).arr.toSeq.flatMap { entry =>
        entry("names").arr.toSeq.flatMap { name =>
            val local = name.obj("localized").arr.head.str
            val roman = name.obj("romanized").arr.head.str
            val gender = name.obj("gender").str
            if gender == "F" then
                Some(if local != roman then s"$roman ($local)" else roman)
            else None
        }
    }.sorted
    pretty(code, surnamesStr, forenamesStrM, forenamesStrF)
}

def pretty(code: String, surnames: Seq[String], firstNamesM: Seq[String], firstNamesF: Seq[String]) =
    val country = new java.util.Locale("", code).getDisplayCountry
    println(
        s"""
# $country
${if firstNamesM.isEmpty then "" else prettyTable(code, "PrenomH", firstNamesM)}

${if firstNamesF.isEmpty then "" else prettyTable(code, "PrenomF", firstNamesF)}

${if surnames.isEmpty then "" else prettyTable(code, "Nom", surnames)}

""")

def prettyTable(code: String, label: String, names: Seq[String]) =
    s"""
| $label |
| ---    |
${names.mkString("| ", " |\n| ", " |")}
^$label-$code


"""



