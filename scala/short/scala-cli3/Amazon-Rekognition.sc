#!/usr/bin/env -S scala-cli shebang -S 3
!#

//> using scala "3"
//> using dep "com.amazonaws:aws-java-sdk-core:1.12.426"
//> using dep "com.amazonaws:aws-java-sdk-rekognition:1.12.426"
//> using dep "com.amazonaws:aws-java-sdk-s3:1.12.426"
//> using dep "com.lihaoyi::mainargs:0.4.0"
//> using dep "com.lihaoyi::pprint:0.8.1"

import com.amazonaws.services.rekognition._
import com.amazonaws.services.rekognition.model._
import com.amazonaws.services.s3._
import com.amazonaws.services.s3.model.{GetObjectMetadataRequest, ListObjectsRequest, S3ObjectSummary}
import mainargs._


import java.nio.charset.Charset
import java.time.ZoneId
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
import scala.jdk.CollectionConverters._

val region = "eu-west-1"
val defaultProjectName: String = "delivery-pictures"
val defaultProjectVersionArn = s"arn:aws:rekognition:$region:949316342391:project/Livreurs/version/v0/1666962039364"
val defaultProjectArn = s"arn:aws:rekognition:$region:949316342391:project/Livreurs/1648115627643"

val rekog = AmazonRekognitionClient.builder().withRegion(region).build()

val s3 = AmazonS3Client.builder().withRegion(region).build()
val defaultBucket = "colisweb-backup"

pprint.pprintln(ParserForMethods(this).runOrExit(args))


// ./Amazon-Rekognition.sc detectLabelsInFolder --prefix pictures/production/2022/5/4/17
@main
def detectLabelsInFolder(prefix: String, count: Int = 10) = {
  startAndWait()
  val idNames = Seq("transporter_id", "carrier_id", "client_id", "store_id", "delivery_id")
  println(s"${idNames.mkString(",")},picture,date,classification,confidence")
  listS3Pictures(prefix, count)
    .foreach { summary =>
      val metadata = s3.getObjectMetadata(new GetObjectMetadataRequest(defaultBucket, summary.getKey))
      val ids = idNames.map(metadata.getUserMetaDataOf)
      val key = summary.getKey
      val date = ISO_LOCAL_DATE_TIME.format(metadata.getLastModified.toInstant.atZone(ZoneId.of("Europe/Paris")))
      val label = findCustomLabels(defaultProjectVersionArn)(summary.getKey)
      val row = ids ++ List(key, date) ++ label.flatMap(l => List(l.getName, l.getConfidence))
      println(row.mkString(","))
    }

  stopAndWait()
}

@main
def listS3Pictures(prefix: String, count: Int = 10): List[S3ObjectSummary] = {
  def listOneBatch(marker: Option[String] = None, max: Int) = {
    val request = (new ListObjectsRequest).withBucketName(defaultBucket).withPrefix(prefix).withMaxKeys(max)
    s3.listObjects(marker.fold(request)(request.withMarker))
  }

  def step(marker: Option[String] = None, read: Int = 0): List[S3ObjectSummary] =
    if (read >= count || (read > 0 && marker.isEmpty)) Nil
    else {
      val listing = listOneBatch(marker, count - read)
      val readInThisBatch = listing.getObjectSummaries.asScala
      val sizeOfThisBatch = readInThisBatch.size
      System.err.println(s"read $sizeOfThisBatch elements starting from $marker")
      if(sizeOfThisBatch > 0)
        step(Option(listing.getNextMarker), read + sizeOfThisBatch) ++ readInThisBatch
      else Nil
    }

  step()
}

// ./Amazon-Rekognition.sc detectLabels --imageName "pictures/production/location/1000009/1547549911.jpg" # box
@main
def detectLabels(imageName: String) =
  println("labels : " + getLabels(imageName))


@main
def describeProject(projectName: String = defaultProjectName) =
  datasets(projectName).foreach(println)

// ./Amazon-Rekognition.sc detectCustomLabels --imageName pictures/production/location/6991852/RackMultipart20220225-35-xkhgkq.jpg
@main
def detectCustomLabel(imageName: String, arn: String = defaultProjectVersionArn) =
  findCustomLabels(arn)(imageName).headOption


// ./Amazon-Rekognition.sc addLabelToTrainingDataset --imageNames pictures/production/location/6991852/RackMultipart20220225-35-xkhgkq.jpg
// ./Amazon-Rekognition.sc addLabelToTrainingDataset --imageNames "$(cat noires.txt)" --label noire
@main
def addLabelToDataset(
                       imageNames: String,
                       label: String = "noire",
                       datasetType: String = "TRAIN", // or TEST
                       projectName: String = defaultProjectName
                     ) = {
  val datasetArn = findDatasetArn(projectName, DatasetType.valueOf(datasetType)).get
  val jsonLines = imageNames.split("\\s+").map(imageName =>
    s"""{
       |  "source-ref": "s3://$defaultBucket/$imageName",
       |  "$label": 1,
       |  "$label-metadata":{
       |        "class-name": "$label",
       |        "confidence": 0.99,
       |        "job-name": "identify-pictures",
       |        "type":"groundtruth/image-classification",
       |        "creation-date": "2018-10-18T22:18:13.527256",
       |        "human-annotated": "yes"
       |    }
       |}""".stripMargin.split("\n").mkString(" ")).mkString("\n")
  val changes = (new DatasetChanges).withGroundTruth(Charset.defaultCharset.encode(jsonLines))
  rekog.updateDatasetEntries((new UpdateDatasetEntriesRequest).withDatasetArn(datasetArn).withChanges(changes))
}


@main
def addImagesToTestDataset(
                            imageNames: String,
                            projectName: String = defaultProjectName
                          ) = {
  val datasetArn = findDatasetArn(projectName, DatasetType.TEST).get
  val jsonLines = imageNames.split("\\s+").map(imageName =>
    s"""{
       |  "source-ref": "s3://$defaultBucket/$imageName"
       |}""".stripMargin.split("\n").mkString(" ")).mkString("\n")
  val changes = (new DatasetChanges).withGroundTruth(Charset.defaultCharset.encode(jsonLines))
  rekog.updateDatasetEntries((new UpdateDatasetEntriesRequest).withDatasetArn(datasetArn).withChanges(changes))
}

@main
def trainProject(
                  versionName: String,
                  projectName: String = defaultProjectName
                ) =
  rekog.createProjectVersion(
    new CreateProjectVersionRequest()
      .withProjectArn(findProject(projectName).get.getProjectArn)
      .withVersionName(versionName)
      .withOutputConfig(new OutputConfig().withS3Bucket(defaultBucket).withS3KeyPrefix(versionName))
  )


// ./Amazon-Rekognition.sc start
@main
def start(arn: String = defaultProjectVersionArn) =
  rekog.startProjectVersion((new StartProjectVersionRequest)
    .withProjectVersionArn(arn)
    .withMinInferenceUnits(1)
  )

@main
def startAndWait(arn: String = defaultProjectVersionArn): Unit =
  status(defaultProjectArn) match {
    case "RUNNING" => System.err.println("\nRunning")
    case "STARTING" =>
      System.err.print(".")
      Thread.sleep(10000)
      startAndWait(arn)
    case "STOPPED" => start(arn)
      startAndWait(arn)
    case s => throw new Exception(s"unexepected state $s")
  }

@main
def stopAndWait(arn: String = defaultProjectVersionArn): Unit =
  status(defaultProjectArn) match {
    case "STOPPED" => System.err.println("\nStopped")
    case "STOPPING" =>
      System.err.print(".")
      Thread.sleep(10000)
      stopAndWait(arn)
    case "RUNNING" => stop(arn)
      stopAndWait(arn)
    case s => throw new Exception(s"unexepected state $s")
  }

// ./Amazon-Rekognition.sc status
@main
def status(arn: String = defaultProjectArn) =
  rekog.describeProjectVersions((new DescribeProjectVersionsRequest()).withProjectArn(arn))
    .getProjectVersionDescriptions.asScala
    .find(_.getProjectVersionArn == defaultProjectVersionArn)
    .fold("project version not found")(_.getStatus)


// ./Amazon-Rekognition.sc stop
@main
def stop(arn: String = defaultProjectVersionArn) =
  rekog.stopProjectVersion((new StopProjectVersionRequest).withProjectVersionArn(arn))

private def image(imageName: String) =
  (new Image)
    .withS3Object((new S3Object)
      .withName(imageName)
      .withBucket(defaultBucket))


private def findDatasetArn(projectName: String, datasetType: DatasetType) =
  datasets(projectName).find(_.getDatasetType == datasetType.name()).map(_.getDatasetArn)

private def datasets(projectName: String) =
  for {
    project <- findProject(projectName).toList
    dataset <- project.getDatasets.asScala
  } yield dataset

private def findProject(projectName: String): Option[ProjectDescription] =
  rekog.describeProjects(new DescribeProjectsRequest().withProjectNames(projectName)).getProjectDescriptions.asScala.headOption

private def getLabels(imageName: String, minConfidence: Double = 0.8): Seq[String] =
  rekog.detectLabels((new DetectLabelsRequest).withImage(image(imageName)))
    .getLabels.asScala.toSeq.collect {
    case label if label.getParents.isEmpty && label.getConfidence > minConfidence => label.getName
  }
private def findCustomLabels(projectVersionArn: String, minimalConfidence: Double = 80)(imageName: String): List[CustomLabel] =
  rekog
    .detectCustomLabels((new DetectCustomLabelsRequest)
      .withImage(image(imageName))
      .withProjectVersionArn(projectVersionArn)
    )
    .getCustomLabels
    .asScala
    .toList
    .filter(_.getConfidence > minimalConfidence)
