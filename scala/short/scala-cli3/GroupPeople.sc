#!/usr/bin/env -S scala-cli shebang
!#

//> using scala "3"
//> using dep "com.lihaoyi::mainargs:0.4.0"

import scala.io.StdIn._
import scala.util.Random
import mainargs._


ParserForMethods(this).runOrExit(args)

// https://docs.google.com/spreadsheets/d/1ua8qlTQlCmqOTu0tRPe9qPkxXWCdd9P4MMDyWVB0zao/edit#gid=306294299
//  pbpaste | ./GroupPeople.sc --maxSize 5
@main
def main(maxSize  : Int) = {
  val data: List[Person] = LazyList.continually(readLine())
    .takeWhile(null.!=)
    .toList
    .flatMap(Person(_))
    .filterNot(_.tags.exists(_.startsWith("abs")))


  val best = Seq.fill(10000)(randomSolution(maxSize, data)).maxBy(_.score)
  println(best)
}

def randomSolution(maxSize: Int, data: Seq[Person]): Solution = {
  val count = math.ceil(data.size.toFloat / maxSize).toInt
  val transposed = Random.shuffle(data).grouped(count).toSeq
  val groups = Seq.tabulate(count)(i => transposed.flatMap(_.lift(i)))
  Solution(groups.map(Group.apply))
}


final case class Solution(groups: Seq[Group]) {
  val score: Int = groups.map(_.score).min

  override def toString = groups.mkString("\n")
}

final case class Group(persons: Seq[Person]) {
  val score: Int = persons.flatMap(_.tags).distinct.size

  override def toString = persons.mkString("* "," | ","")
}

final case class Person(name: String, tags: Seq[String]) {
  override def toString = s"$name (${tags.head})"
}

object Person {
  def apply(line: String): Option[Person] = line.split("""[,;\t]+""").toList match {
    case name :: tags => Some(Person(name, tags))
    case _ => None
  }
}

