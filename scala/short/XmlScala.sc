#!/usr/bin/env amm
!#

import $ivy.`org.scala-lang.modules::scala-xml:2.1.0`
import $ivy.`com.lihaoyi::os-lib:0.8.0`

import scala.xml._
import mainargs.main
import os._

@main
def readAll(folder: String) =
  for {
    f <- list(Path(folder))
    n = f.toString
    if n.endsWith(".xml")
  } read(n)

@main
def read(file: String) = {
  System.err.println(file)
  val x = XML.loadFile(file)
  for {
    shipment <- x \\ "shipments" \ "shipment"
    if (shipment \ "edi2").text == "true" && (shipment \ "shipmentType").text == "CCD"
    s = Shipment(
      orderNumber = (shipment \ "orderNumber").text,
      shipmentNumber = (shipment \ "shipmentNumber").text,
      totalWeight = round(shipment \ "totalWeight"),
      maxLineVolume = (shipment \ "orderLines" \ "orderLine" \ "volume").map(_.text.toFloat*1000).max.toInt,
      maxLineWeight = (shipment \ "orderLines" \ "orderLine" \ "weight").map(_.text.toFloat.toInt).max,
      method = if( (shipment \ "deliveryMethod").text.contains("1") ) "1" else "2"
    )
  } println(s.csv)
}

def round(n:NodeSeq) = n.text.toFloat.toInt

case class Shipment(orderNumber: String, shipmentNumber: String, totalWeight: Int, maxLineVolume: Int, maxLineWeight: Int, method:String) {
  val csv = s"$orderNumber,$shipmentNumber,$totalWeight,$maxLineVolume,$maxLineWeight,$method"
}