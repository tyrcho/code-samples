object Unfoldable {
  import scala.collection.generic.CanBuildFrom

  final implicit class UnfoldableOps[A](a: A) {
    def unfold[B, This, That](f: A => Option[(B, A)])(implicit cb: CanBuildFrom[This, B, That]): That = {
      val builder = cb()
      @tailrec def unfolding(a: A): That = {
        f(a) match {
          case Some((e, next)) =>
            builder += e
            unfolding(next)
          case None =>
            builder.result()
        }
      }
      unfolding(a)
    }
  }

}

