package main

import (
	"fmt"
	"regexp"
	"strings"
)

func main() {
	println(ExtractAccountFromIdentifier("2|677301038893|ap-northeast-1"))
	println(ParseAccountFromIdentifier("2|677301038893|ap-northeast-1"))
}

// Received as 2|677301038893|ap-northeast-1
func ExtractAccountFromIdentifier(identifier string) (string, error) {
	parts := strings.Split(identifier, "|")
	if len(parts) != 3 {
		return "", fmt.Errorf("invalid account identifier format: %s", identifier)
	}
	return parts[1], nil
}

var r = regexp.MustCompile(`\d+\|(\d+)\|\w+`)

// Received as 2|677301038893|ap-northeast-1
func ParseAccountFromIdentifier(identifier string) (string, error) {
	m := r.FindStringSubmatch(identifier)
	if len(m) != 2 {
		return "", fmt.Errorf("invalid account identifier format: %s", identifier)
	}
	return m[1], nil
}
