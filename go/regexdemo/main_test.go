package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseAccountFromIdentifier(t *testing.T) {
	t.Run("parse", func(t *testing.T) {
		got, _ := ParseAccountFromIdentifier("2|677301038893|ap-northeast-1")
		require.Equal(t, "677301038893", got)
	})
	t.Run("parse error", func(t *testing.T) {
		_, err := ParseAccountFromIdentifier("2|qsd|ap-northeast-1")
		require.ErrorContains(t, err, "invalid account identifier format: 2|qsd|ap-northeast-1")
	})
}

var account string
var ids = []string{
	"2|677301038893|ap-northeast-1",
	"12|987|ap-northeast-1",
	"32|67745301038893|eu-northeast-1",
	"82|67730331038893|us-west-1",
	"26|67730111038893|eu-west-1",
	"22|67730101138893|ap-south-1",
	"23|67730103812893|ap-east-1",
	"24|67730103883393|cn-northeast-1",
	"27|67730103889223|au-northeast-1",
	"25|6773010388933|cn-north-2",
	"22|67730103889311|eu-north-1",
}

func BenchmarkParse(b *testing.B) {
	for n := 0; n < b.N; n++ {
		account, _ = ParseAccountFromIdentifier(ids[n%len(ids)])
	}
	println(account)
}

func BenchmarkExtract(b *testing.B) {
	for n := 0; n < b.N; n++ {
		account, _ = ExtractAccountFromIdentifier(ids[n%len(ids)])
	}
	println(account)
}
