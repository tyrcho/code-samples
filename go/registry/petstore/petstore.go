package petstore

var (
	Animals = map[string]Animal{}
)

func Register(name string, a Animal) {
	Animals[name] = a
}

func List() []string {
	names := make([]string, 0)
	for a := range Animals {
		names = append(names, a)
	}
	return names
}

type Animal interface {
	Speak() string
}
