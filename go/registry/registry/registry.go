package registry

import (
	"tyrcho/demo/cat"
	"tyrcho/demo/panda"
)

func RegisterAll() {
	cat.Register()
	panda.Register()
}
