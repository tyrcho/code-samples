package panda

import (
	"strings"
	"tyrcho/demo/petstore"
)

type Panda struct{}

func (*Panda) Speak() string {
	return strings.Join(petstore.List(), ",")
}

func Register() {
	petstore.Register("panda", &Panda{})
}
