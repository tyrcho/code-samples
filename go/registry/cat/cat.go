package cat

import "tyrcho/demo/petstore"

type Cat struct{}

func (*Cat) Speak() string {
	return "meow"
}

func Register() {
	cat := Cat{}
	petstore.Register("cat", &cat)
}
