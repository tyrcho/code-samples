package dog

import "tyrcho/demo/petstore"

type Dog struct{}

func (*Dog) Speak() string {
	return "woof"
}

func init() {
	petstore.Register("dog", &Dog{})
}
