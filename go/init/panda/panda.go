package panda

import (
	"strings"
	"tyrcho/demo/petstore"
)

type Panda struct{}

func (*Panda) Speak() string {
	return strings.Join(petstore.List(), ",")
}

func init() {
	petstore.Register("panda", &Panda{})
}
