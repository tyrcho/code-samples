package main

import (
	"fmt"
	"os"
	_ "tyrcho/demo/registry"

	"tyrcho/demo/petstore"
)

func main() {
	for _, n := range petstore.List() {
		println(n)
	}

	if len(os.Args) > 1 {
		name := os.Args[1]

		if a, ok := petstore.Animals[name]; ok {
			println(a.Speak())
		} else {
			println(fmt.Sprintf("%s not found", name))
		}
	}
}
