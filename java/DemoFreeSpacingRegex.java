import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DemoFreeSpacingRegex {

    public static void main(String[] args) {
        String re = "(?x)\n"
            + "# ?x is free-spacing flag to allow #comments, must be right at start of String\n"
            + "(19|20\\d\\d)              # year (group 1)\n"
            + "[- /.]                     # separator\n"
            + "(0[1-9]|1[012])            # month (group 2)\n"
            + "[- /.]                     # separator\n"
            + "(0[1-9]|[12][0-9]|3[01])   # day (group 3)";
        Pattern pattern = Pattern.compile(re);
        final Matcher matcher = pattern.matcher("2018-04-11");
        final boolean found = matcher.find();
        if (found) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                System.out.println(matcher.group(i));
            }
        }
    }
}
