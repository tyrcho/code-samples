
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class RobotTests {
	public static void main(String[] args) throws Exception {
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();

		for (int i = 0; i < gs.length; i++) {
			DisplayMode mode = gs[i].getDisplayMode();
			Rectangle bounds = new Rectangle(0, 0, mode.getWidth(),
					mode.getHeight());
			BufferedImage capture = new Robot(gs[i])
					.createScreenCapture(bounds);
			String filename = String.format("saved%s.png", i);
			File outputfile = new File(filename);
			ImageIO.write(capture, "png", outputfile);
			System.out.println("saved to " + filename);
		}
	}
}
