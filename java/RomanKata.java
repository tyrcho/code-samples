import org.junit.Test;

import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class RomanKata {
    @Test
    public void testSum() {
        assertEquals("XLIII", sum("XIX", "XXIV"));
        assertEquals("MMMXLVIII", sum("DCXCIV", "MMCCCLIV"));
    }

    @Test
    public void testUnpack() {
        assertEquals("XVIIII", unpack("XIX"));
        assertEquals("XXIIII", unpack("XXIV"));
    }

    @Test
    public void testGroup() {
        assertEquals("XXXXIII", group("XXXVVIII"));
        assertEquals("XXXXIII", group("XXXVIIIIIIII"));
    }

    @Test
    public void testSimplify() {
        assertEquals("XLIII", simplify("XXXXIII"));
    }

    @Test
    public void testSort() {
        assertEquals("XXXVIIIIIIII", sort("XVIIIIXXIIII"));
    }

    static String ordered = "MDCLXVI";

    static String sort(final String in) {
        return in.chars()
                .mapToObj(c -> (char) c)
                .sorted((a, b) -> ordered.indexOf(a) - ordered.indexOf(b))
                .map(c -> c + "")
                .collect(Collectors.joining(""));

    }

    static String sum(final String a, final String b) {
        return simplify(group(sort(unpack(a) + unpack(b))));
    }

    static String simplify(final String in) {
        return in
                .replace("XXXX", "XL")
                .replace("CCCC", "CD")
                ;
    }

    static String unpack(final String in) {
        return in
                .replace("IX", "VIIII")
                .replace("XL", "XXXX")
                .replace("XC", "LXXXX")
                .replace("IV", "IIII");
    }

    static String group(final String in) {
        return in
                .replace("IIIII", "V")
                .replace("VV", "X")
                .replace("XXXXX", "L")
                .replace("LL", "C")
                .replace("CCCCC", "D")
                .replace("DD", "M")
                ;
    }
}
