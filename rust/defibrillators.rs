# https://www.codingame.com/ide/puzzle/defibrillators

use std::io;
use regex::Regex;

macro_rules! parse_input {
    ($x:expr, $t:ident) => ($x.trim().parse::<$t>().unwrap())
}

fn input() -> String {
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    input_line.trim().to_string()
}

#[derive(Debug)]
struct Pos {
    lat: f32,
    lon: f32
}

impl Pos {
    fn distance(&self, other:&Self) -> f32 {
        let x = (other.lon - self.lon) * ((self.lat + other.lat) / 2.).cos();
        let y = other.lat - self.lat;
        6371. * (x * x + y * y).sqrt()
    }
}

#[derive(Debug)]
struct Defib {
    name: String,
    pos:Pos
}

fn parse_float(s: &String) -> f32 {
    parse_input!(s.replace(",", "."), f32)
}

fn main() {
    let re = Regex::new(r"(?x)
    ^.*            # id
    ;(?P<name>.*) 
    ;.*            # address
    ;.*
    ;(?P<lon>.*)   # longitude
    ;(?P<lat>.*)   # latitude
    $").unwrap();

    let user_pos = Pos { 
        lon: parse_float(&input()),
        lat: parse_float(&input())
    };

   
    let n = parse_input!(input(), i32);
    let mut vector: Vec<Defib> = Vec::new();

    for _ in 0..n {
        let defib = input().trim_matches('\n').to_string();
        let caps = re.captures(&defib).unwrap();
        vector.push(Defib{
            pos: Pos{
                lat: parse_float(&caps.name("lat").unwrap().as_str().to_string()),
                lon: parse_float(&caps.name("lon").unwrap().as_str().to_string())
            },
            name:caps.name("name").unwrap().as_str().to_string()
        });
    }
    eprintln!("{:?}", vector);
    let closest = vector.iter().min_by(|defib1, defib2| {
      let dist1 = defib1.pos.distance(&user_pos);
      let dist2 = defib2.pos.distance(&user_pos); 
      dist1.partial_cmp(&dist2).unwrap()
    }).unwrap();

    println!("{}", closest.name);
}
